FROM node:8

# The base node image sets a very verbose log level.
#ENV KARMA_BROWSER PhantomJS
#RUN apt-get update && apt-get install libpng12-0
ENV NPM_CONFIG_LOGLEVEL warn


#WORKDIR .

#ADD package.json package.json

COPY . .

RUN yarn install


EXPOSE 3000

ENV PORT 3000

ENV VIRTUAL_HOST platform.estatechain.io

CMD ["npm", "run", "start:production"]
