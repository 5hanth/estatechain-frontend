// These are the pages you can go to.
// They are all wrapped in the App component, which should contain the navbar etc
// See http://blog.mxstbr.com/2016/01/react-apps-with-pages for more information
// about the code splitting business
import { getAsyncInjectors } from 'utils/asyncInjectors';

const errorLoading = (err) => {
  console.error('Dynamic page loading failed', err); // eslint-disable-line no-console
};

const loadModule = (cb) => (componentModule) => {
  cb(null, componentModule.default);
};

function redirectToLogin(nextState, replace) {
  //console.log(localStorage.token);
  //localStorage.removeItem('token');
   if (localStorage.token===null || localStorage.token===undefined) {
    replace({
      pathname: '/auth'
    })
  }
}

export default function createRoutes(store) {
  // Create reusable async injectors using getAsyncInjectors factory
  const { injectReducer, injectSagas } = getAsyncInjectors(store); // eslint-disable-line no-unused-vars

  return [
    {
      path: '/',
      name: 'home',
      getComponent(nextState, cb) {
        const importModules = Promise.all([
          import('containers/HomePage'),
        ]);

        const renderRoute = loadModule(cb);

        importModules.then(([component]) => {
          renderRoute(component);
        });

        importModules.catch(errorLoading);
      },
    },
    {
      path: '/ico',
      name: 'Ico',
      getComponent(nextState, cb) {
        const importModules = Promise.all([
          import('containers/Ico'),
        ]);

        const renderRoute = loadModule(cb);

        importModules.then(([component]) => {
          renderRoute(component);
        });

        importModules.catch(errorLoading);
      },
    },
    {
      path: '/properties',
      name: 'properties',
      getComponent(nextState, cb) {
        const importModules = Promise.all([
          import('containers/PropertyListing'),
        ]);

        const renderRoute = loadModule(cb);

        importModules.then(([component]) => {
          renderRoute(component);
        });

        importModules.catch(errorLoading);
      },
    },
    {
      path: '/properties/:id',
      name: 'propertyDetail',
      getComponent(nextState, cb) {
        const importModules = Promise.all([
          import('containers/PropertyDetail'),
        ]);

        const renderRoute = loadModule(cb);

        importModules.then(([component]) => {
          renderRoute(component);
        });

        importModules.catch(errorLoading);
      },
    },
    {
      path: '/buytoken/:id',
      name: 'BuyToken',
      onEnter:redirectToLogin,
      getComponent(nextState, cb) {
        const importModules = Promise.all([
          import('containers/BuyToken'),
        ]);

        const renderRoute = loadModule(cb);

        importModules.then(([component]) => {
          renderRoute(component);
        });

        importModules.catch(errorLoading);
      },
    },
    {
      path: '/wallet',
      name: 'Wallet',
      onEnter:redirectToLogin,
      getComponent(nextState, cb) {
        const importModules = Promise.all([
          import('containers/Wallet'),
        ]);

        const renderRoute = loadModule(cb);

        importModules.then(([component]) => {
          renderRoute(component);
        });

        importModules.catch(errorLoading);
      },
    },
     {
      path: '/auth',
      name: 'authPage',
      hideNavbar: true,
      getComponent(nextState, cb) {
        const importModules = Promise.all([
          import('containers/AuthPage/reducer'),
          import('containers/AuthPage/sagas'),
          import('containers/AuthPage'),
        ]);

        const renderRoute = loadModule(cb);

        importModules.then(([reducer, sagas, component]) => {
          injectReducer('authPage', reducer.default);
          injectSagas(sagas.default);
          renderRoute(component);
        });

        importModules.catch(errorLoading);
      },
    },
    {
     path: '/dashboard',
     name: 'Dashboard',
     onEnter:redirectToLogin,
     getComponent(nextState, cb) {
       const importModules = Promise.all([
         import('containers/Dashboard'),
       ]);

       const renderRoute = loadModule(cb);

       importModules.then(([component]) => {
         renderRoute(component);
       });

       importModules.catch(errorLoading);
     },
    },
    {
      path: '/comingsoon',
      name: 'ComingSoon',
      onEnter:redirectToLogin,
      getComponent(nextState, cb) {
        const importModules = Promise.all([
          import('containers/ComingSoon'),
        ]);
 
        const renderRoute = loadModule(cb);
 
        importModules.then(([component]) => {
          renderRoute(component);
        });
 
        importModules.catch(errorLoading);
      },
     },
     {
      path: '/newDashboard',
      name: 'NewDashboard',
      onEnter:redirectToLogin,
      getComponent(nextState, cb) {
        const importModules = Promise.all([
          import('containers/NewDashboard'),
        ]);
 
        const renderRoute = loadModule(cb);
 
        importModules.then(([component]) => {
          renderRoute(component);
        });
 
        importModules.catch(errorLoading);
      },
     }, 
    {
      path: '*',
      name: 'notfound',
      getComponent(nextState, cb) {
        import('containers/NotFoundPage')
          .then(loadModule(cb))
          .catch(errorLoading);
      },
    },
  ];
}
