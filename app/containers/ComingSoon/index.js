/**
*
* coming soon
*
*/

import React from 'react';
// import styled from 'styled-components';

import EsnavbarLoggedIn from 'components/EsnavbarLoggedIn';
import { Grid, Row, Col,Carousel,Clearfix, Button, Thumbnail } from 'react-bootstrap';


export default class ComingSoon extends React.Component{
    constructor(props) {
      super(props);
      this.state = { properties: []}
    }

    render(){
        return (
          <div>
          <EsnavbarLoggedIn style={{padding: '0', margin: '0'}}/>
          <Col xs={12} md={12} lg={12} style={{marginTop:"20%"}}>
            <Col xs={12} md={6} mdOffset={3}>
              <div><h2 style={{fontSize:"-webkit-xxx-large",textAlign:"-webkit-center"}}>Comming Soon ... </h2></div>
            </Col>
            <Clearfix />
          </Col>
          </div>
        );
    }
}