/*
 *
 * AuthPage actions
 *
 */

import {
  DEFAULT_ACTION,
  LOGIN
} from './constants';

export function defaultAction() {
  return {
    type: DEFAULT_ACTION,
  };
}

export function loginAction() {
  return {
    type: LOGIN,
  };
}
