/*
 *
 * AuthPage
 *
 */

import React, { PropTypes } from 'react';
import { connect } from 'react-redux';
import Helmet from 'react-helmet';
import { createStructuredSelector } from 'reselect';
import makeSelectAuthPage from './selectors';
import Wrapper from './wrapper';
import Signup from 'components/Signup';

export class AuthPage extends React.PureComponent { // eslint-disable-line react/prefer-stateless-function
  render() {
    return (
      <Wrapper>
        
        <Helmet
          title="SignIn / Signup"
          meta={[
            { name: 'description', content: 'Description of AuthPage' },
          ]}
        />
        <Signup />
      </Wrapper>
    );
  }
}

AuthPage.propTypes = {
  dispatch: PropTypes.func.isRequired,
};

const mapStateToProps = createStructuredSelector({
  AuthPage: makeSelectAuthPage(),
});

function mapDispatchToProps(dispatch) {
  return {
    dispatch,
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(AuthPage);
