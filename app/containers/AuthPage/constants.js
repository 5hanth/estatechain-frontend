/*
 *
 * AuthPage constants
 *
 */

export const DEFAULT_ACTION = 'app/AuthPage/DEFAULT_ACTION';

export const LOGIN = 'app/AuthPage/LOGIN';
