import styled from 'styled-components';
import Bg from './signupne.jpg';

const Wrapper = styled.div`
  width: 100%;
  height: auto;
  overflow: 'hidden';
  background: #fff;
`;

export default Wrapper;
