import styled from 'styled-components';
import Bg from './bg.png';
import MBg from './mobilelogo.png';

const Wrapper = styled.div`
  width: 100%;
  overflow: 'hidden';
  background: url(${Bg}) no-repeat center center;
  background-size: cover;
  @media only screen
    and (min-device-width : 320px)
    and (max-device-width : 1024px)
     {
      background: url(${MBg}) no-repeat center center;
      background-size: 100vw 51vh;
  }
`;
// background-position-y: -60px;
export default Wrapper;
