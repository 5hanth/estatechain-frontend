/*
 * HomePage
 *
 * This is the first thing users see of our App, at the '/' route
 *
 * NOTE: while this component should technically be a stateless functional
 * component (SFC), hot reloading does not currently support SFCs. If hot
 * reloading is not a necessity for you then you can refactor it and remove
 * the linting exception.
 */

import React from 'react';
import { FormattedMessage } from 'react-intl';
import messages from './messages';
import EsnavbarLoggedIn from 'components/EsnavbarLoggedIn';
import LogoSection  from 'components/LogoSection';
import WhySection  from 'components/WhySection';
import PropertiesSection  from 'components/PropertiesSection';
import FeaturesSection  from 'components/FeaturesSection';
import TestimonialSection  from 'components/TestimonialSection';
import ContactSection  from 'components/ContactSection';
import FooterSection  from 'components/FooterSection';
import { Row, Col } from 'react-bootstrap';
import Wrapper from './wrapper';

export default class HomePage extends React.PureComponent { // eslint-disable-line react/prefer-stateless-function
  constructor(props){
    super(props);
    this.state = { k:true};
  }
  render() {
    if(!this.state.k){
      return(<div>
        Something you wanted to show if the boolean was present
      </div>)
    }
    return (
      <Col>
      <EsnavbarLoggedIn />
      <Wrapper className="wrapMobile">
        <LogoSection></LogoSection>
      </Wrapper>
      <WhySection></WhySection>
      <PropertiesSection></PropertiesSection>
      <FeaturesSection></FeaturesSection>
      <TestimonialSection></TestimonialSection>
      <ContactSection></ContactSection>
      <FooterSection></FooterSection>
      </Col>
    )
  }
}
