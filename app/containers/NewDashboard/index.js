/*
 *
 * New Dashboard
 *
 */

import React from 'react';
import EsnavbarLoggedIn from 'components/EsnavbarLoggedIn';
import DashboardWalletSec from 'components/NewDashboardWalletSec';
import DashboardWalletInvest from 'components/NewDashboardWalletInvest';
import DashboardPortfolio from 'components/NewDashboardPortfolio';
import DashboardChart from 'components/NewDashboardChart';
import DashboardTokenPerformance from 'components/NewDashboardTokenPrfrm';
import DashboardTrnxHis from 'components/NewDashboardTrnxHis';
import FooterSection  from 'components/FooterSection';
import Trends  from 'components/Trends';
import { Row, Col,Nav,NavItem,Collapse,Well,Image, Clearfix } from 'react-bootstrap';
import { Wrapper, dashboardStyles } from './dashboardStyles';
import api from '../../utils/api';
import Bg from './rolling.svg';
import Icon from './icon.png';
import Pocket from './pocket.png';
import Wallet from './wallet.png';
import Pie from './pie.png';
import Line from './line.png';
import User from './user.png';

export default class NewDashboard extends React.PureComponent { // eslint-disable-line react/prefer-stateless-function
    constructor(props) {
        super(props);
        this.state = { portfolio:[],chart:[],wallet:[],transactions:[],AllTransaction:[],active:1,for1:true,for2:false,for3:false,
                        for4:false,for5:false,isLoad:true
                    };
        this.handleSelect = this.handleSelect.bind(this);
        this.handleBodyClick = this.handleBodyClick.bind(this);
    }
    componentDidMount() {
        api.get("portfolio",{}).then(resp => {
            if(resp.data.error===false){
                //console.log(resp.data);
                //ok = resp.data.property;
                this.setState({
                            portfolio: [resp.data]
                        })
            }
        })
        api.get("chart",{}).then(resp => {
            if(resp.data.error===false){
                //console.log(resp.data);
            //ok = resp.data.property;
                this.setState({
                    chart: [resp.data]
                })
            }
        })
        api.get("wallet",{}).then(resp => {
            if(resp.data.error===false){
                //console.log(resp.data);
            //ok = resp.data.property;
                this.setState({
                    wallet: [resp.data]
                })
            }
        })
        api.get("transactions",{}).then(resp => {
            if(resp.data.error===false){
                //console.log(resp.data);
            //ok = resp.data.property;
                this.setState({
                    AllTransaction:[resp.data],
                    transactions: [resp.data.properties]
                })
            }
        })
    }
    handleBodyClick(){
        this.setState({
            isLoad:true,
        })
    }
    handleSelect(selectedKey) {
       // alert(selectedKey);
        this.setState({
            for1:false,
            for2:false,
            for3:false,
            for4:false,
            for5:false,
        })
        if(selectedKey==1){
            this.setState({
                for1:true,
                isLoad:false,
                active:1,
            })
        }
        else if(selectedKey==2){
            this.setState({
                for2:true,
                isLoad:false,
                active:2,
            })
        }
        else if(selectedKey==3){
            this.setState({
                for3:true,
                isLoad:false,
                active:3,
            })
        }
        else if(selectedKey==4){
            this.setState({
                for4:true,
                isLoad:false,
                active:4,
            })
        }
        else if(selectedKey==5){
            this.setState({
                for5:true,
                isLoad:false,
                active:5,
            })
        }
      }

    render() {
        if(this.state.portfolio.length<1 || this.state.chart.length<1 || this.state.wallet.length<1 || this.state.transactions.length<1){
            return(
                <Row style={dashboardStyles.loading}>
                    <img src={Bg} style={dashboardStyles.loadImg}/>
                </Row>
            )
        }
        return (
            <Col sm={12} xs={12} className="row">
                <EsnavbarLoggedIn />
                <Wrapper onClick={this.handleBodyClick}>
                    <Col>
                        <div style={dashboardStyles.headerTitleContainer}>
                            <img src={Icon} href="#" style={dashboardStyles.icon} />
                            <h1  style={dashboardStyles.headerTitle}>My Dashboard</h1>
                        </div>
                        <p style={dashboardStyles.subHead}>Manage your account and property tokens here</p>
                        <Trends style={dashboardStyles.trend}/>
                    </Col>
                </Wrapper>
                <Col sm={1} md={1} lg={1} style={{marginTop:'25px',paddingLeft:'15px'}}>
                    <Nav bsStyle="tabs" stacked activeKey={this.state.active} onSelect={this.handleSelect} style={dashboardStyles.verticalTab}>
                        <NavItem eventKey={1}><Image src={Pocket} responsive/> </NavItem>
                        <NavItem eventKey={2}><Image src={Wallet} responsive/> </NavItem>
                        <NavItem eventKey={3}><Image src={Pie} responsive/> </NavItem>
                        <NavItem eventKey={4}><Image src={Line} responsive/> </NavItem>
                        <NavItem eventKey={5}><Image src={User} responsive/> </NavItem>
                    </Nav>
                </Col>
                <Col sm={2} md={2} lg={2} style={{marginLeft:'-1%',marginTop:'25px',paddingLeft:'6px'}} className={this.state.for1 && !this.state.isLoad ? 'collapse in':'collapse'}>
                    <Col sm={2} style={{position:'fixed',height:'100vh',backgroundColor:'#223d60',color:'white'}}>
                        <h3 style={{fontSize:'17px',textAlign:'center',marginTop:'35px'}} >Wallet Details</h3>
                        <p style={dashboardStyles.seperateLine}></p>
                        <h4 style={{textAlign:'left',paddingBottom:'20px',fontWeight:'100',fontSize:'16px',lineHeight:'inherit'}}>Total account value as sum of your wallet balance and value of your token bought.</h4>
                        <h5 style={{paddingBottom:'10px',fontWeight:'100',fontSize:'16px'}}>Account Balance : {this.state.wallet[0].wallet.total_tokens_value}</h5>
                        <h5 style={{paddingBottom:'10px',fontWeight:'100',fontSize:'16px'}}>Wallet Balance : {this.state.wallet[0].wallet.tokens_value}</h5>
                    </Col>
                </Col>
                <Col sm={2} md={2} lg={2} style={{marginLeft:'-1%',marginTop:'25px',paddingLeft:'6px'}} className={this.state.for2 && !this.state.isLoad ? 'collapse in':'collapse'}>
                    <Col sm={2} style={{position:'fixed',height:'100vh',backgroundColor:'#223d60',color:'white'}}>
                        <h3 style={{fontSize:'17px',textAlign:'center',marginTop:'35px'}} >Transaction History</h3>
                        <p style={dashboardStyles.seperateLine}></p>
                        <h4 style={{textAlign:'left',paddingBottom:'20px',fontWeight:'100',fontSize:'16px',lineHeight:'inherit'}}>You have made {this.state.transactions[0].length} transactions till now.</h4>
                        <h5 style={{paddingBottom:'10px',fontWeight:'100',fontSize:'16px'}}>Last transaction made on date :</h5>
                        <h5 style={{paddingBottom:'10px',fontWeight:'100',fontSize:'16px'}}></h5>
                    </Col>
                </Col>
                <Col sm={2} md={2} lg={2} style={{marginLeft:'-1%',marginTop:'25px',paddingLeft:'6px'}} className={this.state.for3 && !this.state.isLoad ? 'collapse in':'collapse'}>
                    <Col sm={2} style={{position:'fixed',height:'100vh',backgroundColor:'#223d60',color:'white'}}>
                        <h3 style={{fontSize:'17px',textAlign:'center',marginTop:'35px'}} >Portfolio Details</h3>
                        <p style={dashboardStyles.seperateLine}></p>
                        <h4 style={{textAlign:'left',paddingBottom:'20px',lineHeight:'inherit',fontWeight:'100',fontSize:'16px'}}>Your investment portfolio distribution with detailed information of token's value.</h4>
                        <h5 style={{paddingBottom:'10px',fontWeight:'100',fontSize:'16px'}}>You have {this.state.wallet[0].wallet.tokens} tokens till now.</h5>
                    </Col>
                </Col>
                <Col sm={2} md={2} lg={2} style={{marginLeft:'-1%',marginTop:'25px',paddingLeft:'6px'}} className={this.state.for4 && !this.state.isLoad ? 'collapse in':'collapse'}>
                    <Col sm={2} style={{position:'fixed',height:'100vh',backgroundColor:'#223d60',color:'white'}}>
                        <h3 style={{fontSize:'17px',textAlign:'center',marginTop:'15px'}} >Performance Of Tokens</h3>
                        <p style={dashboardStyles.seperateLine}></p>
                        <h4 style={{textAlign:'left',paddingBottom:'20px',fontWeight:'100',lineHeight:'inherit',fontSize:'16px'}}>Track performance of your tokens over the time.</h4>
                        <h5 style={{paddingBottom:'10px',fontWeight:'100',fontSize:'16px'}}>Growth : %</h5>
                        
                    </Col>
                </Col>
                <Col sm={2} md={2} lg={2} style={{marginLeft:'-1%',marginTop:'25px',paddingLeft:'6px'}} className={this.state.for5 && !this.state.isLoad ? 'collapse in':'collapse'}>
                    <Col sm={2} style={{position:'fixed',height:'100vh',backgroundColor:'#223d60',color:'white'}}>
                        <h3 style={{fontSize:'17px',textAlign:'center',marginTop:'35px'}} >Referral</h3>
                        <p style={dashboardStyles.seperateLine}></p>
                        {/* <h4 style={{textAlign:'left',paddingBottom:'20px',fontWeight:'100',lineHeight:'inherit',fontSize:'16px'}}>555555.</h4> */}
                        {/* <h5 style={{paddingBottom:'10px',fontWeight:'100',fontSize:'16px'}}>Wallet Balance : {this.state.wallet[0].wallet.tokens_value}</h5> */}
                    </Col>
                </Col>
                <Col sm={(this.state.for1||this.state.for2||this.state.for3||this.state.for4||this.state.for5)&&!this.state.isLoad?9:11} 
                    md={(this.state.for1||this.state.for2||this.state.for3||this.state.for4||this.state.for5)&&!this.state.isLoad?9:11}
                    lg={(this.state.for1||this.state.for2||this.state.for3||this.state.for4||this.state.for5)&&!this.state.isLoad?9:11}
                    style={{marginTop:'10px',height: '415px'}} onClick={this.handleBodyClick}>
                    <Collapse in={this.state.for1} style={{paddingTop:"15px",paddingBottom:"15px"}}>
                        <div>
                            <Col xs={12} md={5} style={dashboardStyles.custmWell} style={{width:'46.8%'}}>
                                <DashboardWalletSec wallet={this.state.wallet}/>
                            </Col>
                            <Col xs={12} md={1} style={{width:'2%'}}></Col>
                            <Col xs={12} md={6}>
                                <DashboardWalletInvest invest={this.state.portfolio} wallet={this.state.wallet}/>
                            </Col>
                        </div>
                    </Collapse>
                    <Collapse in={this.state.for2} style={{paddingTop:"15px"}}>
                        <Col xs={12} md={12} lg={12} style={dashboardStyles.custmWell}>
                            <DashboardTrnxHis transactions={this.state.transactions}/>
                        </Col>
                    </Collapse>
                    <Collapse in={this.state.for3} style={{paddingTop:"15px"}}>
                    <div>
                        <Col xs={12} md={4} style={dashboardStyles.custmWell}>
                            <DashboardChart chart={this.state.chart}/>
                        </Col>
                        <Col xs={12} md={8} >
                            <DashboardPortfolio style={dashboardStyles.shadow} portfolio={this.state.portfolio}/>
                        </Col>
                    </div>
                    </Collapse>
                    <Collapse in={this.state.for4} style={{paddingTop:"15px"}}>
                        <div>
                            <Col xs={12} md={12} style={dashboardStyles.custmWell}>
                                <DashboardTokenPerformance portfolio={this.state.portfolio} />
                            </Col>
                        </div>
                    </Collapse>
                    <Collapse in={this.state.for5} style={{paddingTop:"15px"}}>
                        <div>
                            <Col xs={12} md={12} style={dashboardStyles.custmWell}>
                                5
                            </Col>
                        </div>
                    </Collapse>
                </Col>
            </Col>
        )
    }
}