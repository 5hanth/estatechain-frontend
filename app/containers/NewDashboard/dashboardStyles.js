import styled from 'styled-components';
import Bg from './wrap-bg.png';
    export const dashboardStyles = {
        loading:{
            position:'fixed',
            top:'0px',
            //margin:'0px 0px',
            width:'100%',
            height:'100%',
            background:'linear-gradient(135deg, rgb(66, 92, 122) 0%, rgb(53, 80, 112) 25%, rgb(32,52, 79) 75%,  rgb(18, 34, 55) 100%)',
            //background-image:url('ajax-loader.gif');
            backgroundRepeat:'no-repeat',
            backgroundPosition:'center',
            zIndex:'999',
            opacity: '0.6',
            filter: 'alpha(opacity=40)', /* For IE8 and earlier */
        },
        headerTitle : {
            fontFamily: 'Open Sans',
            letterspacing: '0.8px',
            fontSize: '30px',
            color: '#fff',
            lineHeight:'0.5',
            margin: '6px 16px',
            fontWeight: '100'
        },
        icon :{
            objectfit: 'contain',
            height:'37px',
            width:'46px',
        },
        headerTitleContainer : {
            display: "flex",
            flexDirection: "Row",
            justifyContent: "center",
            paddingTop: '15px'
        },
        subHead:{
            fontfamily: 'SegoeUI',
            fontSize: '13px',
            textAlign: 'center',
            margin:'0',
            color:'#fff',
            fontWeight: '100'
        },
        trend:{
            margin:'30px 10px'
        },
        loadImg:{
            margin:'25% 45%',
        },
        verticalTab:{
            width:"max-content",
            height:"100vh",
            position:"fixed",
            width:'7%',
        },
        custmWell:{
            background:"#FFF",
            boxShadow:'none',
            border:"0px",
            borderRadius:"0px"
        },
        seperateLine:{
            width: 'auto',
            height: '1px',
            backgroundColor: '#959595',
            border: 'solid 1px #959595',
            margin:'32px 0px',
        },
    }
    export const Wrapper = styled.div`
    width: auto;
    height:auto;
    overflow: hidden;
    margin :120px 0px 14px 15px;
    background: url(${Bg}) no-repeat center center;
    background-size: cover;
    @media only screen
      and (min-device-width : 320px)
      and (max-device-width : 1024px)
       {
         margin : 8vh auto auto;
       }
    `;