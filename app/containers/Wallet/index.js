/*
 *
 * Wallet
 *
 */

import React, { PropTypes } from 'react';
import EsnavbarLoggedIn from 'components/EsnavbarLoggedIn';
import WalletHeader  from 'components/WalletHeader';
import BalanceTile  from 'components/BalanceTile';
import Loading  from 'components/Loading';
import {Row,Col} from 'react-bootstrap';
import api from '../../utils/api';

export default class Wallet extends React.Component {
  constructor(props){
      super(props);
      this.state = {balance:[], wallet: {}};
  }

  componentDidMount() {

      api.get("wallet",{}).then(resp => {
        console.log(resp.data.property)
        if(resp.data.error===false){
          this.setState({
            wallet: resp.data.wallet
          })
          var wallet = resp.data.wallet
          this.setState({
            balance: [
              {currency:'BTC', name: 'BTC', value: ((wallet.tokens_value_raw || 1000) / 7000).toFixed(4)},
              {currency:'$' , name: 'Dollars', value: (wallet.tokens_value_raw || 1000) },
              {currency:'ETH' , name: 'ETH', value: ((wallet.tokens_value_raw || 1000) / 300).toFixed(4)},
              {currency:'EST' , name: 'Estatechain Token', value: ((wallet.tokens_value_raw || 1000)  / 4.31).toFixed(4)}
              ]
          })
        }
      })
  }

  render() {
    if(!this.state.wallet.user_id || !this.state.balance.length) {
      return <Loading />
    }
    return (
      <Col>
      <EsnavbarLoggedIn />
      <WalletHeader balance={this.state.balance}></WalletHeader>
      <BalanceTile balance={this.state.balance}></BalanceTile>
      </Col>
    );
  }
}
