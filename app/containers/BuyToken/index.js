/*
 *
 * Buy Token
 *
 */
import React from 'react';
import EsnavbarLoggedIn from 'components/EsnavbarLoggedIn';
import BuyTokenHeader from 'components/BuyTokenHeader';
import BuyTokenDetail from 'components/BuyTokenDetail';
import FooterSection  from 'components/FooterSection';
import { Row, Col } from 'react-bootstrap';
import {styles} from './style.js';
import api from '../../utils/api';
import Bg from './rolling.svg';

export default class BuyToken extends React.Component { // eslint-disable-line react/prefer-stateless-function
  constructor(props){
      super(props);
      this.state = {id:'', property: {}, wallet: {}};
  }

  componentDidMount() {
      var uri = window.location.href;
      var lastslashindex = uri.lastIndexOf('/');
      console.log(uri)
      var id = uri.substring(lastslashindex  + 1);
      this.setState({
          id: id
      })

      api.get("properties/"+id,{}).then(resp => {
        console.log(resp.data.property)
        if(resp.data.error===false){
          this.setState({
            property: resp.data.property
          })
        }
      })
      api.get("wallet",{}).then(resp => {
        console.log(resp.data.property)
        if(resp.data.error===false){
          this.setState({
            wallet: resp.data.wallet
          })
        }
      })
  }

  render() {
    if(!this.state.property.id || !this.state.wallet.user_id) {
      return(
          <Row style={styles.loading}>
              <img src={Bg} style={styles.loadImg}/>
          </Row>
      )
    }
    return (
        <Col style={styles.mainContainer}>
            <EsnavbarLoggedIn />

            <BuyTokenHeader property={this.state.property}/>
            <Row style={styles.insideData} className="buyDetailContainer">
                <Col sm={12}>
                    <BuyTokenDetail property={this.state.property} wallet={this.state.wallet}/>
                </Col>
            </Row>
            <FooterSection></FooterSection>
        </Col>
      )
  }
}
