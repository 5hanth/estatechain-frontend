/**
 *
 * App.react.js
 *
 * This component is the skeleton around the actual pages, and should only
 * contain code that should be seen on all pages. (e.g. navigation bar)
 *
 * NOTE: while this component should technically be a stateless functional
 * component (SFC), hot reloading does not currently support SFCs. If hot
 * reloading is not a necessity for you then you can refactor it and remove
 * the linting exception.
 */

import React from 'react';
import { Row, Col } from 'react-bootstrap';
import fixNav from './fixNav';
import Esnavbar from 'components/Esnavbar';

export default class App extends React.PureComponent { // eslint-disable-line react/prefer-stateless-function

  static propTypes = {
    children: React.PropTypes.node,
  };

  navbar() {
    if(!this.props.children.props.route.hideNavbar) {
      return <div>
        <Esnavbar />
        <span style={fixNav}></span>
      </div>
     }
  }

  render() {
    return (
        <Col>
          {React.Children.toArray(this.props.children)}
        </Col>
    );
  }
}
