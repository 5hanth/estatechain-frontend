
import { fromJS } from 'immutable';
import propertyDetailReducer from '../reducer';

describe('propertyDetailReducer', () => {
  it('returns the initial state', () => {
    expect(propertyDetailReducer(undefined, {})).toEqual(fromJS({}));
  });
});
