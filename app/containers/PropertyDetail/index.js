/*
 *
 * PropertyDetail
 *
 */

import React, { PropTypes } from 'react';
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';
import makeSelectPropertyDetail from './selectors';

import EsnavbarLoggedIn from 'components/EsnavbarLoggedIn';
import PropertyDetailHeader  from 'components/PropertyDetailHeader';
import DetailCarousel  from 'components/DetailCarousel';
import DetailInfo  from 'components/DetailInfo';
import F4Map  from 'components/F4Map';
import DetailAbout  from 'components/DetailAbout';
import {Row,Col} from 'react-bootstrap';
import api from '../../utils/api';
import Bg from './rolling.svg';
import Style from './style.js';

export default class PropertyDetail extends React.PureComponent { // eslint-disable-line react/prefer-stateless-function

  constructor(props) {
    super(props);
    var threeD = props.params.id === '3dplatformadmin';
    this.state = { properties:[], id: threeD ? 1 : props.params.id, threeD: threeD };
  }
  componentDidMount() {

    api.get("properties/"+this.state.id,{}).then(resp => {
      if(resp.data.error===false){

    //ok = resp.data.property;
      this.setState({
                properties: [resp.data.property]
              })
      }
    })
  }

  render() {

    //console.log(ok.length);
    if(this.state.properties.length<1){
      return(
        <Row style={Style.loading}>
          <img src={Bg} style={Style.loadImg}/>
        </Row>
      );
    }
    return (
      <Col>
        <EsnavbarLoggedIn />
        <PropertyDetailHeader property={this.state.properties}></PropertyDetailHeader>
        <DetailCarousel property={this.state.properties}/>
        { this.state.threeD ? <F4Map /> : <div></div> }
        <DetailInfo property={this.state.properties}/>
        <DetailAbout property={this.state.properties}/>
      </Col>
    );
  }

}
