import { createSelector } from 'reselect';

/**
 * Direct selector to the propertyDetail state domain
 */
const selectPropertyDetailDomain = () => (state) => state.get('propertyDetail');

/**
 * Other specific selectors
 */


/**
 * Default selector used by PropertyDetail
 */

const makeSelectPropertyDetail = () => createSelector(
  selectPropertyDetailDomain(),
  (substate) => substate.toJS()
);

export default makeSelectPropertyDetail;
export {
  selectPropertyDetailDomain,
};
