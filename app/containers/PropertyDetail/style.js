import styled from 'styled-components';

const Style = {
loading:{
    position:'fixed',
    top:'0px',
    width:'100%',
    height:'100%',
    background:'linear-gradient(135deg, rgb(66, 92, 122) 0%, rgb(53, 80, 112) 25%, rgb(32,52, 79) 75%,  rgb(18, 34, 55) 100%)',
    //background-image:url('ajax-loader.gif');
    backgroundRepeat:'no-repeat',
    backgroundPosition:'center',
    zIndex:'999',
    opacity: '0.6',
    filter: 'alpha(opacity=40)', /* For IE8 and earlier */
  },
  loadImg:{
    margin:'25% 45%',
  },
}

export default Style;