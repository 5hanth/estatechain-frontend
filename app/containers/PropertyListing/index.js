/*
 * PropertyListing
 *
 *
 */

import React from 'react';
import EsnavbarLoggedIn from 'components/EsnavbarLoggedIn';
import PropertyListHeader  from 'components/PropertyListHeader';
import {Row,Col,Clearfix,Jumbotron} from 'react-bootstrap';

export default class PropertyListing extends React.PureComponent { // eslint-disable-line react/prefer-stateless-function
  render() {
    return (
      <div>
        <EsnavbarLoggedIn />
        <Jumbotron>
          <PropertyListHeader></PropertyListHeader>
        </Jumbotron>
        <Clearfix />
      </div>
    )
  }
}
