/*
 *
 * Dashboard
 *
 */
import React from 'react';
import EsnavbarLoggedIn from 'components/EsnavbarLoggedIn';
import DashboardWalletSec from 'components/DashboardWalletSec';
import DashboardWalletInvest from 'components/DashboardWalletInvest';
import DashboardPortfolio from 'components/DashboardPortfolio';
import DashboardChart from 'components/DashboardChart';
import DashboardTrnxHis from 'components/DashboardTrnxHis';
import FooterSection  from 'components/FooterSection';
import Trends  from 'components/Trends';
import { Row, Col } from 'react-bootstrap';
import Icon from './icon.png';
import { Wrapper, dashboardStyles } from './dashboardStyles';
import api from '../../utils/api';
import Bg from './rolling.svg';

export default class Dashboard extends React.PureComponent { // eslint-disable-line react/prefer-stateless-function
    constructor(props) {
        super(props);
        this.state = { portfolio:[],chart:[],wallet:[],transactions:[] };
    }
    componentDidMount() {
        api.get("portfolio",{}).then(resp => {
            if(resp.data.error===false){
                //console.log(resp.data);
                //ok = resp.data.property;
                this.setState({
                            portfolio: [resp.data]
                        })
            }
        })
        api.get("chart",{}).then(resp => {
            if(resp.data.error===false){
                //console.log(resp.data);
            //ok = resp.data.property;
                this.setState({
                    chart: [resp.data]
                })
            }
        })
        api.get("wallet",{}).then(resp => {
            if(resp.data.error===false){
                //console.log(resp.data);
            //ok = resp.data.property;
                this.setState({
                    wallet: [resp.data]
                })
            }
        })
        api.get("transactions",{}).then(resp => {
            if(resp.data.error===false){
                //console.log(resp.data);
            //ok = resp.data.property;
                this.setState({
                    transactions: [resp.data.properties]
                })
            }
        })
    }

    render() {
        if(this.state.portfolio.length<1 || this.state.chart.length<1 || this.state.wallet.length<1 || this.state.transactions.length<1){
            return(
                <Row style={dashboardStyles.loading}>
                    <img src={Bg} style={dashboardStyles.loadImg}/>
                </Row>
            )
        }

        return (
            <Col>
                <EsnavbarLoggedIn />
                <Wrapper>
                    <Col>
                        <div style={dashboardStyles.headerTitleContainer}>
                            <img src={Icon} href="#" style={dashboardStyles.icon} />
                            <h1  style={dashboardStyles.headerTitle}>My Dashboard</h1>
                        </div>
                        <p style={dashboardStyles.subHead}>Manage your account and property tokens here</p>
                        <Trends style={dashboardStyles.trend}/>
                    </Col>
                </Wrapper>
                <Row style={dashboardStyles.insideData} className="noMarginMobile">
                    <Col sm={3} md={2} style={dashboardStyles.shadow} className="noPadMobile"><DashboardWalletSec wallet={this.state.wallet}/></Col>
                    <Col sm={4} md={5} style={dashboardStyles.walletSec} className="noPadMobile">
                        <Col sm={12} style={dashboardStyles.shadow}>
                            <DashboardWalletInvest invest={this.state.portfolio} wallet={this.state.wallet}/>
                        </Col>
                        <Col sm={12} style={dashboardStyles.shadow}>
                            <DashboardTrnxHis transactions={this.state.transactions}/>
                        </Col>
                    </Col>
                    <Col sm={4} md={5}>
                      <DashboardPortfolio style={dashboardStyles.shadow} portfolio={this.state.portfolio}/>
                      <Row style={dashboardStyles.ChartBg}>
                        <DashboardChart chart={this.state.chart}/>
                      </Row>
                    </Col>
                </Row>
                <FooterSection />
            </Col>
        )
    }
}
