/*
 *
 * ICO
 *
 */
import React from 'react';
import EsnavbarLoggedIn from 'components/EsnavbarLoggedIn';
import DashboardWalletSec from 'components/DashboardWalletSec';
import DashboardWalletInvest from 'components/DashboardWalletInvest';
import FlipClock from 'components/FlipClock';
import FooterSection  from 'components/FooterSection';
import { Row, Col,Image,Clearfix } from 'react-bootstrap';
import Building from './building.png'
import { Wrapper, style } from './style';
import api from '../../utils/api';
// import Bg from './rolling.svg';

export default class Ico extends React.PureComponent { // eslint-disable-line react/prefer-stateless-function
    constructor(props) {
        super(props);
    }

    componentDidMount() {
    }
    render() {
        return(
            <Col>
                <EsnavbarLoggedIn />
                <Wrapper>
                    <Col sm={12} xs={12} style={style.wrapHeading}>
                        <h2>The Future Of Real Estate CroudFunding is here!</h2>
                    </Col>
                    <Col xs={12} sm={4}>
                        <FlipClock />
                    </Col>
                    <Col xs={12} sm={8}>
                        <Image src={Building} responsive style={style.buildingStyle}/>
                        <h4 className="tiltedText">$10 M Received</h4>
                        <h4 className="tiltedText2">10,12,212 Token Sold</h4>
                    </Col>
                </Wrapper>
                <FooterSection />
            </Col>
        )
    }
}