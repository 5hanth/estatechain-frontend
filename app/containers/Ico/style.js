import styled from 'styled-components';
import Bg from './bg.png';
export const Wrapper = styled.div`
overflow: hidden;
height:-webkit-fill-available;
width:auto;
background-size: cover;
background-image: url(${Bg}),linear-gradient(135deg, rgb(66, 92, 122) 0%, rgb(53, 80, 112) 25%, rgb(32,52, 79) 75%,  rgb(18, 34, 55) 100%);
`;

export const style ={
    wrapHeading:{marginTop:'10%',textAlign: 'center',color: '#FFF'},
    buildingStyle:{marginTop:'5%',position: 'relative',},
}
//background: url(${Bg}) no-repeat center center;