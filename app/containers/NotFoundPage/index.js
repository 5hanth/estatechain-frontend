/**
 * NotFoundPage
 *
 * This is the page we show when the user visits a url that doesn't have a route
 *
 * NOTE: while this component should technically be a stateless functional
 * component (SFC), hot reloading does not currently support SFCs. If hot
 * reloading is not a necessity for you then you can refactor it and remove
 * the linting exception.
 */

import React from 'react';
import { FormattedMessage } from 'react-intl';
import EsnavbarLoggedIn from 'components/EsnavbarLoggedIn';
import { Row, Col,Image } from 'react-bootstrap';
import styled from 'styled-components';
import Bg from './wrap-bg.png';
import nf from './nf.png';

export const Wrapper = styled.div`
width: auto;
height:132px;
overflow: hidden;
margin :120px 46px 14px 46px;
background: url(${Bg}) no-repeat center center;
background-size: cover;
`;

export default class NotFound extends React.PureComponent { // eslint-disable-line react/prefer-stateless-function
  render() {
    return (
      <Col sm={12} style={{padding: '0', margin: '0'}}>

        <EsnavbarLoggedIn style={{padding: '0', margin: '0'}}/>
        <Image src={nf} responsive style={{position:"fixed"}}/>

      </Col>
    );
  }
}
