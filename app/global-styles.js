import { injectGlobal } from 'styled-components';

/* eslint no-unused-expressions: 0 */
injectGlobal`
  html,
  body {
    height: 100%;
    width: 100%;
  }

  body {
    font-family: 'Open Sans', 'Helvetica Neue', Helvetica, Arial, sans-serif;
  }

  body.fontLoaded {
    font-family: 'Open Sans', 'Open Sans', 'Helvetica Neue', Helvetica, Arial, sans-serif;
  }
  .center-align {
    text-align: center;
  }
  #app {
    background-color: #e9e7e5;
    min-width: 100%;
  }

  @import url("https://fonts.googleapis.com/css?family=Droid+Sans+Mono");
* {
  box-sizing: border-box;
}

body {
  margin: 0;
}

#socialIcons a {
  cursor: pointer;
}

/* Portrait and Landscape */
@media only screen
  and (min-device-width : 320px)
  and (max-device-width : 1024px)
   {
     .noMarginMobile {
       margin: 0 !important;
     }
     .hide-mobile {
       display: none !important;
     }
     #walletNavHeight {
       overflow: scroll;
       height: 55vh !important;
     }
     #tokensNav {
       height: 250px;
     }
    .navbar-collapse {
      height: 100% !important;
      background-color: #29405c !important;
    }
    .navbar-header {
      height: 8vh !important;
    }
    .footerRow {
      margin-right: 0 !important;
    }
    #contactWrapper {
      height: 120vh;
    }
    #contactSection {
      margin: 0px 5% !important;
    }
    .howCol {
      margin: 15px 0;
    }
    .landingTitle {
      margin: 10px 0 30px !important;
      font-size: 25px !important;
    }
    #plusCol {
      padding: 0 !important;
      text-align: center;
    }
    #logoSectionWrap {
      height: 60vh !important;
    }
    #propSecRow {
      margin: 60vh 0 0 0 !important;
      background: linear-gradient(135deg, rgb(66, 92, 122) 0%, rgb(53, 80, 112) 25%, rgb(32,52, 79) 75%, rgb(18, 34, 55) 100%);
      width: 100%;
      padding: 10px 0;
      border-radius: 10px;
    }
    #seeTitle {
      color: rgb(74, 74, 74) !important;
    }
    .text-center {
      text-align: center !important;
      margin: auto !important;
      width: auto !important;
    }
    .footerRow {
      padding: 0px 0 !important;
      margin: auto;
      text-align: center;
    }
    .footerImg {
      margin: 0 0 25px;
    }
    #socialIcons {
      margin: 20px 0 0;
      text-align: center;
    }
    #checkWrap {
      padding: 10px 0px 20px !important;
      margin: 0px 5px 10px !important;
    }
    #chartWrap {
      height: auto !important;
    }
    .mobileMarginWrap {
      margin-left: 0px !important;
      margin-right: 0px !important;
    }
    .mobileLocationWrap {
      margin: auto 15px !important;
    }
    .cardMobileWrap {
      padding: 10px 0 !important;
    }
    .tokenPrice {
      margin: 15px 20vw 0 !important;
    }
    .mobileInline {
      display: inline-flex !important;
    }
    #eROI {
      margin: auto auto 15px !important;
    }
    .noPadMobile {
      padding: 0 !important;
    }
    #navImgOut {
      height: 7vh;
    }
    .navbar {
      height: 8vh !important;
    }
}

#root {
  display: -webkit-box;
  display: -ms-flexbox;
  display: flex;
  position: relative;
  width: 100%;
  min-height: 100vh;
  -webkit-box-pack: center;
      -ms-flex-pack: center;
          justify-content: center;
  -webkit-box-align: center;
      -ms-flex-align: center;
          align-items: center;
  background-color: #FBAB7E;
  background-image: -webkit-linear-gradient(28deg, #FBAB7E 0%, #F7CE68 100%);
  background-image: linear-gradient(62deg, #FBAB7E 0%, #F7CE68 100%);
}

header {
  display: -webkit-box;
  display: -ms-flexbox;
  display: flex;
  position: relative;
}
header h1 {
  font-family: "Droid Sans Mono", monospace;
  font-weight: lighter;
  text-transform: uppercase;
  letter-spacing: 0.1em;
  color: white;
}

.flipClock {
  display: -webkit-box;
  display: -ms-flexbox;
  display: flex;
  -webkit-box-pack: justify;
      -ms-flex-pack: justify;
          justify-content: space-between;
  width: 500px;
}

.flipUnitContainer {
  display: block;
  position: relative;
  width: 140px;
  height: 120px;
  -webkit-perspective-origin: 50% 50%;
          perspective-origin: 50% 50%;
  -webkit-perspective: 300px;
          perspective: 300px;
  background-color: white;
  border-radius: 3px;
  box-shadow: 0px 10px 10px -10px grey;
}

.upperCard, .lowerCard {
  display: -webkit-box;
  display: -ms-flexbox;
  display: flex;
  position: relative;
  -webkit-box-pack: center;
      -ms-flex-pack: center;
          justify-content: center;
  width: 100%;
  height: 50%;
  overflow: hidden;
  border: 1px solid whitesmoke;
}
.upperCard span, .lowerCard span {
  font-size: 5em;
  font-family: "Droid Sans Mono", monospace;
  font-weight: lighter;
  color: #333333;
}

.upperCard {
  -webkit-box-align: end;
      -ms-flex-align: end;
          align-items: flex-end;
  border-bottom: 0.5px solid whitesmoke;
  border-top-left-radius: 3px;
  border-top-right-radius: 3px;
}
.upperCard span {
  -webkit-transform: translateY(50%);
          transform: translateY(50%);
}

.lowerCard {
  -webkit-box-align: start;
      -ms-flex-align: start;
          align-items: flex-start;
  border-top: 0.5px solid whitesmoke;
  border-bottom-left-radius: 3px;
  border-bottom-right-radius: 3px;
}
.lowerCard span {
  -webkit-transform: translateY(-50%);
          transform: translateY(-50%);
}

.flipCard {
  display: -webkit-box;
  display: -ms-flexbox;
  display: flex;
  -webkit-box-pack: center;
      -ms-flex-pack: center;
          justify-content: center;
  position: absolute;
  left: 0;
  width: 140px;
  height: 60px;
  overflow: hidden;
  -webkit-backface-visibility: hidden;
          backface-visibility: hidden;
}
.flipCard span {
  font-family: "Droid Sans Mono", monospace;
  font-size: 5em;
  font-weight: lighter;
  color: #333333;
}
.flipCard.unfold {
  top: 50%;
  -webkit-box-align: start;
      -ms-flex-align: start;
          align-items: flex-start;
  -webkit-transform-origin: 50% 0%;
          transform-origin: 50% 0%;
  -webkit-transform: rotateX(180deg);
          transform: rotateX(180deg);
  background-color: white;
  border-bottom-left-radius: 3px;
  border-bottom-right-radius: 3px;
  border: 1px solid whitesmoke;
  border-top: 0.5px solid whitesmoke;
}
.flipCard.unfold span {
  -webkit-transform: translateY(-50%);
          transform: translateY(-50%);
}
.flipCard.fold {
  top: 0%;
  -webkit-box-align: end;
      -ms-flex-align: end;
          align-items: flex-end;
  -webkit-transform-origin: 50% 100%;
          transform-origin: 50% 100%;
  -webkit-transform: rotateX(0deg);
          transform: rotateX(0deg);
  background-color: white;
  border-top-left-radius: 3px;
  border-top-right-radius: 3px;
  border: 0.5px solid whitesmoke;
  border-bottom: 0.5px solid whitesmoke;
}
.flipCard.fold span {
  -webkit-transform: translateY(50%);
          transform: translateY(50%);
}

.fold {
  -webkit-animation: fold 0.6s cubic-bezier(0.455, 0.03, 0.515, 0.955) 0s 1 normal forwards;
          animation: fold 0.6s cubic-bezier(0.455, 0.03, 0.515, 0.955) 0s 1 normal forwards;
  -webkit-transform-style: preserve-3d;
          transform-style: preserve-3d;
}

.unfold {
  -webkit-animation: unfold 0.6s cubic-bezier(0.455, 0.03, 0.515, 0.955) 0s 1 normal forwards;
          animation: unfold 0.6s cubic-bezier(0.455, 0.03, 0.515, 0.955) 0s 1 normal forwards;
  -webkit-transform-style: preserve-3d;
          transform-style: preserve-3d;
}

@-webkit-keyframes fold {
  0% {
    -webkit-transform: rotateX(0deg);
            transform: rotateX(0deg);
  }
  100% {
    -webkit-transform: rotateX(-180deg);
            transform: rotateX(-180deg);
  }
}

@keyframes fold {
  0% {
    -webkit-transform: rotateX(0deg);
            transform: rotateX(0deg);
  }
  100% {
    -webkit-transform: rotateX(-180deg);
            transform: rotateX(-180deg);
  }
}
@-webkit-keyframes unfold {
  0% {
    -webkit-transform: rotateX(180deg);
            transform: rotateX(180deg);
  }
  100% {
    -webkit-transform: rotateX(0deg);
            transform: rotateX(0deg);
  }
}
@keyframes unfold {
  0% {
    -webkit-transform: rotateX(180deg);
            transform: rotateX(180deg);
  }
  100% {
    -webkit-transform: rotateX(0deg);
            transform: rotateX(0deg);
  }
}


  p,
  label {
    font-family: 'Open Sans', 'Helvetica Neue', Helvetica, Arial, sans-serif;
    line-height: 1.5em;
  }

  .navbar-default {
    border-radius: 0px;
    box-shadow: inset 0 0px 0 #fff, 0 2px 0px rgba(255,255,255,0.6);
    padding: 20px 20px 5vh 10vw;
    width: 95%;
    margin: 0 3vw;
    font-size: 17px;
    height: 98px;
  }

  .navbar-nav>li>.dropdown-menu {
    color: #fff;
    background: transparent;
  }

  .navbar-inverse .navbar-nav>li>a, .dropdown-menu>li>a {
    color: #fff;
    padding: 10px 35px;
  }
  .navbar>.container .navbar-brand{
    padding:7px; !important;
    margin-left:auto;
  }

  .navbar-inverse .navbar-nav>li>a:hover,.navbar-inverse .navbar-nav>li>a:focus {
    color: #fff;
    border-Bottom: 1px solid white;
    height: calc(100%-2px);
    width: calc(100%-2px);
    background:transparent;
  }
  .dropdown-menu>li>a:hover {
    color: #fff;
    border-radius: 5px;
    height: calc(100%-2px);
    width: calc(100%-2px);
  }
  .navbar-inverse .container{
    width:100%;
  }

  .thumbnail {
    padding: 0px;
  }

  #login-btn {
    text-transform: uppercase;
  }

  #featuresSection p {
    font-size: 14px;
  }

  .hide-scrollbar::-webkit-scrollbar {
    display: none;
  }

  #featuresSection {
    margin: 60px 0;
  }

  #featuresSection h1 {
    font-size: 18px;
    font-weight: 700;
  }

  #contactSection {
    box-shadow: 3px 6px 34px -1px rgba(0,0,0,0.54);
  }

  #contactSection img {
      max-height: 50px;
      max-width: 50px;
      margin: 15px;
  }

  #contactSection p {
    font-family: 'Open Sans';
    font-size: 18px;
    font-weight: 500;
    letter-spacing: -0.3px;
    text-align: center;
    color: #5d5d5d;
  }

  #whatSection {
    color: #58595b;
    margin: 0 10%;
  }

  .nav-tabs>li>a {
    color: #fff;
  }

  .nav-tabs>li {
    background: grey;
  }
  .nav-tabs>li.active {
    background: white;
  }

  #footerSection {
    background: #57585b;
    padding: 60px 0 30px;
    color: #fff;
  }

  #socialIcons img {
    height: 35px;
    margin: 20px;
  }

  #signup-btn {
    border: 1px solid;
    margin: 10px;
    height: 15px;
    line-height: 0px;
    font-weight: bolder;
    padding: 20px;
    position: relative;
    bottom: 10px;
    width: 200px;
    border-radius: 20px;
    font-size: 16px;
    text-align: center;
    text-transform: uppercase;
  }

  .navbar-inverse .navbar-nav>.open>a, .navbar-inverse .navbar-nav>.open>a:focus, .navbar-inverse .navbar-nav>.open>a:hover {
    color: #fff;
     background-color: transparent;
  }

  .dropdown.dropdown-toggle{
    width: 100%;
    .btn-default{
      background: rgba(255,255,255,0.3);
      color: #fff;
      font-size: 12px;
      padding: 7px;
      line-height: 17px;
      border-right: 0px;
      width: calc(100% - 34px);
      outline: none
    }
    #split-button-pull-right{
      width: auto;
      border-right: 2px solid #fff
      border-left: 0px;
    }
  }

  ul.dropdown-menu{
    margin-right: 11px;
    width: calc(100% - 10px);
    background: #fff;
    max-height: 200px;
    overflow: scroll;
    a{
      color: #000;
      &:hover{
        color: #000;
      }
    }
  }
  .xyz{
    border-bottom: 2px solid #808080 !important;
    border-radius: 0px;
    left: 50%;
    position: relative;
    transform: translateX(-50%);
    color: #808080 !important;
    margin-bottom: 15px;
    box-shadow: none;
    &:hover{
      border-bottom: 2px solid #808080 !important;
      border-top: 2px solid #fff !important;
    }
  }
  ul.dropdown-menu{
    margin: 0;
    width: 100px;
  }
  .nav > li>ul{
    overflow:hidden;
  }
  .btn-default{
    background: rgba(255,255,255,0.3);
    border-radius: 8px;
    color: #fff;
    font-weight: 300;
    transition: background 0.5s
    &:hover{
        background: rgba(255,255,255,1) !important;
        font-weight: 700 !important;
        color: #000 !important;
    }
  input::placeholder {
    color: #fff !important;
  }
  .carousel-control.left {
    background-image: none !important;
  }
  .carousel-control.right {
    background-image: none !important;
  }

  .carousel-inner>.item.active>.row:first-child{
    opacity: 0.4 !important;
  }

  #styled-scroll::-webkit-scrollbar-track {
  color: blue !important;
	-webkit-box-shadow: inset 0 0 6px rgba(0,0,0,0.3);
	background-color: #F5F5F5;
  }
  }
  .navMargin{
    margin-top:120px !important;
  }
  .progress-bar-info
  {
    background-color:'linear-gradient(135deg, rgb(66, 92, 122) 0%, rgb(53, 80, 112) 25%, rgb(32, 52, 79) 75%, rgb(18, 34, 55) 100%) !important'
  }
  .open> ul.dropdown-menu {
    overflow-x:hidden;
    width:-webkit-fill-available;
  }
  .rmScroll .open> ul.dropdown-menu{
    overflow:hidden;
  }
  .navToggle{
    background:transparent;
    border:0px;
  }
 .navbar-inverse .navbar-toggle:focus, .navbar-inverse .navbar-toggle:hover {
    background:transparent;
    border:0px;
  }
  .tiltedText{
    -ms-transform: rotate(17deg); /* IE 9 */
    -webkit-transform: rotate(-20deg);
    position: absolute;
    top: 185px;
    left: 45px;
    width: 15%;
    font-size: x-large;
    font-weight: 600;
    color: #13559f;
    word-wrap: break-word;
  }
  .tiltedText2{
    -ms-transform: rotate(17deg); /* IE 9 */
    -webkit-transform: rotate(-20deg);
    position: absolute;
    top: 135px;
    left: 230px;
    width: 15%;
    font-size: x-large;
    font-weight: 600;
    color: #13559f;
    word-wrap: break-word;
  }

  .arrow-up {
    margin-top: 15px;
    margin-left:-10px;
    width: 0;
    height: 0;
    border-left: 7px solid transparent;
    border-right: 6px solid transparent;
    border-bottom: 10px solid green;
  }

  .arrow-down {
    margin-top: 15px;
    margin-left:-10px;
    width: 0;
    height: 0;
    border-left: 7px solid transparent;
    border-right: 6px solid transparent;
    border-top: 10px solid red;
  }

  .arrow-right {
    width: 0; 
    height: 0; 
    border-top: 10px solid transparent;
    border-bottom: 10px solid transparent;
    border-left: 10px solid green;
  }

  .rowHead{
    display:none;
  }
  .mobHead{
    display:block;
  }
/* for new dashboard tab */
  .nav-stacked{
    background: #4f739a;
  }
  .nav-stacked>li{
    background: #4f739a;
  }
  .nav-stacked>li>a{
    height: 85px;
  }
  .nav-stacked>li>a>.img-responsive{
    height: 40px;
    margin-left:10%;
  }
  .nav-stacked>li>a:hover{
    background: #223d60;
  }
  .nav-tabs>li.active {
    background: #223d60;
  }
  .nav-tabs>li.active>a, .nav-tabs>li.active>a:focus, .nav-tabs>li.active>a:hover{
    background-color: #223d60; 
    border: 0px;
  }
  .subMenu{
    height:78.5px;
    width:100%;
    background-color:#223d60!important;
  }
  .subMenu >a{
    height:inherit;
    text-align: -webkit-center;
    padding-top: 20px!important;
  }
  .subMenu >a:hover{
    border:0px;
  }

  #houseIndex {
    margin-left: 10px;
  }

  .arrow-up {
    margin: 0 35px 0 0;
    position: relative !important;
    top: -14px;
    left: 5px;
    width: 0;
    height: 0;
  }

  .arrow-down {
    margin-left:unset!important;
    margin-top: 5px!important;
    position: absolute!important;
  }

/* ends here */
  @media screen and (min-width: 768px){
    .jumbotron {
        padding-top: 90px!important;
    }
  }

  @media only screen
    and (min-device-width : 320px)
    and (max-device-width : 1024px)
     {
   #houseIndex {
     margin-left: 0px;
   }
   .mainNav{
      height:auto !important;
    }
    .mainNav .navImg{
      height:-webkit-fill-available !important;
    }
    .arrow-up {
      margin: 0 35px 0 0;
      position: relative !important;
      top: -14px;
      left: 5px;
      width: 0;
      height: 0;
    }

    .arrow-down {
      margin-left:unset!important;
      margin-top: 5px!important;
      position: absolute!important;
    }
    .walletMain{
      margin:0px!important;
    }
    .walletMain .walletBtnDiv{
      margin:10px!important
    }
    .trendWrap{
      background:transparent!important;
      width:auto!important;
      padding-top:10px!important;
      margin:0px !important;
    }
    .trendWrap .trendWrapRow {
      height:40px!important;
    }
    .trendWrap .removeBorder{
      border-right:0px!important;
      padding-top:20px!important;
      margin-left:2px!important;
      width:99%!important;
      height:-webkit-fill-available!important;
      background:#FFF!important;
    }
    .trendWrap .removeBorder > h2{
      line-height:0px!important;
    }
    .trendWrap .carouselDiv{
      margin:0px!important;
      padding: 0 !important;
    }
    .trendWrap .carouselDiv .rowHead{
      height:100px!important;
      padding-top:15px!important;
      display:none!important;
    }

    .trendWrap .carouselDiv .mobHead{
      display:inline-flex!important;
      height: 40px!important
      margin: 0 2px !important;
      background:#FFF!important;
      box-sizing: border-box!important;
      overflow:hidden!important;
    }
    .trendWrap .carouselDiv .mobHead:hover .mobAnimate {
      -webkit-animation-play-state: paused
  }
  .trendWrap .carouselDiv .mobHead .mobAnimate {
    width: max-content!important;
    -webkit-animation: 10s linear 0s infinite normal backwards move_eye;;
  }
  @-webkit-keyframes move_eye {
      from {
          -webkit-transform: translate(100%,0)
      }
      to {
          -webkit-transform: translate(-100%,0)
      }
  }
  .trendWrap .carouselDiv .mobHead .mobAnimate .innerAnimate {
    box-sizing: border-box!important;
    display: inline-block!important;
    padding: 5px 10px!important;
    vertical-align: top!important;
    margin: 5px 2px!important;
    height: 100%!important;
  }
  .trendWrap .carouselDiv .mobHead .mobAnimate .innerAnimate .data {
      display:inline-block!important;
  }
  .trendWrap .carouselDiv .mobHead .mobAnimate .innerAnimate .data1 {
    margin-left:10px!important;
    display:inline-block!important;
}
  .trendWrap .carouselDiv .mobHead .mobAnimate .innerAnimate:hover .extra_info {
      display: inline!important;
  }
  .trendWrap .carouselDiv .mobHead .mobAnimate .innerAnimate .extra_info {
      display: none!important;
  }


    .trendWrap .carouselDiv .mobHead .carousel{
      margin-left:inherit!important;
    }
    .trendWrap .carouselDiv .carouselData{
      padding:0px!important;
    }
    .propListContainer{
      margin:0px!important;
      margin-top:30px!important;
      margin-left: 15px!important;
      margin-right: 15px!important;
    }
    .wrapPropList{
      margin:5px!important;
    }
    .propListContainer .propListHead{
      margin:0px!important;
      padding-top:0px!important;
    }
    .propListContainer .propfilterBtn{
      margin:0px!important;
      margin-top: 125px!important;
      margin-left: -25px!important;
      padding-bottom:25px!important;
      display:none!important;
    }
    .propListContainer .propfilterBtn .styleBtn{
      width:auto!important;
    }
    .propTileContainer {
      margin:0px!important;
      margin-top: -5px !important;
    }
    .propTileContainer .propTileCol{
      padding:0px!important;
    }
    .propTileContainer .propTileCol .tileDiv{
      max-width:none!important;
      margin: 15px auto!important;
      padding-left: 10px;
      padding-right: 10px;
    }
    .propDetailHead{
      height: auto!important;
      margin: 0px!important;
      margin-top: 50px!important;
    }
    .propDetailHead .headContainer{
      margin-top:10px;
    }
    .propDetailHead .headContainer .priceRow{
      padding-bottom:20px;
    }
    .propDetailHead .headContainer .priceRow  .propPrice{
      margin: 15px 22vw 0px!important;
    }
    .carouselHideMob{
      display:none;
    }
    .carouselShowMob{
      margin:auto!important;
    }
    .propDetailInfoContainer{
      margin: 2vw 0vw!important;
      padding: 20px 0px!important;
    }
    .propDetailInfoContainer .investmentInfo .sectionDetail{
      margin:0px!important;
      margin-top: 10px!important;
      margin-left: -10px!important;
    }
    .propDetailInfoContainer.investmentInfo .profitEstimate{
      padding:0px!important;
    }
    .propDetailInfoContainer .investmentInfo .profitEstimate .dataProfit{
      margin:0px!important;
      padding:5px!important;
      width:43%!important;
    }
    .propDetailInfoContainer .investmentInfo .profitEstimate .seprator{
      padding:43px 0px !important;
      width:8%!important;
    }
    .propDetailInfoContainer .propertyInfo{
      text-align:left!important;
    }
    .propDetailInfoContainer .propertyInfo .priceDiv{
      margin:0px!important;
      width:90%!important;
      margin-left:5%!important;
    }
    .propDetailInfoContainer .propertyInfo .detailBuyBtn{
      font-weight:500!important;
      height:60px!important;
      font-size:22px!important;
    }
    .detailAboutWrapper .detailAboutContainer{
      margin:5vw 0px !important;
    }
    .detailAboutWrapper .detailAboutContainer .propDetail > p{
      height:300px!important;
      text-align:justify!important;
    }
    .detailAboutWrapper .detailAboutContainer .reportDiv{
      margin-right:5px!important;
    }
    .detailAboutWrapper .detailAboutContainer .chartDiv .chartData{
      margin-top:0px!important;
      margin-right:-5px!important;
      margin-left:-5px!important;
      width: -webkit-fill-available;
      height:auto!important;
    }
    .detailAboutWrapper .detailAboutContainer .chartDiv .chartData .chartjs-render-monitor{
      height: 250px!important;
      width: 300px!important;
      margin-left: -10px!important;
    }
    .detailAboutWrapper .detailAboutContainer .estimateDiv{
      margin-top:inherit!important;
    }
    .detailAboutWrapper .detailAboutContainer .estimateDiv .estimateContainer{
      margin-left: -5px!important;
      width: -webkit-fill-available!important;
      margin-right: -5px!important;
      margin-top:0px!important;
    }
    .detailAboutWrapper .detailAboutContainer .estimateDiv .estimateContainer .data{
      margin:0px!important;
      margin-top:20px!important;
    }
    .detailAboutWrapper .detailAboutContainer .mapHeading{
      margin-left:10px!important;
    }
    .buyHeaderContainer{
      margin-top:50px!important;
      height:123px!important;
    }
    .buyHeaderContainer .icon{
      height:34px!important;
      width:38px!important;
    }
    .buyHeaderContainer .headerTitle{
      font-size:24px!important;
    }
    .buyHeaderContainer .subHead{
      font-size:18px!important;
    }
    .buyHeaderContainer .insideData{
      width:auto!important;
      height:20px!important;
      font-size:12px!important;
    }
    .buyDetailContainer{
      margin: 0px 0px !important;
      margin-left:0px!important;
      margin-right:0px!important;
    }
    .buyDetailContainer .buyTokenForm{
      margin-left:-40px!important;
      margin-right:-40px!important;
    }
    .buyTokenForm .formData{
      height:auto!important;
      font-size:15px!important;
      padding-left:0px!important;
      padding-right:0px!important;
    }
    .buyTokenForm .formDataDiv{
      margin-top:35px !important;
      height:auto!important;
      word-wrap: break-word!important;
    }
    .buyTokenForm .formDataInput{
      margin-left:0px!important;
      width:65px!important;
      padding:0px 0px !important;
    }
    .walletHeadContainer .walletIcon{
      margin-left:15px!important;
    }
    .walletHeadContainer .walletBalance{
      height:auto!important;
      width:99%!important;
    }
    .walletBalanceContainer{
      padding: 0px !important;
      margin-top: auto!important;
    }
    .walletBalanceContainer .walletDetail{
      max-width:none!important;
      border-bottom:2px solid rgb(128, 128, 128)!important;
    }
    .walletBalanceContainer .walletDetail .withdrawbtn{
      margin-top:0px!important;
    }
/* new dashboard CSS starts*/
    
/* new dashboard css end */
  }
`;
