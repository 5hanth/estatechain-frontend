/**
 * 
 * Buy Token Header
 * 
 */
import React from 'react';
import { Row, Col } from 'react-bootstrap';
import {Wrapper,styles} from './style.js';
import Icon from './icon.png';
import api from '../../utils/api';

class BuyTokenHeader extends React.Component{
      constructor(props){
          super(props);
          console.log(props)
          this.state = {id:'', property: props.property};
      }
      render() {
        return (
            <Col style={styles.headerContainer} className="buyHeaderContainer">
                <Col smOffset={1} sm={10} xs={12} style={styles.banner}>
                    <div style={styles.headerTitleContainer}>
                        <img src={Icon} href="#" style={styles.icon}  className="icon"/>
                        <h1  style={styles.headerTitle} className="headerTitle">Buy Tokens</h1>
                    </div>
                    <p style={styles.subHead} className="subHead">{this.state.property.address}</p>
                    <p style={styles.insideData} className="insideData">Token Price  {this.state.property.token_price} | {((this.state.property.token_price_raw || 4.31) / 8000).toFixed(5)} BTC | {((this.state.property.token_price_raw || 4.31) / 320).toFixed(5)} ETH</p>
                </Col>
            </Col>
        )
    }
}

export default BuyTokenHeader;