import styled from 'styled-components';
import Bg from './wrap-bg.png';
export const styles = {
  headerContainer:{
    height: '130px',
    background: '#e9e7e5',
    marginTop: '120px',
  },
  banner:{
    background : 'Url('+Bg+')',
  },
    icon :{
      objectfit: 'contain',
      height:'43px',
      width:'44px',
    },
    headerTitleContainer : {
      display: "flex",
      flexDirection: "Row",
      justifyContent: "center",
      paddingTop: '10px',
      height:'48px',
    },
    headerTitle:{
        height: '30px',
        fontFamily: 'Open Sans',
        fontSize: '30px',
        textAlign: 'center',
        color: '#ffffff',
        marginLeft:'10px',
        lineHeight:'0px',
    },
    subHead:{
        height:'24px',
      fontfamily: 'Open Sans',
      fontSize: '24px',
      textAlign: 'center',
      color:'#fff',
    },
    insideData:{
        width:'500px',
        height: '30px',
        fontFamily: 'Open Sans',
        fontSize: '18px',
        textAlign: 'center',
        color: '#ffffff',
        textShadow: '0px 3px 7px rgba(21, 22, 22, 0.35)',
        borderRadius: '15.5px',
        border: 'solid 1px #ffffff',
        margin:'15px auto',
    },
    ChartBg:{
      marginTop:'10px',
      backgroundColor:'#fff',
    },
  }
export const Wrapper = styled.div`
width: auto;
height:132px;
overflow: hidden;
margin :14px 46px 14px 46px;
background: url(${Bg}) no-repeat center center;
background-size: cover;
`;
