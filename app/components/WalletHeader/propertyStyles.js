import styled from 'styled-components';
import Bg from './wrap-bg.png';

export const propertyStyles = {
  headerTitle : {
    fontFamily: 'Open Sans',
    fontSize: '30px',
    color: '#fff',
    margin: '6px 23px',
    fontWeight: '100'
  },
  headerTitleContainer : {
    display: "flex",
    flexDirection: "Row",
    justifyContent: "center",
    padding: '19px 0'
  },
  subHead:{
    fontSize: '23px',
    textAlign: 'center',
    margin:'0',
    color:'#fff',
    fontWeight: '100'
  },

  balanceCont : {
    border: '2px solid #fff',
    height: '45px',
    width: '70%',
    margin: '14px auto',
    background: 'rgba(255,255,255,0.3)'
  },

  balanceElement :{
    color: '#fff',
    fontSize: '21px',
    fontWeight: '100',
    textAlign: 'center',
    margin: '8px 0',
    lineHeight: '25px',
    borderRight: '2px solid #fff'
  },
  balanceElementLast : {
      color: '#fff',
      fontSize: '21px',
      fontWeight: '100',
      textAlign: 'center',
      margin: '8px 0',
      lineHeight: '25px',
  }
}

export const Wrapper = styled.div`
  overflow: 'hidden';
  background: url(${Bg}) no-repeat center center;
  height: 190px;
  margin: 120px;
  background-size: cover;
  @media (max-width: 767px) {
    margin:0px!important;
    height:260px!important;
    margin-Top:50px!important;
  }
`;
