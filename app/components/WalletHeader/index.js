/**
*
* WalletHeader
*
*/

import React from 'react';
import {Wrapper, propertyStyles} from './propertyStyles';
import {Row,Col,Image, Clearfix} from 'react-bootstrap';
import WalletIcon from './WalletIcon.png';
// import styled from 'styled-components';


export default class WalletHeader extends React.Component {
  constructor(props){
      super(props);
      this.state = {balance: props.balance, wallet: {}};
  }

  render() {

  var balance = this.state.balance.map(function(input,index){
                return <Col sm={3} xs={6} key={index} style={index != 3 ? propertyStyles.balanceElement : propertyStyles.balanceElementLast }>{input.value}  {input.currency}</Col>
                
                      })

  return (
    <Col>
    <Wrapper>
      <Col className="walletHeadContainer">
          <div style={propertyStyles.headerTitleContainer}>
            <Image href="#" src={WalletIcon} className="walletIcon"/><h1  style={propertyStyles.headerTitle}>My Wallet</h1>
          </div>
          <h2 style={propertyStyles.subHead}>Available Funds</h2>
          <Row style={propertyStyles.balanceCont} className="walletBalance">
            {balance}
          </Row>
      </Col>
    </Wrapper>
    </Col>
  );
}
}
