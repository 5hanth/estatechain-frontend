import styled from 'styled-components';

const propertyStyles = {
  propertyRowContainer: {
    margin: '30px 120px'
  },
  propertyRow : {
    padding: '30px 50px',
  },
  propertyTile : {
    boxShadow: '0px 10px 51px -3px rgba(0,0,0,0.75)',
    color: 'rgb(110,110,110)',
    background: '#fff',
    maxWidth: '305px',
    margin: '0 auto'
  },
  addressBar : {
    height: '50px',
    // background:' -moz-linear-gradient(135deg, rgba(72,177,199,1) 0%, rgba(56,139,194,1) 25%, rgba(82,105,189,1) 75%, rgba(108,76,183,1) 100%)',
    // background:' -webkit-gradient(linear, left bottom, right top, color-stop(0%, rgba(72,177,199,1)), color-stop(25%, rgba(56,139,194,1)), color-stop(75%, rgba(82,105,189,1)), color-stop(100%, rgba(108,76,183,1)))',
    // background:' -webkit-linear-gradient(135deg, rgba(72,177,199,1) 0%, rgba(56,139,194,1) 25%, rgba(82,105,189,1) 75%, rgba(108,76,183,1) 100%)',
    // background:' -o-linear-gradient(135deg, rgba(72,177,199,1) 0%, rgba(56,139,194,1) 25%, rgba(82,105,189,1) 75%, rgba(108,76,183,1) 100%)',
    // background:' -ms-linear-gradient(135deg, rgba(72,177,199,1) 0%, rgba(56,139,194,1) 25%, rgba(82,105,189,1) 75%, rgba(108,76,183,1) 100%)',
    background:'linear-gradient(135deg, rgb(62, 90, 122) 0%, rgb(53, 84, 117) 25%, rgb(38, 53, 75) 75%, rgb(19, 37, 57) 100%)',
    color: '#fff',
    lineHeight: '50px',
    fontSize: '28px',
    paddingLeft: '10px',
    fontWeight: '100',
    fontFamily: 'Open Sans'
  },
  Image: {
    width: '100%'
  },
  tokensSoldContainer: {
    height: '42px',
    borderRadius: '5px',
    background: 'rgba(0, 0, 0, 0.4)',
    top: '-47px',
    margin: '0 5px',
    position: 'relative',
    fontSize: '12px',
    textAlign: 'right',
    padding: '6px 7px',
    color: '#fff'
  },
  tokenSoldBarCont : {
    border: '1px solid #fff',
    height: '10px',
    borderRadius: '5px',
    margin: '3px 0px'
  },
  height0: {
    height: '0px'
  },
  investmentText: {
    float: 'right'
  },
  investmentTextContainer: {
    margin: '10px',
    lineHeight: '10px',
    height: '10px'
  },
  percentileFields: {
    margin: '0 15px',
    borderTop: '1px solid rgb(170,170,170)',
    borderBottom: '1px solid rgb(170,170,170)',
    padding: '7px 0'
  },
  percentileFieldsCont: {
    width: 'calc(50% - 10px)',
    fontSize: '10px',
    fontWeight: '600',
    textAlign: 'center',
    border: '1px solid rgb(18, 34, 55)',
    borderRadius: '5px',
    padding:'7px',
    margin: '0 5px'
  },
  percentileField:{
    display: 'block',
    fontSize: '18px',
    fontWeight: '600',
    marginTop: '3px'
  },
  checkContainer:{
    padding: '10px 0',
    // background:' -moz-linear-gradient(45deg, rgba(72,177,199,1) 0%, rgba(56,139,194,1) 25%, rgba(82,105,189,1) 75%, rgba(108,76,183,1) 100%)',
    // background:' -webkit-gradient(linear, left bottom, right top, color-stop(0%, rgba(72,177,199,1)), color-stop(25%, rgba(56,139,194,1)), color-stop(75%, rgba(82,105,189,1)), color-stop(100%, rgba(108,76,183,1)))',
    // background:' -webkit-linear-gradient(45deg, rgba(72,177,199,1) 0%, rgba(56,139,194,1) 25%, rgba(82,105,189,1) 75%, rgba(108,76,183,1) 100%)',
    // background:' -o-linear-gradient(45deg, rgba(72,177,199,1) 0%, rgba(56,139,194,1) 25%, rgba(82,105,189,1) 75%, rgba(108,76,183,1) 100%)',
    // background:' -ms-linear-gradient(45deg, rgba(72,177,199,1) 0%, rgba(56,139,194,1) 25%, rgba(82,105,189,1) 75%, rgba(108,76,183,1) 100%)',
    background:'linear-gradient(135deg, rgb(62, 90, 122) 0%, rgb(53, 84, 117) 25%, rgb(38, 53, 75) 75%, rgb(19, 37, 57) 100%)',
    margin: '0 5px 5px 5px',
    color:'#fff',
    textAlign: 'center'
  },
  tokenValue:{
    display: 'block',
    fontSize: '18px',
    fontWeight: '500',
  },
  checkBtnContainer: {
    position: 'relative',
    top: '7px'
  },
  checkBtn: {
    background: 'rbga(255,255,255,0.2)'
  },
  loading:{
    position:'fixed',
    top:'0px',
    //margin:'0px 0px',
    width:'100%',
    height:'100%',
    background:'linear-gradient(135deg, rgb(66, 92, 122) 0%, rgb(53, 80, 112) 25%, rgb(32,52, 79) 75%,  rgb(18, 34, 55) 100%)',
    //background-image:url('ajax-loader.gif');
    backgroundRepeat:'no-repeat',
    backgroundPosition:'center',
    zIndex:'999',
    opacity: '0.6',
    filter: 'alpha(opacity=40)', /* For IE8 and earlier */
  },
  loadImg:{
    margin:'25% 45%',
  },
}


export default propertyStyles;
