/**
*
* PropertyTile
*
*/

import React from 'react';
// import styled from 'styled-components';
import propertyStyles from './propertyStyles';
import { Grid, Row, Col, Button, Image } from 'react-bootstrap';
import api from '../../utils/api';
//import bg from './rolling.svg';

class PropertyTile extends React.Component {
  constructor(props) {
    super(props);
    this.state = { properties: []}
  }
  componentDidMount() {
    api.get("properties", {}).then(resp => {
      if(resp.data.error===false){
      this.setState({
                properties: resp.data.properties
              })
      }
    })
  }
  render() {
    // if (this.state.properties.length<1) {
    //   return (
    //     <Row style={propertyStyles.loading}>
    //       <img src={bg} style={propertyStyles.loadImg}/>
    //     </Row>
    //   );
    // }
    var properties = this.state.properties.map(function(input,index){
      var barWidth = {
        background : '#fff',
        height: '100%',
        width: (input.tokens_sold_percent || 48) + '%'
      }
    //var img = input.imgs1.map((i) => <img src={i} style={propertyStyles.Image}/>);
      var img = <img src={input.imgs1[0]} style={propertyStyles.Image}/>
      return <Col sm={4} lg={4} md={4} xs={12} key={index} style={propertyStyles.propertyRow} className="propTileCol">
                <Col style={propertyStyles.propertyTile} className="tileDiv">
                  <div style={propertyStyles.addressBar}>{input.address}</div>
                  {img}
                  <div style={propertyStyles.height0}>
                    <div style={propertyStyles.tokensSoldContainer}>
                      {input.tokens_sold_percent || 48}% Tokens sold
                      <div style={propertyStyles.tokenSoldBarCont}>
                        <div style={barWidth}></div>
                      </div>
                    </div>
                  </div>
                  <div style={propertyStyles.investmentTextContainer}>
                    Total Investment<span style={propertyStyles.investmentText}><b>{input.total_investment}</b></span>
                  </div>
                  <Row style={propertyStyles.percentileFields}>
                    <Col md={6} xs={12} style={propertyStyles.percentileFieldsCont}>
                      <div>
                      Net Rental Yield
                      <span style={propertyStyles.percentileField}>{input.net_rental_yield}</span>
                      </div>
                    </Col>
                    <Col md={6} xs={12} style={propertyStyles.percentileFieldsCont}>
                      <div>
                      Estimated ROI
                      <span style={propertyStyles.percentileField}>{input.estimated_roi}</span>
                      </div>
                    </Col>
                  </Row>
                  <div style={propertyStyles.investmentTextContainer}>
                    Total Investors<span style={propertyStyles.investmentText}><b>{input.total_investors}</b></span>
                  </div>
                  <Row style={propertyStyles.checkContainer} id="checkWrap">
                    <Col md={5}>
                    Token Value
                    <span style={propertyStyles.tokenValue}> {input.token_price}</span>
                    </Col>
                    <Col md={7} style={propertyStyles.checkBtnContainer}>
                      <a href={'properties/'+ input.id}><Button>Check Property</Button></a>
                    </Col>
                  </Row>
                  <Row></Row>
                </Col>
              </Col>
            })
            return (
            <Row style={propertyStyles.propertyRowContainer} className="propTileContainer">
            {properties}
            </Row>
            );
  }
}


PropertyTile.propTypes = {

};

export default PropertyTile;
