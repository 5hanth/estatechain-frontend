import styled from 'styled-components';

export const propertyStyles = {
  headerTitle : {
    fontFamily: 'Open Sans',
    fontSize: '30px',
    color: '#fff',
    margin: '6px 23px',
    fontWeight: '100'
  },
  headerTitleContainer : {
    display: "flex",
    flexDirection: "Row",
    justifyContent: "center",
    padding: '19px 0'
  },
  subHead:{
    fontSize: '14px',
    textAlign: 'center',
    margin:'8px 0 0',
    fontWeight: '500'
  },
  balanceTxt :{
    fontSize: '21px',
    textAlign: 'center'
  },
  balanceCont : {
    padding: '3px 57px',
    marginTop:'-80px',
  },
  currBtn:{
    fontSize: '17px',
    fontWeight: '500',
    borderBottom: '2px solid rgb(128, 128, 128)',
    color: 'rgb(128, 128, 128)',
    padding: '5px 35px',
    position: 'relative',
    marginRight:'-43px',
    left: '-40%',
  },
    balanceTile :{
      color: '#464646',
      padding: '0 8px',
      background: "lightgrey"
    },
  balanceElement : {
    width: '100%',
    boxShadow: '0px 3px 5px -1px rgba(138,138,138,1)',
    padding: '18px 0 48px',
    background: '#fff',
    maxWidth: '290px',
    margin: '0 auto'
  },
  icon: {
    height: '81px',
    width: 'auto',
    left: '50%',
    position: 'relative',
    transform: 'translateX(-50%)'
  },
  btnStyle: {
    fontSize: '17px',
    fontWeight: '500',
    border: '2px solid #808080',
    color: '#808080',
    padding: '2px 21px',
    position: 'relative',
    left: '50%',
    width: '100%',
    transform: 'translateX(-50%)'
  },
  singlebtnStyle: {
    fontSize: '17px',
    fontWeight: '500',
    border: '2px solid #808080',
    color: '#808080',
    padding: '2px 21px',
    position: 'relative',
    left: '50%',
    marginTop:'20px',
    transform: 'translateX(-50%)'
  },
  btnContainer: {
    padding: '15px 0',
    margin:'15px 10px',
    borderTop: '2px solid #808080',
    borderBottom: '2px solid #808080'
  },
  btnCont: {
    padding: '0 3px',
    margin:'0'
  },
  borderBtm: {
    borderBottom: '2px solid #808080',
    paddingBottom: '15px',
    margin:'0 10px'
  },
  convertText: {
    margin: '10px',
    textAlign: 'center'
  }
}
