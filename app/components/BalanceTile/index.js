/**
*
* BalanceTile
*
*/

import React from 'react';
import {Row,Col,Image,Button,DropdownButton,MenuItem, Clearfix} from 'react-bootstrap';
import {propertyStyles} from './propStyles';
import Loading  from 'components/Loading';
import ecIco from './estatecoin.jpg';
import ethIco from './etho.jpg';
import dollarIco from './doller.jpg';
import btcIco from './bitcoin.jpg';
// import styled from 'styled-components';

function restrict() {
  alert("This is only for demo purpose.")
}

export default class BalanceTile extends React.Component {
  constructor(props){
      super(props);
      this.state = {balance: props.balance, wallet: {}};
  }

  Balance(){
    return this.state.balance.map(function(input,index){

                  if (input.name === 'BTC'){
                     var Icon = btcIco;
                     var conversions = ['Dollars','ETH','Estatechain Token'];
                   }
                  else if (input.name === 'Dollars'){
                     var Icon = dollarIco;
                     var conversions = ['BTC','ETH','Estatechain Token'];
                   }
                  else if (input.name === 'ETH'){
                     var Icon = ethIco;
                     var conversions = ['BTC','Dollars','Estatechain Token'];
                   }
                  else if (input.name === 'Estatechain Token'){
                     var Icon = ecIco;
                     var conversions = ['BTC','Dollars','ETH'];
                   }


                 var typeList = conversions.map(function(input,index){
                                 return <MenuItem key={index}>{input}</MenuItem>
                                       })


                  return <Col sm={3} xs={12} key={index}  style={propertyStyles.balanceTile}>
                            <div style={propertyStyles.balanceElement} className="walletDetail">
                              <Image href="#" src={Icon} style={propertyStyles.icon}/>
                              <Col style={propertyStyles.subHead}>Available {input.name}</Col>
                              <Col style={propertyStyles.balanceTxt}>{input.value} {input.currency}</Col>
                              <Row style={propertyStyles.btnContainer}>
                                <Col sm={6} xs={6} style={propertyStyles.btnCont}>
                                 <Button style={propertyStyles.btnStyle} onClick={restrict}>Send</Button>
                                </Col>
                                <Col sm={6} xs={6} style={propertyStyles.btnCont}>
                                 <Button style={propertyStyles.btnStyle} onClick={restrict}>Recieve</Button>
                                </Col>
                              </Row>
                              <Col style={propertyStyles.borderBtm}>
                                <Button className="withdrawbtn" style={propertyStyles.singlebtnStyle}  onClick={restrict}>Withdraw</Button>
                              </Col>
                              <div style={propertyStyles.convertText}>Convert To</div>

                                <Col md={4} mdOffset={4} xs={10} xsOffset={2} className="center-align">
                                <DropdownButton bsSize="default" style={propertyStyles.currBtn} title="Currency" pullRight id="split-button-pull-right">
                                  {typeList}
                                </DropdownButton>
                                </Col>
                              <Button style={propertyStyles.singlebtnStyle}  onClick={restrict}>Convert</Button>
                            </div>
                            <Clearfix />
                          </Col>

                        })
  }

  render() {
    if(!this.state.balance.length) {
      return <Loading />
    }
    return (
      <Col style={propertyStyles.balanceCont} className="walletBalanceContainer">
        {this.Balance()}
      </Col>
    );
  }

}

BalanceTile.propTypes = {

};
