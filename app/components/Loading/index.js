/**
*
* Loading
*
*/

import React from 'react';
import { Row, Col } from 'react-bootstrap';
import {styles} from './style.js';
import Bg from './rolling.svg';


function Loading() {
  return (
    <Row style={styles.loading}>
        <img src={Bg} style={styles.loadImg}/>
    </Row>
  );
}

Loading.propTypes = {

};

export default Loading;
