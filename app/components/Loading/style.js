import styled from 'styled-components';
export const styles = {
    container :{
      paddingLeft:'31px',
      paddingRight:'31px',
      //marginTop:'55px',
      backgroundColor:'#FFF',
      marginBottom:'20px',
    },
    headerTitleContainer : {
      display: "flex",
      flexDirection: "Row",
      justifyContent: "center",
      paddingTop: '10px',
      height:'48px',
    },
    insideData:{
      margin :'0px 16px',
    },
    mainContainer:{
      background:'#FFF',
    },
    loading:{
      position:'fixed',
      top:'0px',
      width:'100%',
      height:'100%',
      background:'linear-gradient(135deg, rgb(66, 92, 122) 0%, rgb(53, 80, 112) 25%, rgb(32,52, 79) 75%,  rgb(18, 34, 55) 100%)',
      backgroundRepeat:'no-repeat',
      backgroundPosition:'center',
      zIndex:'999',
      opacity: '0.6',
      filter: 'alpha(opacity=40)', /* For IE8 and earlier */
    },
    loadImg:{
      margin:'25% 45%',
    },
  }
