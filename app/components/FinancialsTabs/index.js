/**
*
* FinancialsTabs
*
*/

import React from 'react';
import {Col,Row, Tab, Nav, NavItem} from 'react-bootstrap';
import styles from './financialStyles'
import tabsPng from './tabs.png'


function FinancialsTabs() {
  return (
    <Tab.Container id="financials-tabs" defaultActiveKey="first">
    <Row className="clearfix">
      <Col sm={12}>
        <Nav bsStyle="tabs">
          <NavItem eventKey="first">
            Token Valuation
          </NavItem>
          <NavItem eventKey="second">
            House Price Index
          </NavItem>
          <NavItem eventKey="third">
            Rental Income
          </NavItem>
        </Nav>
      </Col>
      <Col sm={12}>
        <Tab.Content animation style={styles.whiteBg}>
          <Tab.Pane eventKey="first">
            <img src={tabsPng} style={styles.tabsStyle} />
          </Tab.Pane>
          <Tab.Pane eventKey="second">
            <img src={tabsPng} style={styles.tabsStyle} />
          </Tab.Pane>
          <Tab.Pane eventKey="third">
            <img src={tabsPng} style={styles.tabsStyle} />
          </Tab.Pane>
        </Tab.Content>
      </Col>
    </Row>
  </Tab.Container>
  );
}

FinancialsTabs.propTypes = {

};

export default FinancialsTabs;
