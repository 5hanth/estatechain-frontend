const financialStyles = {
  container: {
    margin: '2vw 10vw'
  },
  title: {
    fontFamily: 'Open Sans',
    fontSize: '20.7px',
    fontWeight: '600',
    textAlign: 'left',
    color: '#464646'
  },
  whiteBg: {
    background: '#fff'
  },
  tabsStyle: {
    margin: '35px 0'
  }
}

export default financialStyles;
