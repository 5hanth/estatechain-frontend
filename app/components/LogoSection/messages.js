/*
 * LogoSection Messages
 *
 * This contains all the text for the LogoSection component.
 */
import { defineMessages } from 'react-intl';

export default defineMessages({
  header: {
    id: 'app.components.LogoSection.header',
    defaultMessage: 'This is the LogoSection component !',
  },
});
