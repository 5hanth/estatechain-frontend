const logoTitleStyles = {
  color: '#fff',
  textTransform: 'uppercase',
  textShadow:  '5px 5px 10px rgba(17, 17, 17,0.5)',
  margin: '50px 0px 40px 0px'
}

export const knowMore = {
  color: '#fff',
  lineHeight: '35px',
  cursor: 'pointer',
  textDecoration: 'none'
}

export const chain = {
  fontFamily: 'Raleway',
  fontSize: '120px',
  letterSpacing: '26.5px',
  textAlign: 'left',
  color: '#e5e5e5',
  lineHeight: '85px'
}

export const estate = {
  fontFamily: 'Raleway',
  fontSize: '96px',
  color: '#e5e5e5',
  fontWeight: 600,
  letterSpacing: '7px',
  color: '#ffffff'
}

export const logoActions = {
  marginTop: '30px'
}

export const logoStart = {
  marginTop: '121px'
}

export const vdivider = {
  padding: '50px 0px',
  width: '10px',
  margin: '10px 15px 10px 15px',
  borderLeft: '2px solid #fff',
  position: 'relative'
}

export const logoSectionItem = {
  padding: '10px 25px',
  color: '#fff'
}

export const logoSectionItem1 = {
  padding: '10px 0px 20px 20px',
  color: '#fff',
  width: '45%'
}

export const logoSectionItems = {
  margin: '290px 0px',
  border: '1px solid #fff',
  borderRadius: '10px',
  borderRadius: '10px',
  backgroundColor: 'rgba(255, 255, 255, 0.3)',
  height: '121.5px'
}

export const mBody = {
  paddingTop: '5px',
  fontSize: '18px',
  fontFamily: 'Open Sans'
}

export const watchVideo = {
  color: '#4568ba',
  fontWeight: 'bolder',
  padding: '10px',
  width: '200px',
  borderRadius: '20px',
  fontSize: '16px',
  background:'#FFFFFF'
}
export const watch = {
  marginTop: 'inherit',
}

export default logoTitleStyles;
