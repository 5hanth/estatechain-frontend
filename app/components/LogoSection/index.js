/**
*
* LogoSection
*
*/

import React from 'react';
// import styled from 'styled-components';

import { FormattedMessage } from 'react-intl';
import messages from './messages';
import Wrapper from './wrapper';
import Logo from './logo.png';
import HomeSvg from './home.svg';
import ShieldSvg from './shield.svg';
import logoStyles from './logoStyles';
import logoSectionStyles from './logoSectionStyles';
import logoTitleStyles, { watch,logoStart, estate, knowMore, watchVideo, chain, logoActions, vdivider, logoSectionItem, logoSectionItem1, logoSectionItems, mBody } from './logoTitleStyles';

import { Row, Col, Button, Media } from 'react-bootstrap';


function LogoSection() {
  return (
    <div style={logoSectionStyles} id="logoSectionWrap">
    <Row style={logoStart}>
      {/* <Col md={3} mdOffset={1}><img src={Logo} style={logoStyles}/></Col> */}
      <Col sm={5} smOffset={6} style={watch}>
        {/* <Col style={logoTitleStyles}><Row style={estate}>Estate</Row>
        <Row style={chain}>Chain</Row></Col> */}
        <Row style={logoActions}>
          {/* <Col sm={3} md={3} mdOffset={2}><a style={knowMore}>Know more</a></Col>
          <Col sm={8} md={7}><Button style={watchVideo}>Watch the Video</Button></Col> */}
        </Row>
      </Col>
    </Row>
    </div>
  );
}

// <Row style={logoSectionItems}>
//   <Col sm={6} style={logoSectionItem}>
//   <Media>
//     <Media.Left><img src={ShieldSvg}  width={102} height={99}/>
//     </Media.Left>
//     <Media.Body style={mBody}>A platform powered by Block Chain  and AI, which enables everyone to invest via crowdfunding in real estate around the world</Media.Body>
//   </Media></Col>
//
//   <Col sm={1} style={vdivider}></Col>
//   <Col sm={5} style={logoSectionItem1}>
//     <Media>
//       <Media.Left>
//       <img src={HomeSvg} width={102} height={99}/>
//       </Media.Left>
//       <Media.Body style={mBody}>A Social Media platform to follow leading investors in the real estate market and keep updated about the latest market trends</Media.Body>
//     </Media></Col>
// </Row>

LogoSection.propTypes = {

};

export default LogoSection;
