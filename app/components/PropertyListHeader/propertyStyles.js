import styled from 'styled-components';
import Bg from './wrap-bg.png';

const propertyStyles = {
}

export const Wrapper = styled.div`
  overflow: 'hidden';
  background: url(${Bg}) no-repeat center center;
  height: 210px;
  margin: 5px;
  background-size: cover;
`;

export const headerTitle = {
  fontFamily: 'Open Sans',
  textAlign: 'center',
  fontSize: '30px',
  margin: '11px',
  color: '#fff',
  paddingTop: '11px'
}

export const propText = {
  textAlign: 'center',
  color: '#fff',
  fontSize: '18px'
}

export const propButton = {
  background: 'transparent',
  color: 'white',
  borderRadius: '20px',
  width: '200px',
  margin: '10px 0px',
  border: '2px solid'
}

export const propRow = {
  margin: '150px 0px'
}

export const widthfix = {
  width : '100% - 60px',
  margin: '30px'
  }

export const dropDownContainer = {
  margin: '30px 0 0 85px'
}

export const styleButton ={
  width:'250px'
}
export const styleButton2 ={
  width:'250px',
  overflow:'hidden',
}


export default propertyStyles;
