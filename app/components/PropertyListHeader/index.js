/**
*
* PropertyListHeader
*
*/

import React from 'react';
// import styled from 'styled-components';
import { Grid, Row,DropdownButton, Col, Button, Thumbnail,clearFix, SplitButton, MenuItem } from 'react-bootstrap';
import Trends  from 'components/Trends';
import PropertyTile  from 'components/PropertyTile';
import {Wrapper,styleButton, headerTitle, propText, propButton, propRow, widthfix, dropDownContainer} from './propertyStyles';



function PropertyListHeader() {
  var countries = require('./countries.json');
  var sort =  ['Type','Name','Tokens'];
  var type = ['Token Price','HPI', 'ROI'];

  var countryList = countries.map(function(input,index){
                return <MenuItem key={index}>{input.name}</MenuItem>
                      })

  var typeList = sort.map(function(input,index){
                return <MenuItem key={index}>{input}</MenuItem>
                      })
  var sortList = type.map(function(input,index){
                return <MenuItem key={index}>{input}</MenuItem>
                      })

  return (
    <Col>
      <Wrapper>
          <Col style={widthfix} className="propListContainer">
            <Row style={headerTitle} className="propListHead">Properties</Row>
            <Trends/>
            <clearFix />
            <Row style={dropDownContainer} className="propfilterBtn">
            <Col sm={4} xs={8} className="">
            <DropdownButton bsStyle="default" className="styleBtn row" style={styleButton} title="Select Country/Region" pullRight id="split-button-pull-right">
              {countryList}
            </DropdownButton>
              {/* <SplitButton style={styleButton} title="Select Country/Region" pullRight id="split-button-pull-right">
                  {countryList}
              </SplitButton> */}
            </Col>
            <Col sm={4} xs={4} className="rmScroll">
            <DropdownButton bsStyle="default" className="styleBtn" style={styleButton} title="Sort By" pullRight id="split-button-pull-right">
              {sortList}
            </DropdownButton>
            </Col>
            <Col sm={4} xs={6} className="rmScroll">
              <DropdownButton bsStyle="default" className="styleBtn" style={styleButton} title="Select Type" pullRight id="split-button-pull-right">
                {typeList}
              </DropdownButton>
            </Col>
            </Row>
          </Col>
      </Wrapper>
      <clearFix />
      <PropertyTile/>
    </Col>
    );
}

PropertyListHeader.propTypes = {

};

export default PropertyListHeader;
