/**
*
* FeaturesSection
*
*/

import React from 'react';
// import styled from 'styled-components';
import { headerTitle, featuresWrap, rowStyle } from './featuresStyles';
import { Row, Col, Button, Media } from 'react-bootstrap';

import fi1 from './bit.png';
import fi2 from './crossborder.png';
import fi3 from './crowdfunding.png';
import fi4 from './debtfunding.png';
import fi5 from './digitaltoken.png';
import fi6 from './share.png';
import fi7 from './simpleplatform.png';
import fi8 from './smart.png';
import fi9 from './socialrealstate.png';

function FeaturesSection() {
  return (
    <Col style={featuresWrap} id="featuresSection">
      <h1 style={headerTitle} className="landingTitle">Features of EstateChain</h1>
      <Row style={rowStyle}>
        <Col sm={4}>
          <img src={fi1} />
          <h1>Crypto or Fiat</h1>
          <p>Use Crypto or Fiat Currencies to participate in a property crowdsale.</p>
        </Col>
        <Col sm={4}>
          <img src={fi8} />
          <h1>Smart Property Contracts</h1>
          <p>Unique smart contracts for leasing, management, and services for investment transparency.</p>
        </Col>
        <Col sm={4}>
          <img src={fi3} />
          <h1>CrowdFunding</h1>
          <p>Property Token Sales open to everyone for fractional ownership of property on blockchain.</p>
        </Col>
      </Row><Row style={rowStyle}>
        <Col sm={4}>
          <img src={fi7} />
          <h1>Simple Platform</h1>
          <p>Ease-of-use platform for review of investments, property details & returns.</p>
        </Col>
        <Col sm={4}>
          <img src={fi5} />
          <h1>Digital Tokens</h1>
          <p>Digital tokens on a cryptographically secured Ethereum platform for security & reliability.</p>
        </Col>
        <Col sm={4}>
          <img src={fi6} />
          <h1>Single Touch Transfer</h1>
          <p>Unique tokenization allows investors to trade P2P with a single click.</p>
        </Col>
      </Row><Row style={rowStyle}>
        <Col sm={4}>
          <img src={fi4} />
          <h1>Debt CrowdFunding</h1>
          <p>Innovative debt crowdfunding available for select properties for greater leverage.</p>
        </Col>
        <Col sm={4}>
          <img src={fi9} />
          <h1>Social Real Estate</h1>
          <p>Engage in a vibrant community with other split-property owners worldwide on EstateChain.</p>
        </Col>
        <Col sm={4}>
          <img src={fi2} />
          <h1>Cross-Border Ownership</h1>
          <p>Own properties in multiple popular cities all over the world with ease & security.</p>
        </Col>
      </Row>
    </Col>
  );
}

FeaturesSection.propTypes = {

};

export default FeaturesSection;
