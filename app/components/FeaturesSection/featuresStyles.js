import styled from 'styled-components';

const Wrapper = styled.div`
  width: 100%;
`;

export const headerTitle = {
  fontFamily: 'Open Sans',
  fontWeight: 'bold',
  fontSize: '36px',
  color: '#4a4a4a',
  textAlign: 'center'
}

export const rowStyle = {
    margin: '50px 0'
}

export const featuresWrap = {
  padding: '0 10vw',
  textAlign: 'center'
}


export default Wrapper;
