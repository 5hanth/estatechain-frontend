/**
*
* Esnavbar
*
*/

import React from 'react';
// import styled from 'styled-components';

import { Image,Navbar,hr, NavItem, MenuItem, NavDropdown, Nav } from 'react-bootstrap';
import navbarStyles from './navbarStyles';
import logo from './logo.png';

function Esnavbar() {
  return (
    <Navbar inverse collapseOnSelect style={navbarStyles.main}>
    <Navbar.Header>
      <Navbar.Brand>
      <a href="https://estatechain.io"><Image src={logo} id="navImgOut" responsive/></a>
      </Navbar.Brand>
      <Navbar.Toggle />
    </Navbar.Header>
    <Navbar.Collapse>
      <Nav pullRight style={navbarStyles.rightMenu}>
        <MenuItem eventKey={1} href="/">Home</MenuItem>
        <MenuItem eventKey={2} href="/properties">Properties</MenuItem>
        {/* <NavDropdown eventKey={3} title="Learn" id="basic-nav-dropdown">
          <MenuItem eventKey={3.1}>Action</MenuItem>
          <MenuItem eventKey={3.2}>Another action</MenuItem>
          <MenuItem eventKey={3.3}>Something else here</MenuItem>
          <MenuItem divider />
          <MenuItem eventKey={3.4}>Separated link</MenuItem>
        </NavDropdown> */}
        <MenuItem eventKey={4} href="/comingsoon">Token Sale</MenuItem>
        <MenuItem eventKey={5} href="/auth" id="login-btn">Login</MenuItem>
        <MenuItem eventKey={6} href="/auth" id="signup-btn">Signup</MenuItem>
      </Nav>
    </Navbar.Collapse>
  </Navbar>
  );
}

Esnavbar.propTypes = {

};

export default Esnavbar;
