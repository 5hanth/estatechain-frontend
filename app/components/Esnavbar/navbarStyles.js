const navbarStyles = {
  main:{
    border: 'none',
    background: 'linear-gradient(135deg, rgb(66, 92, 122) 0%, rgb(53, 80, 112) 25%, rgb(32,52, 79) 75%,  rgb(18, 34, 55) 100%)',
    position: 'fixed',
    width:'100%',
    height: '98px',
    zIndex:'999',
    top: '0px'
  },
  rightMenu:{
    marginTop:'20px',
  }
}

export default navbarStyles;
