import styled from 'styled-components';
import Bg from './bg.png';

const Styles = {
  mainContainer:{
    //marginLeft:'10px',
  },
    header:{
        background: 'linear-gradient(135deg, rgb(66, 92, 122) 0%, rgb(53, 80, 112) 25%, rgb(32,52, 79) 75%, rgb(18, 34, 55) 100%)',
        color: '#ffffff',
        height:'56px',
    },
    infoLeft:{
        height: '24px',
        fontFamily: 'Open Sans',
        fontSize: '24px',
        letterSpacing: '0.6px',
        textAlign: 'left',
        color: '#ffffff',
        textShadow: '0px 3px 7px rgba(21, 22, 22, 0.35)',
      },
    infoRight:{
        height: '21px',
        fontFamily: 'Open Sans',
        fontSize: '24px',
        letterSpacing: '0.6px',
        textAlign: 'right',
        color: '#ffffff',
        textShadow: '0px 3px 7px rgba(21, 22, 22, 0.35)',
      },
      infoWallet:{
          border:'1px solid rgba(21, 22, 22, 0.35)',
          backgroundColor:'#FFF',
      },
      infoWalletLeft:{
        height: '14px',
        fontFamily: 'Open Sans',
        fontSize: '18px',
        textAlign: 'left',
        color: '#464646',
      },
      infoWalletRight:{
        height: '14px',
        fontFamily: 'Open Sans',
        fontSize: '18px',
        textAlign: 'right',
        color: '#464646',
      },
      seperateLine:{
        width: 'auto',
        height: '1px',
        margin:'15px 0px',
        backgroundColor: '#959595',
        border: 'solid 1px #959595',
      },
}

export default Styles;
