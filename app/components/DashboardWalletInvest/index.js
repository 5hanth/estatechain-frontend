import React from 'react';
import {Clearfix, Row,Col, Button} from 'react-bootstrap';
import styles from './style.js';

function DashboardWalletInvest(props){
    var invest = props.invest[0];
    var wallet = props.wallet[0].wallet;
    console.log(invest);
    return (
        <Row style={styles.mainContainer}>
            <Col sm={12} style = {styles.header}>
                <Row>
                    <Col sm={8} ><h1 style={styles.infoLeft}>My Account</h1></Col>
                    <Col sm={4}><h1 style={styles.infoRight}>{wallet.total_tokens_value}</h1></Col>
                </Row>
            </Col>

            <Col sm={12} style = {styles.infoWallet} id="tokensNav">
                <Row>
                    <Col sm={7} xs={8}><h1 style={styles.infoWalletLeft}>Total tokens owned</h1></Col>
                    <Col sm={5} xs={4}><h1 style={styles.infoWalletRight}>{invest.tokens_owned}</h1></Col>
                    <Col sm={7} xs={8}><h1 style={styles.infoWalletLeft}>Total no. of properties invested</h1></Col>
                    <Col sm={5} xs={4}><h1 style={styles.infoWalletRight}>{invest.properties_invested}</h1></Col>
                    <Col sm={7} xs={8}><h1 style={styles.infoWalletLeft}>Last transaction made on</h1></Col>
                    <Col sm={5} xs={4}><h1 style={styles.infoWalletRight}>{invest.last_transaction}</h1></Col>
                </Row>
                <p style={styles.seperateLine} ></p>
                <Row>
                    <Col sm={8} xs={8}><h1 style={styles.infoWalletLeft}>Latest valuation of tokens</h1></Col>
                    <Col sm={4} xs={4}><h1 style={styles.infoWalletRight}>{invest.value_of_tokens}</h1></Col>
                    <Col sm={8} xs={8}><h1 style={styles.infoWalletLeft}>Total rental dividend received</h1></Col>
                    <Col sm={4} xs={4}><h1 style={styles.infoWalletRight}>$23</h1></Col>
                </Row>
            </Col>
        </Row>
    );
}

DashboardWalletInvest.propTypes = {
};

export default DashboardWalletInvest;
