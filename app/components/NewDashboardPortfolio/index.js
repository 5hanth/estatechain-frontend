import React from 'react';
import {browserHistory} from 'react-router';
import {Clearfix, Row,Col, Button,Image,Glyphicon} from 'react-bootstrap';
import styles from './style.js';
import img from './bg.png';

function NewDashboardPortfolio(props){

    function handlePlus(e){
        browserHistory.push("/properties");
    }
    //console.log('===>'+JSON.stringify(props.portfolio[0].properties));
    var portfolio1 = props.portfolio[0].properties;
    var Balance = portfolio1.map(function(input,index){
        var img = input.img_url;

        return  <Col sm={12} key={index}  style={styles.balanceTile}>
                    <span style={{...styles.colorBlock,...{backgroundColor: input.color}}}></span>
                    <Col sm={2}>
                        <Image src={img} responsive/>
                    </Col>
                    <Col sm={3}>
                        <h5 style={styles.propDetail}>EST {input.id}</h5>
                        <h6 style={styles.propDetail}>{input.name}</h6>
                        <h4 style={styles.propDetail}>{input.address}</h4>
                    </Col>
                    <span style={styles.colorBlock1}></span>
                    <Col sm={2} style={styles.rmvPadding}>
                        <h6 style={styles.propDetail2}>Rental Income</h6>
                        <h5></h5>
                        <h4 style={styles.propDetail3}>{input.net_rental_dividend}</h4>
                    </Col>
                    <span style={styles.colorBlock1}></span>
                    <Col sm={2} style={styles.rmvPadding}>
                        <h6 style={styles.propDetail3}>Return</h6>
                        <h5></h5>
                        <h4 style={styles.propDetail3}>{input.estimated_value_growth}</h4>
                    </Col>
                    <span style={styles.colorBlock1}></span>
                    <Col sm={3} style={styles.rmvPadding}>
                        <h6 style={styles.propDetail3}>Token Value</h6>
                        <h5></h5>
                        <h4 style={styles.propDetail3}>${input.current_price}</h4>
                    </Col>

                     {/*<Col sm={12} style={styles.leftStyle}>

                        <Row style={styles.noMar}>
                          <p><b>{input.name}</b></p>
                        </Row>
                        <p style={styles.noMar}>
                            Token’s value <b>{input.token_price}</b>
                        </p>
                        <p style={styles.noMar}>
                            Token bought <b>{input.tokens_owned}</b>
                        </p>
                    </Col>
                    {/* <Col sm={3} style={styles.rightStyle}>
                        {input.value}
                        </Col> */}

                    {/* // <Col sm={12} style={styles.leftStyle}>
                    //     <Row>
                    //         Token bought  {input.bought}
                    //     </Row>
                    // </Col> */}

                        {/* <Col sm={3} style={styles.rightStyle}>
                        {input.bought}
                    </Col> */}
                </Col>
    })
    return(
            <Row style={styles.containerDiv}>
                <Col sm={12} style={styles.header}>
                    <h1 style={styles.headerData}>My Portfolio</h1>
                    <p style={styles.seperateLine} ></p>
                </Col>
                <Col sm={12} style={styles.scroller} className="hide-scrollbar">
                  {Balance}
                  <Col sm={4} md={4}>
                      <Col sm={12} style={styles.emptyBox} onClick={handlePlus}>
                          <Glyphicon glyph="plus" style={styles.glyph}/>
                      </Col>
                  </Col>
                </Col>
            </Row>
    );
}

NewDashboardPortfolio.propTypes = {
};

export default NewDashboardPortfolio;
