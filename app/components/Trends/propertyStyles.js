import styled from 'styled-components';

const propertyStyles = {
  Wrapper: {
    background: '#fff',
    height: '50px',
    borderRadius: '2px',
    width: 'calc(100% - 20px)',
    margin: '10px',
  },

  headerTitle: {
    fontFamily: 'Open Sans',
    textAlign: 'center',
    fontSize: '30px',
    margin: '11px',
    color: '#fff'
  },

  propText: {
    textAlign: 'center',
    color: '#fff',
    fontSize: '18px'
  },

  propButton: {
    background: 'transparent',
    color: 'white',
    borderRadius: '20px',
    width: '200px',
    margin: '10px 0px',
    border: '2px solid'
  },

  propRow: {
    margin: '150px 0px'
  },
  rowHead: {
    height: '30px',
  },
  loading:{
    position:'fixed',
    top:'0px',
    marginLeft:'-55px',
    width:'100%',
    height:'100%',
    background:'linear-gradient(135deg, rgb(66, 92, 122) 0%, rgb(53, 80, 112) 25%, rgb(32,52, 79) 75%,  rgb(18, 34, 55) 100%)',
    //background-image:url('ajax-loader.gif');
    backgroundRepeat:'no-repeat',
    backgroundPosition:'center',
    zIndex:'999',
    opacity: '0.6',
    filter: 'alpha(opacity=40)', /* For IE8 and earlier */
  },
  loadImg:{
    margin:'25% 45%',
  },
  rowHeadTitle: {
    fontSize: '18px',
	  textAlign: 'center',
    lineHeight: '40px',
    margin: '0'
  },
  rowHeadContainer: {
    margin: '5px 0'
  },
  leftBorder:{
    borderRight: '1px solid rgb(53, 130, 225)',
    margin: '5px 0'
  },
  rowHeadtime: {
    color: 'rgb(160,160,160)',
    fontSize: '12px',
    lineHeight: '40px',
    margin: '0'
  },
  rowHeadData:{
    color: 'rgb(106,106,106)',
    lineHeight: '40px',
    fontSize: '12px',
    margin: '0'
  },
  trendsData: {
    display: 'flex',
    flexDirection: 'row',
    padding: '0 35px',
    justifyContent: 'space-around'
  }
}


export default propertyStyles;
