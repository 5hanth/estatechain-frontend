/**
*
* Trends
*
*/

import React from 'react';
// import styled from 'styled-components';
import { Grid, Row, Col,Carousel,clearFix, Button, Thumbnail } from 'react-bootstrap';
import api from '../../utils/api';
import propertyStyles from './propertyStyles';
import Bg from './rolling.svg';

class Trends extends React.Component{
  constructor(props) {
    super(props);
    this.state = { properties: []}
  }
  componentDidMount(){
    api.get("trends", {}).then(resp => {
      if(resp.data.error===false){
      this.setState({
                properties: resp.data.properties
              })
      }
    })
  }
  render(){
    if(this.state.properties.length<1){
      return(
          <Row style={propertyStyles.loading}>
              <img src={Bg} style={propertyStyles.loadImg}/>
          </Row>
      )
  }
    return (
      <div style={propertyStyles.Wrapper} className="trendWrap">
        <Row  style={propertyStyles.rowHead} className="trendWrapRow">
          <Col sm={3} xs={12} md={3} style={propertyStyles.leftBorder} className="removeBorder">
            <h2 style={propertyStyles.rowHeadTitle}>Market Trends</h2>
          </Col>
          <Col sm={9} xs={12} md={9} style={propertyStyles.rowHeadContainer} className="carouselDiv">
            <Row  style={propertyStyles.rowHead} className="rowHead">
              <Carousel style={propertyStyles.wrapperStyle} controls={false} indicators={false} interval={2000}>
                <Carousel.Item>
                      <Col sm={6} xs={12}>
                        <div style={propertyStyles.trendsData} className="carouselData row">
                            <h2 style={propertyStyles.rowHeadtime}>{this.state.properties[0].updated_at}</h2>

                            <h2 style={propertyStyles.rowHeadData}>{this.state.properties[0].address}</h2>

                            <h2 style={propertyStyles.rowHeadData}>Token:  {this.state.properties[0].token_price}</h2>
                            <span className="arrow-up"></span>
                        </div>

                      </Col>
                      <clearFix />
                      <Col sm={6} xs={12}>
                        <div style={propertyStyles.trendsData} className="carouselData">
                            <h2 style={propertyStyles.rowHeadtime}>{this.state.properties[1].updated_at}</h2>

                            <h2 style={propertyStyles.rowHeadData}>{this.state.properties[1].address}</h2>

                            <h2 style={propertyStyles.rowHeadData}>Token:  {this.state.properties[1].token_price}</h2>
                            <span className="arrow-down"></span>
                        </div>
                      </Col>
                </Carousel.Item>
                <Carousel.Item>
                    <Col sm={12} xs={12}>
                    <Row>
                      <Col sm={6} xs={12}>
                        <div style={propertyStyles.trendsData} className="carouselData row">
                            <h2 style={propertyStyles.rowHeadtime}>{this.state.properties[2].updated_at}</h2>

                            <h2 style={propertyStyles.rowHeadData}>{this.state.properties[2].address}</h2>

                            <h2 style={propertyStyles.rowHeadData}>Token:  {this.state.properties[2].token_price}</h2>
                            <span className="arrow-up"></span>
                        </div>

                      </Col>
                      <clearFix />
                      <Col sm={6} xs={12}>
                        <div style={propertyStyles.trendsData} className="carouselData">
                            <h2 style={propertyStyles.rowHeadtime}>{this.state.properties[3].updated_at}</h2>

                            <h2 style={propertyStyles.rowHeadData}>{this.state.properties[3].address}</h2>

                            <h2 style={propertyStyles.rowHeadData}>Token:  {this.state.properties[3].token_price}</h2>
                            <span className="arrow-up"></span>
                        </div>
                      </Col>
                    </Row>
                    </Col>
                </Carousel.Item>
                <Carousel.Item>
                    <Col sm={12} xs={12}>
                    <Row>
                      <Col sm={6} xs={12}>
                        <div style={propertyStyles.trendsData} className="carouselData row">
                            <h2 style={propertyStyles.rowHeadtime}>{this.state.properties[4].updated_at}</h2>

                            <h2 style={propertyStyles.rowHeadData}>{this.state.properties[4].address}</h2>

                            <h2 style={propertyStyles.rowHeadData}>Token:  {this.state.properties[4].token_price}</h2>
                            <span className="arrow-up"></span>
                        </div>

                      </Col>
                      <clearFix />
                      <Col sm={6} xs={12}>
                        <div style={propertyStyles.trendsData} className="carouselData">
                            <h2 style={propertyStyles.rowHeadtime}>{this.state.properties[5].updated_at}</h2>

                            <h2 style={propertyStyles.rowHeadData}>{this.state.properties[5].address}</h2>

                            <h2 style={propertyStyles.rowHeadData}>Token:  {this.state.properties[5].token_price}</h2>
                            <span className="arrow-down"></span>
                        </div>
                      </Col>
                    </Row>
                    </Col>
                </Carousel.Item>
                <Carousel.Item>
                    <Col sm={12} xs={12}>
                    <Row>
                      <Col sm={6} xs={12}>
                        <div style={propertyStyles.trendsData} className="carouselData row">
                            <h2 style={propertyStyles.rowHeadtime}>{this.state.properties[6].updated_at}</h2>

                            <h2 style={propertyStyles.rowHeadData}>{this.state.properties[6].address}</h2>

                            <h2 style={propertyStyles.rowHeadData}>Token:  {this.state.properties[6].token_price}</h2>
                            <span className="arrow-down"></span>
                        </div>

                      </Col>
                      <clearFix />
                      <Col sm={6} xs={12}>
                        <div style={propertyStyles.trendsData} className="carouselData">
                            <h2 style={propertyStyles.rowHeadtime}>{this.state.properties[7].updated_at}</h2>

                            <h2 style={propertyStyles.rowHeadData}>{this.state.properties[7].address}</h2>

                            <h2 style={propertyStyles.rowHeadData}>Token:  {this.state.properties[7].token_price}</h2>
                            <span className="arrow-up"></span>
                        </div>
                      </Col>
                    </Row>
                    </Col>
                </Carousel.Item>
                <Carousel.Item>
                    <Col sm={12} xs={12}>
                    <Row>
                      <Col sm={6} xs={12}>
                        <div style={propertyStyles.trendsData} className="carouselData">
                            <h2 style={propertyStyles.rowHeadtime}>{this.state.properties[8].updated_at}</h2>

                            <h2 style={propertyStyles.rowHeadData}>{this.state.properties[8].address}</h2>

                            <h2 style={propertyStyles.rowHeadData}>Token:  {this.state.properties[8].token_price}</h2>
                            <span className="arrow-up"></span>
                        </div>

                      </Col>
                    </Row>
                    </Col>
                </Carousel.Item>
              </Carousel>
            </Row>

            <Row  style={propertyStyles.rowHead} className="mobHead">
              {/*<div className="mobAnimate">
                <div className="innerAnimate">
                  <div className="data">
                    {this.state.properties[0].updated_at}
                  </div>
                  <div className="data1">
                    {this.state.properties[0].address}
                  </div>
                  <div className="data1">
                    Token:  {this.state.properties[0].token_price}
                  </div>
                  <span className="arrow-up"></span>
                </div>

                <div className="innerAnimate">
                  <div className="data">
                    {this.state.properties[1].updated_at}
                  </div>
                  <div className="data1">
                    {this.state.properties[1].address}
                  </div>
                  <div className="data1">
                    Token:  {this.state.properties[1].token_price}
                  </div>
                  <span className="arrow-up"></span>
                </div>

                <div className="innerAnimate">
                  <div className="data">
                    {this.state.properties[2].updated_at}
                  </div>
                  <div className="data1">
                    {this.state.properties[2].address}
                  </div>
                  <div className="data1">
                    Token:  {this.state.properties[2].token_price}
                  </div>
                  <span className="arrow-up"></span>
                </div>

                <div className="innerAnimate">
                  <div className="data">
                    {this.state.properties[3].updated_at}
                  </div>
                  <div className="data1">
                    {this.state.properties[3].address}
                  </div>
                  <div className="data1">
                    Token:  {this.state.properties[3].token_price}
                  </div>
                  <span className="arrow-down"></span>
                </div>

                <div className="innerAnimate">
                  <div className="data">
                    {this.state.properties[4].updated_at}
                  </div>
                  <div className="data1">
                    {this.state.properties[4].address}
                  </div>
                  <div className="data1">
                    Token:  {this.state.properties[4].token_price}
                  </div>
                  <span className="arrow-up"></span>
                </div>

                <div className="innerAnimate">
                  <div className="data">
                    {this.state.properties[5].updated_at}
                  </div>
                  <div className="data1">
                    {this.state.properties[5].address}
                  </div>
                  <div className="data1">
                    Token:  {this.state.properties[5].token_price}
                  </div>
                  <span className="arrow-up"></span>
                </div>

                <div className="innerAnimate">
                  <div className="data">
                    {this.state.properties[6].updated_at}
                  </div>
                  <div className="data1">
                    {this.state.properties[6].address}
                  </div>
                  <div className="data1">
                    Token:  {this.state.properties[6].token_price}
                  </div>
                  <span className="arrow-down"></span>
                </div>

                <div className="innerAnimate">
                  <div className="data">
                    {this.state.properties[7].updated_at}
                  </div>
                  <div className="data1">
                    {this.state.properties[7].address}
                  </div>
                  <div className="data1">
                    Token:  {this.state.properties[7].token_price}
                  </div>
                  <span className="arrow-down"></span>
                </div>

                <div className="innerAnimate">
                  <div className="data">
                    {this.state.properties[8].updated_at}
                  </div>
                  <div className="data1">
                    {this.state.properties[8].address}
                  </div>
                  <div className="data1">
                    Token:  {this.state.properties[8].token_price}
                  </div>
                  <span className="arrow-up"></span>
                </div>

              </div>*/}

              <marquee style={{height: "-webkit-fill-available",paddingTop: '10px'}}>
                  {this.state.properties[0].updated_at}  {this.state.properties[0].address}  Token:  {this.state.properties[0].token_price}
                  <span className="arrow-up"></span>


                  {this.state.properties[1].updated_at}   {this.state.properties[1].address}   Token:  {this.state.properties[1].token_price}
                  <span className="arrow-up"></span>

                  {this.state.properties[2].updated_at}   {this.state.properties[2].address}   Token:  {this.state.properties[2].token_price}
                  <span className="arrow-up"></span>

                  {this.state.properties[3].updated_at}   {this.state.properties[3].address}   Token:  {this.state.properties[3].token_price}
                  <span className="arrow-up"></span>

                  {this.state.properties[4].updated_at}   {this.state.properties[4].address}   Token:  {this.state.properties[4].token_price}
                  <span className="arrow-up"></span>

                  {this.state.properties[5].updated_at}   {this.state.properties[5].address}   Token:  {this.state.properties[5].token_price}
                  <span className="arrow-up"></span>

                  {this.state.properties[6].updated_at}   {this.state.properties[6].address}   Token:  {this.state.properties[6].token_price}
                  <span className="arrow-up"></span>

                  {this.state.properties[7].updated_at}   {this.state.properties[7].address}   Token:  {this.state.properties[7].token_price}
                  <span className="arrow-up"></span>

                  {this.state.properties[8].updated_at}   {this.state.properties[8].address}   Token:  {this.state.properties[8].token_price}
                  <span className="arrow-up"></span>
              </marquee>
              {/* <Carousel style={propertyStyles.wrapperStyle} controls={false} indicators={false} interval={2000}>
                <Carousel.Item>
                      <Col sm={6} xs={12}>
                        <div style={propertyStyles.trendsData} className="carouselData row">
                            <h2 style={propertyStyles.rowHeadtime}>{this.state.properties[0].updated_at}</h2>

                            <h2 style={propertyStyles.rowHeadData}>{this.state.properties[0].address}</h2>

                            <h2 style={propertyStyles.rowHeadData}>Token:  {this.state.properties[0].token_price}</h2>
                            <span className="arrow-up"></span>
                        </div>

                      </Col>
                </Carousel.Item>
                <Carousel.Item>
                    <Col sm={12} xs={12}>
                    <Row>
                    <Col sm={6} xs={12}>
                        <div style={propertyStyles.trendsData} className="carouselData">
                            <h2 style={propertyStyles.rowHeadtime}>{this.state.properties[1].updated_at}</h2>

                            <h2 style={propertyStyles.rowHeadData}>{this.state.properties[1].address}</h2>

                            <h2 style={propertyStyles.rowHeadData}>Token:  {this.state.properties[1].token_price}</h2>
                            <span className="arrow-down"></span>
                        </div>
                      </Col>
                    </Row>
                    </Col>
                </Carousel.Item>
                <Carousel.Item>
                    <Col sm={12} xs={12}>
                    <Row>
                      <Col sm={6} xs={12}>
                        <div style={propertyStyles.trendsData} className="carouselData row">
                            <h2 style={propertyStyles.rowHeadtime}>{this.state.properties[2].updated_at}</h2>

                            <h2 style={propertyStyles.rowHeadData}>{this.state.properties[2].address}</h2>

                            <h2 style={propertyStyles.rowHeadData}>Token:  {this.state.properties[2].token_price}</h2>
                            <span className="arrow-up"></span>
                        </div>

                      </Col>
                    </Row>
                    </Col>
                </Carousel.Item>
                <Carousel.Item>
                    <Col sm={12} xs={12}>
                    <Row>
                    <Col sm={6} xs={12}>
                        <div style={propertyStyles.trendsData} className="carouselData">
                            <h2 style={propertyStyles.rowHeadtime}>{this.state.properties[3].updated_at}</h2>

                            <h2 style={propertyStyles.rowHeadData}>{this.state.properties[3].address}</h2>

                            <h2 style={propertyStyles.rowHeadData}>Token:  {this.state.properties[3].token_price}</h2>
                            <span className="arrow-down"></span>
                        </div>
                      </Col>
                    </Row>
                    </Col>
                </Carousel.Item>
                <Carousel.Item>
                    <Col sm={12} xs={12}>
                    <Row>
                      <Col sm={6} xs={12}>
                        <div style={propertyStyles.trendsData} className="carouselData row">
                            <h2 style={propertyStyles.rowHeadtime}>{this.state.properties[4].updated_at}</h2>

                            <h2 style={propertyStyles.rowHeadData}>{this.state.properties[4].address}</h2>

                            <h2 style={propertyStyles.rowHeadData}>Token:  {this.state.properties[4].token_price}</h2>
                            <span className="arrow-up"></span>
                        </div>

                      </Col>
                    </Row>
                    </Col>
                </Carousel.Item>
                <Carousel.Item>
                    <Col sm={12} xs={12}>
                    <Row>
                    <Col sm={6} xs={12}>
                        <div style={propertyStyles.trendsData} className="carouselData">
                            <h2 style={propertyStyles.rowHeadtime}>{this.state.properties[5].updated_at}</h2>

                            <h2 style={propertyStyles.rowHeadData}>{this.state.properties[5].address}</h2>

                            <h2 style={propertyStyles.rowHeadData}>Token:  {this.state.properties[5].token_price}</h2>
                            <span className="arrow-down"></span>
                        </div>
                      </Col>
                    </Row>
                    </Col>
                </Carousel.Item>
                <Carousel.Item>
                    <Col sm={12} xs={12}>
                    <Row>
                      <Col sm={6} xs={12}>
                        <div style={propertyStyles.trendsData} className="carouselData row">
                            <h2 style={propertyStyles.rowHeadtime}>{this.state.properties[6].updated_at}</h2>

                            <h2 style={propertyStyles.rowHeadData}>{this.state.properties[6].address}</h2>

                            <h2 style={propertyStyles.rowHeadData}>Token:  {this.state.properties[6].token_price}</h2>
                            <span className="arrow-down"></span>
                        </div>

                      </Col>
                    </Row>
                    </Col>
                </Carousel.Item>
                <Carousel.Item>
                    <Col sm={12} xs={12}>
                    <Row>
                    <Col sm={6} xs={12}>
                        <div style={propertyStyles.trendsData} className="carouselData">
                            <h2 style={propertyStyles.rowHeadtime}>{this.state.properties[7].updated_at}</h2>

                            <h2 style={propertyStyles.rowHeadData}>{this.state.properties[7].address}</h2>

                            <h2 style={propertyStyles.rowHeadData}>Token:  {this.state.properties[7].token_price}</h2>
                            <span className="arrow-down"></span>
                        </div>
                      </Col>
                    </Row>
                    </Col>
                </Carousel.Item>
                <Carousel.Item>
                    <Col sm={12} xs={12}>
                    <Row>
                      <Col sm={6} xs={12}>
                        <div style={propertyStyles.trendsData} className="carouselData">
                            <h2 style={propertyStyles.rowHeadtime}>{this.state.properties[8].updated_at}</h2>

                            <h2 style={propertyStyles.rowHeadData}>{this.state.properties[8].address}</h2>

                            <h2 style={propertyStyles.rowHeadData}>Token:  {this.state.properties[8].token_price}</h2>
                            <span className="arrow-up"></span>
                        </div>

                      </Col>
                    </Row>
                    </Col>
                </Carousel.Item>
              </Carousel>   */}
            </Row>

          </Col>
        </Row>
      </div>
    );
  }
}

Trends.propTypes = {};

export default Trends;
