import React from 'react';
import {browserHistory} from 'react-router';
import {Clearfix, Row,Col, Button,Image,Glyphicon} from 'react-bootstrap';
import styles from './style.js';
import img from './bg.png';

function DashboardPortfolio(props){

    function handlePlus(e){
        browserHistory.push("/properties");
    }
    //console.log('===>'+JSON.stringify(props.portfolio[0].properties));
    var portfolio1 = props.portfolio[0].properties;
    var Balance = portfolio1.map(function(input,index){
        var img = input.img_url;

        return  <Col sm={4} key={index}  style={styles.balanceTile}>
                    <Col sm={12}>
                        <Row>
                            <Image src={img} responsive/>
                        </Row>
                    </Col>

                    <Col sm={12} style={styles.leftStyle}>
                        <Row style={{...styles.colorBlock,...{backgroundColor: input.color}}}></Row>
                        <Row style={styles.noMar}>
                          <p><b>{input.name}</b></p>
                        </Row>
                        <p style={styles.noMar}>
                            Token’s value <b>{input.token_price}</b>
                        </p>
                        <p style={styles.noMar}>
                            Token bought <b>{input.tokens_owned}</b>
                        </p>
                    </Col>
                    {/* <Col sm={3} style={styles.rightStyle}>
                        {input.value}
                        </Col> */}

                    {/* // <Col sm={12} style={styles.leftStyle}>
                    //     <Row>
                    //         Token bought  {input.bought}
                    //     </Row>
                    // </Col> */}

                        {/* <Col sm={3} style={styles.rightStyle}>
                        {input.bought}
                    </Col> */}
                </Col>
    })
    return(
            <Row style={styles.containerDiv}>
                <Col sm={12} style={styles.header}>
                    <h1 style={styles.headerData}>My Portfolio</h1>
                </Col>
                <Col sm={12} style={styles.scroller} className="hide-scrollbar">
                  {Balance}
                  <Col sm={4} md={4}>
                      <Col sm={12} style={styles.emptyBox} onClick={handlePlus}>
                          <Glyphicon glyph="plus" style={styles.glyph}/>
                      </Col>
                  </Col>
                </Col>
            </Row>
    );
}

DashboardPortfolio.propTypes = {
};

export default DashboardPortfolio;
