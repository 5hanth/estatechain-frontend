import styled from 'styled-components';
import Bg from './bg.1.png';
const Styles = {
    containerDiv:{
        border :'2px solid lightgrey',
        background:'#fff',
    },
    scroller: {
      maxHeight: '270px',
      overflowY: 'scroll'
    },
    header:{
        background: 'linear-gradient(135deg, rgb(66, 92, 122) 0%, rgb(53, 80, 112) 25%, rgb(32,52, 79) 75%, rgb(18, 34, 55) 100%)',
        height:'56px',
    },
    headerData:{
        fontFamily: 'Open Sans',
        fontSize: '24px',
        height: '24px',
        lineHeight: '50px',
        letterSpacing: '0.6px',
        textAlign: 'left',
        color: '#ffffff',
        margin: '0',
        padding: '10px auto',
        textShadow: '0px 3px 7px rgba(21, 22, 22, 0.35)',
    },
    balanceTile:{
        margin: '0px 0px 2px',
        padding: '7.5px'
    },
    leftStyle:{
        fontFamily: 'Open Sans',
        fontSize: '14px',
        color: '#464646',
        borderLeft: '2px solid lightgrey',
        borderRight: '2px solid lightgrey',
        borderBottom: '2px solid lightgrey',
        padding: '5px'
    },
    emptyBox:{
        border : '2px solid lightgrey',
        width:'132px',
        height:'162px',
        cursor:'pointer',
        margin: '25px 10px'
    },
    glyph:{
        color:'grey',
        fontSize:'50px',
        top:'40px',
        left:'25%',

    },
    noMar: {
      margin: '0'
    },
    noPad: {
      padding: '0'
    },
    tmpColor: {
      color: 'blue'
    },
    colorBlock: {
      height: '10px',
      width: '20px',
      textAlign: 'right',
      display: 'inline-block',
      margin: '5px 0'
    }
}
export default Styles;
