const detailInfoStyles = {
  infoContainer: {
    margin: '2vw 10vw',
    background: '#fff',
    padding: '20px 30px'
  },
  text: {
    color: '#464646',
    fontSize: '18px',
    fontFamily: 'Open Sans',
    color: '#464646'
  },
  rightText: {
    textAlign: 'right'
  },
  valueStyle: {
    textAlign: 'right',
    fontWeight: 'bold'
  },
  valueCenterStyle: {
    textAlign: 'center',
    fontWeight: '500'
  },
  centerStyle: {
    textAlign: 'center',
    border: '1px solid #000',
    borderRadius: '10px',
    padding: '0 10px',
    margin: '10px 0'
  },
  borderStyle: {
    borderRight: '1px solid #000',
    padding: '35px 0'
  },
  centerContainer: {
    margin: '10px 0 0',
  },
  topSpace: {
    margin: '10px 0',
  },
  rowStyle: {
    margin: '10px 0'
  },
  lastCenter: {
    margin: '10px 0 0 35px'
  },
  buyStyle: {
    lineHeight: '60px',
  },
  buyInfoStyle: {
    border: '1px solid #000',
    borderRadius: '10px',
  },
  buyBtnStyle: {
    width: '100%',
    height: '50px',
    margin: '10px 0',
    borderRadius: '10px',
    backgroundImage: 'linear-gradient(135deg, rgb(66, 92, 122) 0%, rgb(53, 80, 112) 25%, rgb(32, 52, 79) 75%, rgb(18, 34, 55) 100%)',
    color: '#fff',
    fontWeight: 'bold',
    backgroundColor:'linear-gradient(135deg, rgb(66, 92, 122) 0%, rgb(53, 80, 112) 25%, rgb(32, 52, 79) 75%, rgb(18, 34, 55) 100%)'
  },
  
}

export default detailInfoStyles;
