/**
*
* DetailInfo
*
*/

import React from 'react';
import styles from './detailInfoStyles';
import {browserHistory} from 'react-router';
import {Row,Col, Button, ProgressBar, Clearfix} from 'react-bootstrap';


function DetailInfo(props) {
  function handleBuyToken(e){
    //e.preventDefault();
    //console.log(property.id);
    browserHistory.push("/buytoken/"+property.id);
  }
  var property = props.property[0];
  return (

    <Row style={styles.infoContainer} className="propDetailInfoContainer">
    <Clearfix />
     <Col sm={6} xs={12} style={styles.text} className="investmentInfo">
        <Row style={styles.rowStyle} className="sectionDetail">
          <Col sm={6} xs={8}>Total Investment</Col>
          <Col sm={3} xs={4} style={styles.valueStyle}>{property.total_investment}</Col>
        </Row>
        <Row style={styles.rowStyle} className="sectionDetail">
          <Col sm={6} xs={8}>Investment Pending</Col>
          <Col sm={3} xs={4} style={styles.valueStyle}>{property.investment_pending}</Col>
        </Row>
        <Row style={styles.rowStyle} className="sectionDetail">
          <Col sm={6} xs={8}>Total Investors</Col>
          <Col sm={3} xs={4} style={styles.valueStyle}>{property.total_investors}</Col>
        </Row>
        <Row style={styles.centerContainer}>
          <Row style={styles.centerStyle} className="profitEstimate">
            <Col sm={5} xs={5} style={styles.topSpace} className="dataProfit">Net Rental Yield <br/> {property.net_rental_yield}</Col>
            <Col sm={1} xs={1} style={styles.borderStyle} className="seprator"></Col>
            <Col sm={5} xs={5} style={{...styles.topSpace, ...styles.valueCenterStyle, ...styles.lastCenter}} className="dataProfit">Estimated ROI <br/> {property.estimated_roi}</Col>
          </Row>
        </Row>
     </Col>
     <Col sm={5} xs={12} smOffset={1} style={{...styles.text, ...styles.rightText}} className="propertyInfo">
        <Row style={styles.rowStyle}>
          <div>{property.tokens_left} Tokens Left</div>
        </Row>
        <Row style={styles.rowStyle}>
          <ProgressBar striped now={78} />
        </Row>
        <Row style={styles.rowStyle}>
          <div>{property.days_left} Days Left</div>
        </Row>
        <Row style={styles.centerRightContainer}>
          <Col sm={6} xs={12} className="priceDiv" style={{...styles.topSpace, ...styles.valueCenterStyle, ...styles.buyInfoStyle}}>Token Price <br/> {property.token_price}</Col>
          <Col sm={6} xs={12} style={{...styles.buyStyle}}>
            <Button style={styles.buyBtnStyle} className="detailBuyBtn" onClick={handleBuyToken}>Buy Token</Button>
          </Col>
        </Row>
     </Col>
    </Row>
  );
}

DetailInfo.propTypes = {

};

export default DetailInfo;
