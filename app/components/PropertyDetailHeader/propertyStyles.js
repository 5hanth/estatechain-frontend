import styled from 'styled-components';
import Bg from './wrap-bg.png';

const propertyStyles = {
}

export const Wrapper = styled.div`
  overflow: hidden;
  background: url(${Bg}) no-repeat center center;
  height: 120px;
  margin: 10px;
  margin-top:120px;
  background-size: cover;
`;

export const headerTitle = {
  fontFamily: 'Open Sans',
  textAlign: 'center',
  fontSize: '21px',
  margin: '0px',
  color: '#fff',
  paddingTop: '11px'
}

export const propText = {
  fontFamily: 'Open Sans',
  textAlign: 'center',
  color: 'rgba(255, 255, 255,0.8)',
  fontSize: '14px'
}

export const tokenValue = {
  fontWeight: 'bold',
  margin: '0 10px'
}


export const tokenPrice = {
  fontFamily: 'Open Sans',
  textAlign: 'center',
  color: 'rgba(255, 255, 255,1)',
  fontSize: '14px',
  margin: '15px 38vw 0',
  height: '28px',
  lineHeight: '24px',
  background: 'rgba(255, 255, 255,0.38)',
  borderRadius: '10px',
  border: '2px solid #fff'
}

export const propButton = {
  background: 'transparent',
  color: 'white',
  borderRadius: '20px',
  width: '200px',
  margin: '10px 0px',
  border: '2px solid'
}

export const propRow = {
  margin: '150px 0px'
}

export const widthfix = {
  width : '100% - 60px',
  margin: '0px'
  }

export const dropDownContainer = {
  margin: '30px 0 0 8px'
}


export default propertyStyles;
