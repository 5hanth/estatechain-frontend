/**
*
* PropertyDetailHeader
*
*/

import React from 'react';
import { Grid, Row, Col, Button, Thumbnail, SplitButton, MenuItem } from 'react-bootstrap';
import PropertyTile  from 'components/PropertyTile';
import {Wrapper, headerTitle, tokenPrice, tokenValue, propText, propButton, propRow, widthfix, dropDownContainer} from './propertyStyles';



class PropertyDetailHeader extends React.Component {

  constructor(props) {
    super(props);
    this.state = {property: []};
    //console.log("asda"+JSON.stringify(props.property));
  }
  componentDidMount() {
      this.setState({
        property:this.props.property
      })
  }
  render(){
    if(this.state.property!==undefined && this.state.property.length>0){
      //console.log(this.state.property);
      var property = this.state.property[0];
      return (
        <Col>
          <Wrapper className="propDetailHead">
              <Col style={widthfix} className="headContainer">
                <Row style={headerTitle}>{property.address}</Row>
                <Row style={propText}>Independent House, Two Floor</Row>
                <Row className="priceRow"><Col style={tokenPrice} className="propPrice">Token Price <b style={tokenValue}>{property.token_price}</b></Col></Row>
              </Col>
          </Wrapper>
        </Col>
      );
    }
    return (
      <Col>
        <Wrapper>
        </Wrapper>
      </Col>
    );
  }
}

PropertyDetailHeader.propTypes = {

};

export default PropertyDetailHeader;
