import styled from 'styled-components';

const signupStyles = {

  headerTitle: {
    fontFamily: 'Open Sans',
    margin: '30px 0 10px',
    textAlign: 'center',
    fontWeight: 'bold',
    fontSize: '21px',
    textTransform: 'uppercase',
    letterSpacing: '1.5px',
    color: '#696969'
  },

  signupText: {
    textAlign: 'center',
    color: '#696969',
    fontSize: '18px'
  },

  already: {
    color: '#696969'
  },

  loginButton: {
    background: '#fff',
    color: '#696969',
    borderRadius: '20px',
    width: '200px',
    fontSize: '20px',
    fontWeight: 'bold',
    margin: '10px 0px',
    border: '2px solid #696969'
  },

  signupButton: {
    background: 'linear-gradient(135deg, rgb(66, 92, 122) 0%, rgb(53, 80, 112) 25%, rgb(32,52, 79) 75%, rgb(18, 34, 55) 100%)',
    color: '#fff',
    borderRadius: '20px',
    width: '200px',
    fontSize: '20px',
    fontWeight: 'normal',
    letterSpacing: '1px',
    margin: '10px 0px',
    border: '0px solid #fff'
  },

  formInput: {
    width: '300px',
    borderRadius: '20px',
    border: '2px solid',
    color: 'rgb(18, 34, 55)',
    background: 'white',
    margin: '20px auto',
    height:'40px'
  },


  container: {
    textAlign: 'center',
    margin: '25vh auto'
  },
}

export default signupStyles;
