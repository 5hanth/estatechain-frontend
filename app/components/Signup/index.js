/**
*
* Signup
*
*/

import React from 'react';
import {browserHistory} from 'react-router';
import api from '../../utils/api';
// import styled from 'styled-components';
import { Grid, Row, Col, Button, FormGroup, FormControl, HelpBlock, ControlLabel } from 'react-bootstrap';
import signupStyles from './signupStyles';
import Bg from './authbg.png';

class Signup extends React.Component {

  constructor(props) {
    super(props);
    this.state = {email: '', pwd: '', confirmPwd: '', newUser: false};
    this.handleLoginButton = this.handleLoginButton.bind(this);
    this.handleSignupButton = this.handleSignupButton.bind(this);
    this.handleChangeEmail = this.handleChangeEmail.bind(this);
    this.handleChangePassword = this.handleChangePassword.bind(this);
    this.handleChangeConfirm = this.handleChangeConfirm.bind(this);
  }

  getValidationState() {
    return null;
    const email = this.state.email;
    const pwd = this.state.pwd;
    const confirmPwd = this.state.confirmPwd;
    if (pwd !== confirmPwd) return "error";
    if (!email || !pwd || !confirmPwd) return "error";
    if (email && pwd && confirmPwd && (pwd === confirmPwd)) return 'success';
  }

  handleChangeEmail(e) {
    this.setState({email: e.target.value});
  }

  handleChangePassword(e) {
    this.setState({pwd: e.target.value});
  }

  handleChangeConfirm(e) {
    this.setState({confirmPwd: e.target.value});
  }

  handleSignupButton() {
    const { newUser } = this.state;
    if(!this.state.newUser) {
      this.setState({newUser: true});
    }
    if(this.state.newUser == true){
      api.post('register', {"user":{
              "email": this.state.email,
              "password":this.state.pwd
      }})
      .then((resp) =>{
        if(resp.data.error === false){
          localStorage.setItem('token',resp.data.jwt);
          browserHistory.push("/properties");
        }
        else{
          //show error message
          alert(resp.data.msg)
        }
      }).catch(function(error){
        alert("Unable to signup. Please try again with valid parameters.")
      })
    }
  }

  handleLoginButton() {
    if(this.state.newUser) {
      this.setState({newUser: false});
    }
    if(this.state.newUser == false){
      api.post('user_token', {"auth":{
        "email": this.state.email,
        "password":this.state.pwd
      }})
      .then((resp) =>{
        if(resp.data.jwt){
          browserHistory.push("/dashboard");
          localStorage.setItem('token',resp.data.jwt);
        }
        else{
          //show error message
          alert(resp.data.msg)
        }
      }).catch(function(error){
        alert("Invalid credentials")
      })
    }
  }

  signupForm() {
      return (
        <form style={signupStyles.formStyle} className="authForm">
         <FormGroup
           controlId="formBasicText"
           validationState={this.getValidationState()}
         >
           <FormControl
             style={signupStyles.formInput}
             type="text"
             value={this.state.email}
             placeholder="Email"
             onChange={(event) => this.handleChangeEmail(event)}
           />
           <FormControl
             type="password"
             style={signupStyles.formInput}
             value={this.state.pwd}
             placeholder="Password"
             onChange={(event) => this.handleChangePassword(event)}
           />
           <FormControl
             type="password"
             style={signupStyles.formInput}
             id="placeholder"
             value={this.state.confirmPwd}
             placeholder="Confirm Password"
             onChange={(event) => this.handleChangeConfirm(event)}
           />

           <Button style={signupStyles.signupButton} onClick={this.handleSignupButton}>Sign up</Button>

           <p style={signupStyles.already}>Already an existing customer?</p>

           <Button style={signupStyles.loginButton} onClick={this.handleLoginButton}>Login</Button>

         </FormGroup>
       </form>
      );
  }

  signinForm() {
      return (
        <form style={signupStyles.formStyle} className="authForm">
         <FormGroup
           controlId="formBasicText"
           validationState={this.getValidationState()}
         >
           <FormControl
             style={signupStyles.formInput}
             type="text"
             value={this.state.email}
             placeholder="Email"
             onChange={this.handleChangeEmail}
           />
           <FormControl
             type="password"
             style={signupStyles.formInput}
             value={this.state.pwd}
             placeholder="Password"
             onChange={this.handleChangePassword}
           />

           <Button style={signupStyles.signupButton} onClick={this.handleLoginButton}>Login</Button>

           <p style={signupStyles.already}>Don&#39;t have an account?</p>

           <Button style={signupStyles.loginButton} onClick={this.handleSignupButton}>Sign up</Button>

         </FormGroup>
       </form>
      );
  }

  signupHeader() {
    return <Col>
      <h1 style={signupStyles.headerTitle}>Sign up</h1>
      <p style={signupStyles.signupText}>For New Users</p>
    </Col>
  }

  signinHeader() {
    return <Col>
      <h1 style={signupStyles.headerTitle}>Login</h1>
      <p style={signupStyles.signupText}>For Existing Users</p>
    </Col>
  }

  authForm() {
    return this.state.newUser ? this.signupForm() : this.signinForm();
  }

  authHeader() {
    return this.state.newUser ? this.signupHeader() : this.signinHeader();
  }

render() {
    return (
      <Row>
        <Col sm={4} className="hide-mobile"><img src={Bg} style={{width: '40vw', height: '100vh'}} /></Col>
        <Col xs={12} sm={8} style={signupStyles.container}>
          {this.authHeader()}
          {this.authForm()}
        </Col>
      </Row>
    );
  }
}

Signup.propTypes = {

};

export default Signup;
