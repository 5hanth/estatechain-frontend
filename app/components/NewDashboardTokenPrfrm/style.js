import styled from 'styled-components';
const Styles = {
    containerDiv:{
        background:'#fff',
        height:'395px',
    },
    giveBorder:{
       // border:'2px solid lightgrey',
       boxShadow: '0px 3px 7px rgba(21, 22, 22, 0.35)',
    },
    tablehead:{
        fontSize:'18px',
        fontFamily:'Open Sans',
        fontWeight:'500',
        lineHeight:'20px'
    },
    tableData:{
        fontSize:'18px',
        fontFamily:'Open Sans',
        fontWeight:'400',
        lineHeight:'20px',
        textAlign:'center'   
    },
    scroller: {
      maxHeight: '270px',
      overflowY: 'scroll'
    },
    header:{
        //background: 'linear-gradient(135deg, rgb(66, 92, 122) 0%, rgb(53, 80, 112) 25%, rgb(32,52, 79) 75%, rgb(18, 34, 55) 100%)',
        background:'#FFF',
        height:'56px',
        fontFamily: 'Open Sans',
        fontSize: '24px',
    },
    headerData:{
        fontFamily: 'Open Sans',
        fontSize: '24px',
        lineHeight: '50px',
        letterSpacing: '0.6px',
        textAlign: 'left',
        margin: '0',
        padding: '10px auto',
    },
    balanceTile:{
        margin: '0px 0px 2px',
        padding: '7.5px',
        borderBottom:'2px solid lightgrey',
    },
    leftStyle:{
        fontFamily: 'Open Sans',
        fontSize: '14px',
        color: '#464646',
        borderLeft: '2px solid lightgrey',
        borderRight: '2px solid lightgrey',
        borderBottom: '2px solid lightgrey',
        padding: '5px'
    },
    emptyBox:{
        border : '2px solid lightgrey',
        width:'132px',
        height:'162px',
        cursor:'pointer',
        margin: '25px 10px'
    },
    glyph:{
        color:'grey',
        fontSize:'50px',
        top:'40px',
        left:'25%',

    },
    noMar: {
      margin: '0'
    },
    noPad: {
      padding: '0'
    },
    rmvPadding:{
        paddingLeft:'10px',
    },
    tmpColor: {
      color: 'blue'
    },
    colorBlock: {
      height: '45px',
      width: '5px',
      textAlign: 'right',
      display: 'inline-block',
      position: 'absolute',
      marginTop: '1%',
    },
    colorBlock1: {
        height: '60px',
        width: '2px',
        textAlign: 'right',
        display: 'inline-block',
        position: 'absolute',
        marginTop: '0%',
        backgroundColor:'lightgray',
      },
    propDetail:{
        marginTop: '0px',
        marginBottom: '5px',
    },
    propDetail2:{
        marginTop: '0px',
        marginBottom: '5px',
        textAlign:'center',
        width:'max-content',
    },
    propDetail3:{
        marginTop: '0px',
        marginBottom: '5px',
        textAlign:'center',
    },
    seperateLine:{
        width: 'auto',
        height: '1px',
        backgroundColor: '#959595',
        border: 'solid 1px #959595',
    },
}
export default Styles;
