import React from 'react';
import {browserHistory} from 'react-router';
import {Line } from 'react-chartjs-2';
import {Clearfix, Row,Col, Button,Image,Glyphicon,Table} from 'react-bootstrap';
import styles from './style.js';

function NewDashboardTokenPrfrm(props){

    var portfolio1 = props.portfolio[0].properties;
    var Balance =   portfolio1.map(function(input,index){
          return <tr style={styles.tableData}>
              <td>EST {input.id}</td>
              <td>${input.bought_price}</td>
              <td>${input.current_price}</td>
              <td>{input.growth_percent}%</td>
          </tr>
        });

      const data = {
        labels: ['May', 'June', 'July', 'August', 'September', 'October', 'November'],
        datasets: [
          {
            label: 'EST 1',
            fill: false,
            lineTension: 0.1,
            backgroundColor: '#122237',
            borderColor: '#122237',
            borderCapStyle: 'butt',
            borderDash: [],
            borderDashOffset: 0.0,
            borderJoinStyle: 'miter',
            pointBorderColor: '#ffffff',
            pointBackgroundColor: '#fff',
            pointBorderWidth: 1,
            pointHoverRadius: 5,
            pointHoverBackgroundColor: '#122237',
            pointHoverBorderColor: 'rgba(220,220,220,1)',
            pointHoverBorderWidth: 2,
            pointRadius: 1,
            pointHitRadius: 10,
            data: [30, 42, 54, 46, 56, 65, 79]
          },
          {
            label: 'EST 2',
            fill: false,
            lineTension: 0.1,
            backgroundColor: '#4286f4',
            borderColor: '#122237',
            borderCapStyle: 'butt',
            borderDash: [],
            borderDashOffset: 0.0,
            borderJoinStyle: 'miter',
            pointBorderColor: '#ffffff',
            pointBackgroundColor: '#fff',
            pointBorderWidth: 1,
            pointHoverRadius: 5,
            pointHoverBackgroundColor: '#122237',
            pointHoverBorderColor: 'rgba(220,220,220,1)',
            pointHoverBorderWidth: 2,
            pointRadius: 1,
            pointHitRadius: 10,
            data: [10, 22, 64, 36, 46, 35, 69]
          }
        ]
      };

    function handlePlus(e){
        browserHistory.push("/properties");
    }
    return(
            <Row style={styles.containerDiv}>
                <Col sm={5} style={styles.header}>
                    <Col sm={12} style={styles.giveBorder}>
                        <h1 style={styles.headerData}>Performance Of Tokens</h1>
                        <p style={styles.seperateLine} ></p>
                        <Line data={data} />
                    </Col>
                </Col>
                <Col sm={7} style={styles.header}>
                    <Col sm={12} style={styles.giveBorder}>
                        <h1 style={styles.headerData}>Performance Of Tokens</h1>
                        <p style={styles.seperateLine} ></p>
                        <Table responsive>
                            <thead>
                            <tr>
                                <th style={styles.tablehead}>Token Name</th>
                                <th style={styles.tablehead}>Bought Price</th>
                                <th style={styles.tablehead}>Current Price</th>
                                <th style={styles.tablehead}>Percentage Growth</th>
                            </tr>
                            </thead>
                            <tbody>
                              {Balance}
                            </tbody>
                        </Table>
                    </Col>
                </Col>
            </Row>
    );
}

NewDashboardTokenPrfrm.propTypes = {

};

export default NewDashboardTokenPrfrm;
