import React, { Component } from 'react';
import { Pie,Area,Bar,Line,PolarArea } from 'react-chartjs-2';
import { Row, Col,Image, Clearfix } from 'react-bootstrap';
import api from '../../utils/api';
import styles from './Style';
class PieChart extends Component {
    constructor(props) {
        super(props);
        //console.log(props.chart[0].chart);
        this.state = { data: [],labels:[],colors:[] }
        //
    }
    componentDidMount(){
        this.setState({
            data:this.props.chart[0].chart.data,
            labels:this.props.chart[0].chart.labels,
            colors:this.props.chart[0].chart.colors
        })
    }
    render() {
    const data = {
        labels:this.state.labels,
        datasets: [{
            data: this.state.data,
            backgroundColor: this.state.colors,
            hoverBackgroundColor: this.state.colors
        }]
    };
    return (
      <div className='PieChart'>
        <Col xs={12} sm={12} style={{boxShadow: '0px -2px 7px rgba(21, 22, 22, 0.35)', borderBottom: '0px'}}>
            <h1 style={styles.headerData}>Invest Distribution</h1>
        </Col>
        
        <Col xs={12} sm={12} style={{boxShadow: '0px 3px 7px rgba(21, 22, 22, 0.35)', borderTop: '0px',height:"348px"}}>
            <p style={styles.seperateLine} ></p>
            <Pie data={data} legend={{display: false}} height='273' />
        </Col>
      </div>
    );
  }
}
export default PieChart;
