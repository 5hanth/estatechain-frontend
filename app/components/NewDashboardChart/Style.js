import styled from 'styled-components';

const Styles = {
    seperateLine:{
        width: 'auto',
        height: '1px',
        backgroundColor: '#959595',
        border: 'solid 1px #959595',
    },
    headerData:{
        fontFamily: 'Open Sans',
        fontSize: '24px',
        lineHeight: '50px',
        letterSpacing: '0.6px',
        textAlign: 'left',
        margin: '0',
        padding: '10px auto',
    },
}

export default Styles;