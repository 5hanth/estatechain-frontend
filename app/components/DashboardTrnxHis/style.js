import styled from 'styled-components';
import Bg from './bg.png';

const Styles = {
    header:{
        background: 'linear-gradient(135deg, rgb(66, 92, 122) 0%, rgb(53, 80, 112) 25%, rgb(32,52, 79) 75%, rgb(18, 34, 55) 100%)',
        marginTop:'10px',
    },
    headerData:{
        fontFamily: 'Open Sans',
        fontSize: '24px',
        letterSpacing: '0.6px',
        textAlign: 'left',
        color: '#ffffff',
        margin: '0px',
        padding: '10px 0',
        textShadow: '0px 3px 7px rgba(21, 22, 22, 0.35)',
    },
    noTrans: {
      textAlign: 'center',
      fontWeight: 'bold',
      margin: '30px'
    },
    dataSection:{
        backgroundColor:'#FFF',
        height: '58.5vh',
        overflowY: 'scroll'
    },
    trans: {
      margin: '10px 0'
    },
    tokens: {
      fontWeight: 'bold',
      margin: '0 10px'
    },
    token_value: {
      fontWeight: 'bold',
      margin: '0 10px'
    }
}

export default Styles;
