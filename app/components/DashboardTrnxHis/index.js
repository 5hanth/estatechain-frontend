import React from 'react';
import {Clearfix, Row,Col, Button,Image,Glyphicon} from 'react-bootstrap';
import styles from './style.js';
import img from './bg.png';

function DashboardTrnxHis(props){
  var transactions = props.transactions[0];
    return (
        <Row>
            <Col sm={12} style={styles.header} className="noMarginMobile">
                <h1 style={styles.headerData}>Transaction History</h1>
            </Col>
            <Col sm={12} style={styles.dataSection}>
                {transactions.length ? transactions.map(function(trans) {
                  var buy = trans.type == 'Buy' ? 'Bought' : 'Sold';
                  return <Row style={styles.trans}>
                    <Col sm={4} xs={3} style={styles.name}>{trans.name}</Col>
                    <Col sm={1} xs={2} style={styles.tokens}>{trans.tokens}</Col>
                    <Col sm={1} xs={2} style={styles.token_value}>{trans.token_value}</Col>
                    <Col sm={1} xs={2} style={{ margin: '0 10px', color: buy ? 'green' : 'red' }}>{buy ? 'Bought' : 'Sold'}</Col>
                    <Col sm={3} xs={3} style={styles.token_value}>{trans.timestamp}</Col>
                  </Row>
                }) :
                  <div style={styles.noTrans}> You haven&#39;t made any transaction yet.</div>
                }
            </Col>
        </Row>
    )
}

DashboardTrnxHis.propTypes ={};

export default DashboardTrnxHis;
