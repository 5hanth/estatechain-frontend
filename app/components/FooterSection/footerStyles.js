import styled from 'styled-components';

const wrapper = {
  width: '100%',
  background: '#57585b'
}

export const rowStyle = {
    padding: '0 15%'
}

export const links = {
    textAlign: 'center'
}

export const social = {
    textAlign: 'right'
}

export const logoText = {
  width: '300px',
  margin: '20px 0',
  color: '#ffffff',
  fontSize: '14px'
}

export const reach = {
  textAlign: 'center',
  marginLeft: '35%'
}

export default wrapper;
