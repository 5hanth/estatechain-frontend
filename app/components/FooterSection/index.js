/**
*
* FooterSection
*
*/

import React from 'react';
// import styled from 'styled-components';
import {wrapper, rowStyle, logoText, links, social, reach} from './footerStyles';
import { Row, Col, Button, Media } from 'react-bootstrap';
import logo from './logo.png';
import fb from './fb.png';
import tw from './tw.png';
import ln from './ln.png';

function FooterSection() {
  return (
    <div id="footerSection">
      <Row style={rowStyle} className="footerRow">
        <Col md={4}>
          <img src={logo} className="footerImg"/>
          <p style={logoText} className="text-center">Estate chain is blockchain and AI based real estate investment  and crowdfunding platform for everyone around the world.</p>
        </Col>
        <Col md={4} style={links}>
          <p>About Us</p>
          <p>Career</p>
          <p>Contact</p>
        </Col>
        <Col md={4} style={social}>
          <p style={reach} className="text-center">Reach Us At</p>
          <Row id="socialIcons"><img src={fb} /> <a href="https://twitter.com/estatechain_io"><img src={tw} /></a> <img src={ln} /></Row>
        </Col>
      </Row>
    </div>
  );
}

FooterSection.propTypes = {

};

export default FooterSection;
