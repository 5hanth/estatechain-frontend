import React from 'react';
import {Clearfix, Row,Col, Button,Glyphicon} from 'react-bootstrap';
import styles from './style.js';
import {browserHistory} from 'react-router';

function NewDashboardWalletSec(props){
    //var balance = '$100';
    function gotoWallet(e){
        browserHistory.push("/wallet");
    }
    var wallet = props.wallet[0].wallet;
    //console.log(wallet);
    return(
        <Row style={styles.mainContainer}>
            <Col sm={12} style = {styles.header}>
                <Row>
                    <Col sm={8} ><h1 style={styles.infoLeft}>My Wallet</h1></Col>
                    <Col sm={4}><h1 style={styles.infoRight}>{wallet.tokens_value}</h1></Col>
                </Row>
                <p style={styles.seperateLine1} ></p>
            </Col>

            <Col sm={12} style = {styles.infoWallet} id="tokensNav">
                <Row>
                    <Col sm={6} xs={5}><h1 style={styles.infoWalletLeft}>Available Fiat Currency</h1></Col>
                    <Col sm={3} xs={4}><h1 style={styles.infoWalletRight}>(Dollars)</h1></Col>
                    <Col sm={3} xs={3}><h1 style={styles.infoWalletRight}>{wallet.tokens_value}</h1></Col>
                    <Clearfix />
                    <Col sm={6} xs={5}><h1 style={styles.infoWalletLeft}>Available Bitcoins</h1></Col>
                    <Col sm={3} xs={4}><h1 style={styles.infoWalletRight}>{wallet.btc_raw} BTC</h1></Col>
                    <Col sm={3} xs={3}><h1 style={styles.infoWalletRight}>${wallet.btc_value}</h1></Col>
                    <Clearfix />
                    <Col sm={6} xs={5}><h1 style={styles.infoWalletLeft}>Available Ethereum</h1></Col>
                    <Col sm={3} xs={4}><h1 style={styles.infoWalletRight}>{wallet.eth_raw} ETH</h1></Col>
                    <Col sm={3} xs={3}><h1 style={styles.infoWalletRight}>${wallet.eth_value}</h1></Col>
                </Row>
                <p style={styles.seperateLine} ></p>
                <Row>
                    <Col sm={5} xs={5}><h1 style={styles.infoWalletLeft}>Available EST</h1></Col>
                    <Col sm={4} xs={4}><h1 style={styles.infoWalletRight}>{wallet.est_raw} EST</h1></Col>
                    <Col sm={3} xs={3}><h1 style={styles.infoWalletRight}>${wallet.est_value}</h1></Col>
                </Row>
                <Col sm={12} className="walletBtnDiv" style={styles.btnCont}>
                    <Button style={styles.btnStyle} onClick={gotoWallet}>Go to Wallet <Glyphicon glyph="play" style={{paddingLeft:'40px'}}/></Button>
                </Col>
            </Col>
        </Row>
    );
}
NewDashboardWalletSec.propTypes = {

};

export default NewDashboardWalletSec;
