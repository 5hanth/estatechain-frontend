import React from 'react';
import {Clearfix, Row,Col, Button,Image,Glyphicon,Table} from 'react-bootstrap';
import styles from './style.js';
import img from './bg.png';

function DashboardTrnxHis(props){
  var transactions = props.transactions[0];
  var tableData = transactions.map(function(input,index){
    return  <tr key={index} style={styles.tableData}>
                <td>{input.created_at}</td>
                <td>bought {input.tokens} EST1 @ ${input.token_unit}</td>
                <td>{input.btc_raw} BTC @ ${input.btc_unit} </td>
                <td>{input.token_value}</td>
            </tr>
  })
    return (
        <Row style={{boxShadow: '0px 3px 7px rgba(21, 22, 22, 0.35)'}}>
            <Col sm={12} style={styles.header} className="noMarginMobile">
                <h1 style={styles.headerData}>Transaction History</h1>
                <p style={styles.seperateLine} ></p>
            </Col>
            <Col sm={12} style={styles.dataSection} className="hide-scrollbar">
            <Table responsive>
                <thead>
                <tr>
                    <th style={styles.tablehead}>Date</th>
                    <th style={styles.tablehead}>Details</th>
                    <th style={styles.tablehead}>Currency Medium</th>
                    <th style={styles.tablehead}>Total Amount</th>
                </tr>
                </thead>
                <tbody>
                    {tableData}
                </tbody>
            </Table>
            </Col>
        </Row>
    )
}

DashboardTrnxHis.propTypes ={};

export default DashboardTrnxHis;
