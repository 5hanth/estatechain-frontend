import styled from 'styled-components';
import Bg from './bg.png';

const Styles = {
    header:{
        //background: 'linear-gradient(135deg, rgb(66, 92, 122) 0%, rgb(53, 80, 112) 25%, rgb(32,52, 79) 75%, rgb(18, 34, 55) 100%)',
        marginTop:'10px',
    },
    headerData:{
        fontFamily: 'Open Sans',
        fontSize: '24px',
        letterSpacing: '0.6px',
        textAlign: 'left',
        color:'#000',
        margin: '0px',
        padding: '10px 0',
    },
    tablehead:{
      fontSize:'18px',
      fontFamily:'Open Sans',
      fontWeight:'500',
      lineHeight:'20px',
      textAlign:'center'
  },
  tableData:{
      fontSize:'14px',
      fontFamily:'Open Sans',
      fontWeight:'400',
      lineHeight:'20px',
      textAlign:'center'   
  },
    noTrans: {
      textAlign: 'center',
      fontWeight: 'bold',
      margin: '30px'
    },
    dataSection:{
        backgroundColor:'#FFF',
        height: '46.4vh',
        overflowY: 'scroll'
    },
    trans: {
      margin: '10px 0'
    },
    tokens: {
      fontWeight: 'bold',
      margin: '0 10px'
    },
    token_value: {
      fontWeight: 'bold',
      margin: '0 10px'
    },
    seperateLine:{
        width: 'auto',
        height: '1px',
        backgroundColor: '#959595',
        border: 'solid 1px #959595',
    },
}

export default Styles;
