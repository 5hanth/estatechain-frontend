const detailCarouselStyles = {
  imgStyle: {
    margin: '0 10px',
    height: '280px'
  },
  wrapperStyle: {
    margin: '0 0vw',
    textAlign: 'center'
  },
  blur: {
    opacity: '0.6'
  }
}

export default detailCarouselStyles;
