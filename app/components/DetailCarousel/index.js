/**
*
* DetailCarousel
*
*/

import React from 'react';
// import styled from 'styled-components';
import {Carousel,Col, Row,Image} from 'react-bootstrap';
import img1 from './1.png';
import img2 from './2.png';
import img3 from './3.png';
import styles from './detailCarouselStyles'

function DetailCarousel(props) {
  // console.log("ok"+JSON.stringify(props.property));
  // console.log("ok"+JSON.stringify(props.property[0].imgs1[0]));
 // var img121 = props.property[0].imgs1.map((i) => <img src={i} style={styles.imgStyle}/>);

    return (
      <Carousel style={styles.wrapperStyle} controls={true}>
        <Carousel.Item>
            <Col sm={12}>
            <Row>
              <Col sm={4} className="carouselHideMob">
                <Image style={{...styles.blur, ...styles.imgStyle}} src={props.property[0].imgs1[0]} responsive/>
              </Col>
              <Col sm={4} xs={12} className="carouselShowMob">
                <Image style={styles.imgStyle} className="carouselShowMob" src={props.property[0].imgs1[1]} responsive/>
              </Col>
              <Col sm={4} className="carouselHideMob">
                <Image style={{...styles.blur, ...styles.imgStyle}} src={props.property[0].imgs1[2]} responsive/>
              </Col>
            </Row>
            </Col>
        </Carousel.Item>
        <Carousel.Item>
        <Col sm={12}>
          <Row>
            <Col sm={4} className="carouselHideMob">
              <img style={{...styles.blur, ...styles.imgStyle}} src={props.property[0].imgs2[0]}/>
            </Col>
            <Col sm={4} xs={12} className="carouselShowMob">
              <img style={styles.imgStyle} className="carouselShowMob" src={props.property[0].imgs2[1]}/>
            </Col>
            <Col sm={4} className="carouselHideMob">
              <img style={{...styles.blur, ...styles.imgStyle}} src={props.property[0].imgs2[2]}/>
            </Col>
          </Row>
        </Col>
        </Carousel.Item>
        <Carousel.Item>
        <Col sm={12} >
          <Row>
            <Col sm={4} className="carouselHideMob">
              <img style={{...styles.blur, ...styles.imgStyle}} src={props.property[0].imgs3[0]}/>
            </Col>  
            <Col sm={4} xs={12} className="carouselShowMob">
              <img style={styles.imgStyle} className="carouselShowMob" src={props.property[0].imgs3[1]}/>
            </Col>
            <Col sm={4} className="carouselHideMob">  
              <img style={{...styles.blur, ...styles.imgStyle}} src={props.property[0].imgs3[2]}/>
            </Col>
          </Row>
        </Col>
        </Carousel.Item>
      </Carousel>
    );
}

DetailCarousel.propTypes = {

};

export default DetailCarousel;
