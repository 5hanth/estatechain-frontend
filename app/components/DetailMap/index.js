/**
*
* DetailMap
*
*/

import React from 'react';
import { withScriptjs, GoogleMap, Marker, withGoogleMap } from "react-google-maps"

const DetailMetaMap = withScriptjs(withGoogleMap((props) =>
  <GoogleMap
    defaultZoom={8}
    defaultCenter={{ lat: props.property.location.lat, lng: props.property.location.lng }}
  >
  {/*console.log("ok"+JSON.stringify(props.property.location));*/}
    {props.isMarkerShown && <Marker position={{ lat: props.property.location.lat, lng: props.property.location.lng }} />}
  </GoogleMap>
))

function DetailMap(props) {
  return (
     <DetailMetaMap
     googleMapURL="https://maps.googleapis.com/maps/api/js?key=AIzaSyDvxEts0Xo4Qn5z0TMNh7dD_2Z9fCiGHjU&v=3.exp&libraries=geometry,drawing,places"
     loadingElement={<div style={{ height: `100%` }} />}
     containerElement={<div style={{ height: `250px` }} />}
     mapElement={<div style={{ height: `100%` }} />}
     property={props.property}
  />
  );
}

DetailMap.propTypes = {

};

export default DetailMap;
