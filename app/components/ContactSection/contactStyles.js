import styled from 'styled-components';
import Bg from './testbg.png';

export const CardContainer = styled.div`
  box-shadow: -1px 2px 27px -2px rgba(0,0,0,0.24);
`;

const contactStyles = {}

export const Wrapper = styled.div`
  overflow: hidden;
  height: 365px;
  margin: 100px 0px 20px;
  background: linear-gradient(135deg, rgb(66, 92, 122) 0%, rgb(53, 80, 112) 25%, rgb(32, 52, 79) 75%, rgb(18, 34, 55) 100%);
  @media only screen
    and (min-device-width : 320px)
    and (max-device-width : 1024px)
     {
      margin: 10px 0px 20px;
  }
`;

export const headerTitle = {
  fontFamily: 'Open Sans',
  fontWeight: 'bold',
  fontSize: '36px',
  color: '#fff',
  textAlign: 'center',
  lineHeight: '0.1',
  letterSpacing: '-0.6px',
  margin: '50px 0px'
}

export const divider = {
}

export const colStyle = {
  // height: '175px'
}

export const rowStyle = {
  background: '#fff',
  margin: '0 15%'
}

export const fixMail = {
  marginTop: '15px'
}

export const cardItem1 = {
  // boxShadow: '-1px 2px 27px -2px rgba(0,0,0,0.24)',
  background: '#fff',
  padding: '20px',
  textAlign: 'center',
  fontFamily: 'Roboto',
  lineHeight: '0.1',
  letterSpacing: '-0.6px'
}

export const cardItem2 = {
  // boxShadow: '-1px 2px 27px -2px rgba(0,0,0,0.24)',
  background: '#fff',
  padding: '20px',
  textAlign: 'center',
  fontFamily: 'Roboto',
  lineHeight: '0.1',
  letterSpacing: '-0.6px',
}

export default contactStyles;
