/**
*
* ContactSection
*
*/

import React from 'react';
// import styled from 'styled-components';
import { Wrapper, headerTitle, cardItem1, cardItem2, divider, colStyle, rowStyle, fixMail } from './contactStyles';
import { Row, Col, Button, Media } from 'react-bootstrap';
import mobile from './mobile.png';
import mail from './mail.png';
import address from './address.png';

function ContactSection() {
  return (
    <Wrapper id="contactWrapper">
      <h1 style={headerTitle}>Contact Us</h1>
      <Row id="contactSection" style={rowStyle}>
        <Col md={4} style={cardItem1}>
         <Row>
          <Col style={colStyle}>
          <img src={mobile} />
          <p>Call Us at</p><p><b>+44-7448355019</b></p></Col>
         </Row>
        </Col>
        <Col md={4} style={cardItem1}>
         <Row>
          <Col style={colStyle}>
          <img src={mail} />
          <p style={fixMail}>Mail Us at </p><p><b>info@estatchainplatform.com</b></p></Col>
         </Row>
        </Col>
        <Col md={4} style={cardItem2}>
         <Row>
          <Col style={colStyle}>
          <img src={address} />
          <p>Address </p><p><b>41 Corsham Street, London N1 6DR</b></p></Col>
         </Row>
        </Col>

      </Row>
    </Wrapper>
  );
}

ContactSection.propTypes = {

};

export default ContactSection;
