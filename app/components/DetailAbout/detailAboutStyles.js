const detailInfoStyles = {
  container: {
    margin: '5vw 10vw'
  },
  title: {
    fontFamily: 'Open Sans',
    fontSize: '20.7px',
    fontWeight: '600',
    textAlign: 'left',
    color: '#464646'
  },
  aboutText: {
    color: '#656669',
    overflowY: 'scroll',
    height: '150px',
    paddingRight: '15px'
  },
  cardStyle: {
    textAlign: 'center',
    margin: '10px',
    height:'147px',
    background: '#fff',
    boxShadow: '0px 3px 5px -1px rgba(138,138,138,1)',
  },
  header: {
    fontFamily: 'Open Sans',
    fontSize: '18.3px',
    textAlign: "center",
    color: '#464646'
  },
  arrowDown: {
    margin: '20px'
  },
  chartstyle:{
    marginTop: 'inherit'
  },
  chartContainer: {
    background: '#fff',
    marginTop: '10px',
    marginLeft:'14px',
    height:'320px'
  },
  returnsContainer: {
    background: '#fff',
    marginTop: '10px'
  },
  returnsItem: {
    margin: '0'
  },
  returnsHeader: {
    fontFamily: 'Open Sans',
    fontSize: '18.3px',
    color: '#464646',
    margin: '30px 0'
  },
  bold: {
    fontWeight: '600',
  },
  rank: {
    color: '#506ac0',
    fontSize: '31px',
    letterSpacing: '2px',
    lineHeight: '20px'
  },
  hr: {
    borderBottom: '1px solid #464646',
    margin: '0'
  },
  right: {
    textAlign: 'right'
  },
  locationContainer: {
  },
  detailsWrapper: {
    paddingBottom: '30px'
  }

}

export default detailInfoStyles;
