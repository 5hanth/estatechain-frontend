/**
*
* DetailAbout
*
*/

import React from 'react';
import {Col,Row,Image, Clearfix} from 'react-bootstrap';
//import videoImg from './video.png';
import arrowDownImg from './arrowDown.png';
import {Line } from 'react-chartjs-2';
import chartImg from './chart.png';
import styles from './detailAboutStyles';
import FinancialsTabs  from 'components/FinancialsTabs';
import DetailMap  from 'components/DetailMap';

function DetailAbout(props) {
  //console.log("ok"+JSON.stringify(props.property));
  const data = {
    labels: ['January', 'February', 'March', 'April', 'May', 'June', 'July'],
    datasets: [
      {
        label: 'House Price Index',
        fill: false,
        lineTension: 0.1,
        backgroundColor: '#122237',
        borderColor: '#122237',
        borderCapStyle: 'butt',
        borderDash: [],
        borderDashOffset: 0.0,
        borderJoinStyle: 'miter',
        pointBorderColor: '#ffffff',
        pointBackgroundColor: '#fff',
        pointBorderWidth: 1,
        pointHoverRadius: 5,
        pointHoverBackgroundColor: '#122237',
        pointHoverBorderColor: 'rgba(220,220,220,1)',
        pointHoverBorderWidth: 2,
        pointRadius: 1,
        pointHitRadius: 10,
        data: [30, 42, 54, 46, 56, 65, 79]
      }
    ]
  };
  var property = props.property[0];
  return (
    <Col style={styles.detailsWrapper} className="detailAboutWrapper">
      <Row style={styles.container} className="detailAboutContainer">
        <Col sm={12} xs={12} className="propDetail">
          <h1 style={styles.title}>About Property</h1>
          <p style={styles.aboutText} id="styled-scroll">{property.about}</p>
        </Col>
        {/* <Col smOffset={1} sm={5}>
          <Image src={property.video} responsive/>
        </Col> */}
      </Row>

      <Row style={styles.container} className="detailAboutContainer">
        <Col sm={2} xs={12}>
          <h1 style={styles.title}>About Property</h1>
          <Row className="reportDiv">
            <Col sm={12} xs={12} style={styles.cardStyle}>
              <h1 style={styles.header}>Due Diligence Report</h1>
              <img style={styles.arrowDown} src={arrowDownImg} />
            </Col>
            <Col sm={12} xs={12} style={styles.cardStyle}>
              <h1 style={styles.header}>Smart Contract</h1>
              <img style={styles.arrowDown} src={arrowDownImg} />
            </Col>
            {/* <Col sm={12} style={styles.cardStyle}>
              <h1 style={styles.header}>Land Registry</h1>
              <img style={styles.arrowDown} src={arrowDownImg} />
            </Col>
            <Col sm={12} style={styles.cardStyle}>
              <h1 style={styles.header}>Incorporation Certificate</h1>
              <img style={styles.arrowDown} src={arrowDownImg} />
            </Col> */}
          </Row>
        </Col>
        <Col sm={5} xs={12} className="chartDiv">
          <h1 style={styles.title} id="houseIndex">House Price Index</h1>

          <Col sm={12} xs={12} style={styles.chartContainer} className="chartData">
            {/* <Image style={styles.chartstyle} src={chartImg} responsive/> */}
            <div>
              <Line data={data} />
            </div>
          </Col>
        </Col>
        <Col sm={5} xs={12} className="estimateDiv">
          <h1 style={styles.title}>Estimated Returns</h1>
          <Col sm={12} xs={12} style={{...styles.returnsContainer}} className="estimateContainer">
            <Row style={{...styles.returnsItem,...styles.returnsHeader}} className="data">
              <Col sm={8} xs={8}>Net Rental divident</Col><Col sm={4} xs={4} style={styles.right}>{property.net_rental_dividend}</Col>
            </Row>
            <Row style={{...styles.returnsItem,...styles.returnsHeader}}>
              <Col sm={8} xs={8}>Estimated Value Growth</Col><Col sm={4} xs={4} style={styles.right}>{property.estimated_value_growth}</Col>
            </Row>
            <Row style={styles.hr} />
            <Row style={{...styles.returnsItem,...styles.returnsHeader}}>
              <Col sm={8} xs={8}>Total Estimated Growth</Col><Col sm={4} xs={4} style={styles.right}>{property.total_estimated_growth}</Col>
            </Row>
            <Row style={styles.hr} />
            <Row style={{...styles.returnsItem,...styles.returnsHeader}}>
              <Col sm={8} xs={8} style={styles.bold}>Property Trending Rank</Col><Col sm={4} xs={4} style={styles.right}><span style={styles.rank}>{property.rank}</span>/{property.rank_total}</Col>
            </Row>
          </Col>
        </Col>
      </Row>

      <Row style={styles.container} className="detailAboutContainer">
        {/* <Col sm={6}>
          <h1 style={styles.title}>Estimated Returns</h1>
          <Col sm={12} style={{...styles.returnsContainer}}>
            <Row style={{...styles.returnsItem,...styles.returnsHeader}}>
              <Col sm={10}>Net Rental divident</Col><Col sm={2} style={styles.right}>{property.net_rental_dividend}</Col>
            </Row>
            <Row style={{...styles.returnsItem,...styles.returnsHeader}}>
              <Col sm={10}>Estimated Value Growth</Col><Col sm={2} style={styles.right}>{property.estimated_value_growth}</Col>
            </Row>
            <Row style={styles.hr} />
            <Row style={{...styles.returnsItem,...styles.returnsHeader}}>
              <Col sm={10}>Total Estimated Growth</Col><Col sm={2} style={styles.right}>{property.total_estimated_growth}</Col>
            </Row>
            <Row style={styles.hr} />
            <Row style={{...styles.returnsItem,...styles.returnsHeader}}>
              <Col sm={10} style={styles.bold}>Property Trending Rank</Col><Col sm={2}><span style={styles.rank}>{property.rank}</span>/{property.rank_total}</Col>
            </Row>
          </Col>
        </Col> */}
        {/* <Col sm={12}>
          <h1 style={styles.title}>Financials</h1>
          <FinancialsTabs />
        </Col> */}
      </Row>

      <Row style={{...styles.container,...styles.locationContainer}} className="detailAboutContainer">
        <h1 style={styles.title} className="mapHeading">Location of Property</h1>
        <DetailMap property={property} />
      </Row>
    </Col>
  );
}

DetailAbout.propTypes = {

};

export default DetailAbout;
