/**
 *
 * BuyTokenDetail
 *
 */
import React from 'react';
import {browserHistory} from 'react-router';
import { Row, Col, Clearfix,Button,FormControl,Form } from 'react-bootstrap';
import {styles} from './style.js';
import api from '../../utils/api';

class BuyTokenDetail extends React.Component{
    constructor(props){
        super(props);
        this.state = {id:props.property.id ,token: 0, offer: '', trans: 0, price: 0, property: props.property, wallet: props.wallet};
        this.handleBuy = this.handleBuy.bind(this);
        this.handleChangeToken = this.handleChangeToken.bind(this);
        this.handleChangeOffer = this.handleChangeOffer.bind(this);
    }

    handleBuy(e){
        api.post('buy', {"property":{
            "id": this.state.id,
            "tokens":this.state.token
        }})
        .then((resp) =>{
        if(resp.data.error === false){
            browserHistory.push("/dashboard");
        }
        else{
            //show error message
        }
        })
    }
    handleChangeToken(e){
        this.setState({token: (Number(e.target.value) / 4.31).toFixed(3)});
        this.setState({price: Number(e.target.value)});
        this.setState({trans: Number(e.target.value) * 2/100});
    }
    handleChangeOffer(e){
        this.setState({offer: e.target.value});
    }
    render(){
        return(
                <Col sm={12} xs={12} className="">
                    <Col sm={12} xs={12} className="buyDetailContainer" style={styles.container}>
                    <Form className="buyTokenForm row">
                        <Col smOffset={2} sm={4} xs={8} className="formData" style={styles.leftData}>• How much do you want to invest? (in USD)</Col>
                        <Col sm={4} xs={4} style={styles.rightData} className="formDataDiv">
                        <FormControl type="text" className="formDataInput" placeholder="$"
                        value={this.state.price} onChange={(event) => this.handleChangeToken(event)} style={styles.textform}/></Col>
                        <Clearfix />
                        <Col smOffset={2} sm={4} xs={8} style={styles.leftData} className="formData">• Number of tokens </Col>
                        <Col sm={4} xs={4} style={styles.rightData} className="formDataDiv">{this.state.token}</Col>
                        <Clearfix />
                        <Col smOffset={2} sm={4} xs={8} style={styles.leftData} className="formData">• Token Price</Col>
                        <Col sm={4} xs={4} style={styles.rightData} className="formDataDiv">{this.state.property.token_price}</Col>
                        <Clearfix />
                        <Col smOffset={2} sm={4} xs={8} style={styles.leftData} className="formData">• Transaction charges</Col>
                        <Col sm={4} xs={4} style={styles.rightData} className="formDataDiv">${this.state.trans}</Col>
                        <Clearfix />
                        <Col smOffset={2} sm={4} xs={8} style={styles.leftData} className="formData">• Any offer code</Col>
                        <Col sm={4} xs={4} style={styles.rightData} className="formDataDiv"><FormControl type="text" placeholder=""
                        value={this.state.offer} className="formDataInput" onChange={(event) => this.handleChangeOffer(event)} style={styles.textform}/></Col>
                        <Clearfix />
                        <Col smOffset={2} sm={4} xs={8} style={styles.leftData} className="formData">• Total Cost</Col>
                        <Col sm={4} xs={4} style={styles.rightData} className="formDataDiv">${this.state.price + this.state.trans}</Col>
                        <Clearfix />
                        <Col smOffset={2} sm={4} xs={8} style={styles.leftData} className="formData">• Available Money</Col>
                        <Col sm={4} xs={4} style={styles.rightData} className="formDataDiv">{this.state.wallet.tokens_value}</Col>
                        <Clearfix />
                        <Col smOffset={2} sm={4} xs={8} style={styles.leftData} className="formData">• Total payable money</Col>
                        <Col sm={4} xs={4} style={styles.rightData} className="formDataDiv">${this.state.price + this.state.trans}</Col>
                        <Clearfix />
                        <Col smOffset={4} sm={4}><Button bsSize="large" block style={styles.btn} onClick={this.handleBuy}>Buy tokens</Button></Col>
                    </Form>
                    </Col>
                </Col>
        )
    }
}

export default BuyTokenDetail;
