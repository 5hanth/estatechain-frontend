import styled from 'styled-components';
export const styles = {
    container :{
      paddingLeft:'31px',
      paddingRight:'31px',
      //marginTop:'55px',
      backgroundColor:'#FFF',
      marginBottom:'20px',
    },
    headerTitleContainer : {
      display: "flex",
      flexDirection: "Row",
      justifyContent: "center",
      paddingTop: '10px',
      height:'48px',
    },
    insideData:{
      margin :'14px 46px 14px 46px',
    },
    leftData:{
      marginTop:"30px",
      textAlign:"left",
      height: '18px',
      fontFamily: 'Open Sans',
      fontSize: '18px',
      color: '#58595b',
    },
    rightData:{
      marginTop:"30px",
      textAlign:"center",
      height: '18px',
      fontFamily: 'Open Sans',
      fontSize: '18px',
      color: '#58595b',
    },
    textform:{
      marginLeft:'97px',
      width:'auto',
      padding:'0px 5px',
      fontSize:'18px',
      textAlign:'center',
    },
    btn:{
      background:'#465f7c',
      fontSize:"24px",
      margin:'35px 0px',
      fontFamily:'SegoeUI',
    }
  }
