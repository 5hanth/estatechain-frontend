import styled from 'styled-components';
import Bg from './seeourprop.jpg';
import MBg from './seepropmob.png';

const propertyStyles = {
}

export const Wrapper = styled.div`
  width: 100%;
  overflow: 'hidden';
  background: url(${Bg}) no-repeat center center;
  height: 800px;
  margin: 100px 0px 0px 0px;
  background-size: cover;
  @media only screen
    and (min-device-width : 320px) 
    and (max-device-width : 1024px)
     {
      background: url(${MBg}) no-repeat center center;
      background-size: 100vw 50vh;
      background-position-y: 96px;
      margin: 35px 0px 0px 0px;
  }
`;

export const headerTitle = {
  fontFamily: 'Open Sans',
  margin: '30px',
  textAlign: 'center',
  fontWeight: 'bold',
  fontSize: '36px',
  color: '#fff'
}

export const propText = {
  textAlign: 'center',
  color: '#fff',
  fontSize: '18px'
}

export const propButton = {
  background: 'transparent',
  color: 'white',
  borderRadius: '20px',
  width: '200px',
  margin: '10px 0px',
  border: '2px solid'
}

export const propRow = {
  margin: '150px 0px'
}


export default propertyStyles;
