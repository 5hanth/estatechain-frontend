/**
*
* PropertiesSection
*
*/

import React from 'react';
// import styled from 'styled-components';
import { Grid, Row, Col, Button, Thumbnail } from 'react-bootstrap';
import {Wrapper, headerTitle, propText, propButton, propRow} from './propertyStyles';



function PropertiesSection() {
  return (
    <Wrapper>
        <Grid>
          <Row style={headerTitle} className="landingTitle" id="seeTitle">See our properties</Row>
          <Row style={propRow} id="propSecRow">
              <Col xs={12} md={4} mdOffset={8} style={propText}>
                  <p>We have got the best properties across the globe with highest Return of investment value and good amount of rental income dividend.</p>
                  <p>Because of blockchain platform you get chance to invest in different properties all around the world and sell your tokens anytime, anywhere without any restriction.</p>
                  <Button style={propButton} href="/properties">View All Properties</Button>
                  <Button style={propButton}>Meet Our Partners</Button>
              </Col>
          </Row>
      </Grid>
    </Wrapper>
    );
}

PropertiesSection.propTypes = {

};

export default PropertiesSection;
