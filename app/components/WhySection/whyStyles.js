import styled from 'styled-components';
import Bg from './bggra.jpg';

const whyStyles = {
}

export const Wrapper = styled.div`
  width: 100%;
  overflow: 'hidden';
  background: '#fff'
`;
// background: url(${Bg}) no-repeat;
// background-position-y: 690px;
// background-size: 100vw 650px;


export const whyTitle = {
  margin: '30px',
  textAlign: 'center',
  fontWeight: 'bold',
  fontSize: '36px',
  fontFamily: 'Open Sans',
  color: '#414042'
}

export const whyImg = {
  width: '300px',
  height: '300px',
  padding: '30px'
}

export const whatImg = {
  width: '20vw'
}

export const headerTitle = {
  fontFamily: 'Open Sans',
  fontWeight: 'bold',
  fontSize: '21px',
  color: '#808080',
  textAlign: 'center'
}

export const howTitle = {
  fontFamily: 'Open Sans',
  margin: '60px 30px 30px',
  textAlign: 'center',
  fontWeight: 'bold',
  fontSize: '36px',
  color: 'rgb(66, 92, 122)'
}

export const desc = {
  fontSize: '18px',
  color: '#808080',
  fontFamily: 'Open Sans',
  textAlign: 'center'
}

export const howDesc = {
  fontSize: '18px',
  color: 'rgb(66, 92, 122)',
  fontFamily: 'Open Sans',
  textAlign: 'center'
}

export const whatDesc = {
  fontSize: '18px',
  color: '#58595b',
  fontFamily: 'Open Sans',
  textAlign: 'center'
}

export const whatRow = {
}

export const cardItem = {
  boxShadow: ' -1px 2px 27px -2px rgba(0,0,0,0.24)'
}

export const cardHowItem = {
  boxShadow: ' -1px 2px 27px -2px rgba(0,0,0,0.24)',
  maxWidth: '300px',
  margin: 'auto'
}

export const cardWhatItem = {
  boxShadow: ' -1px 2px 27px -2px rgba(0,0,0,0.24)',
  maxWidth: '350px',
  margin: 'auto'
}

export const plusStyle = {
  padding: '100px 0 0 0px'
}

export const plusImg = {
  width: '80px'
}

export const howImg = {
  width: '100%',
}

export const howItem = {
  textAlign: 'center',
  background: 'white',
  boxShadow: 'rgba(0, 0, 0, 0.24) -1px 2px 27px -2px'
}

export const center = {
  textAlign: 'center',
}

export const cardText = {
  height: '230px',
  padding: '0 15px'
}

export const percent = {
  fontSize: '21px'
}

export const howCardText = {
  height: '120px',
  margin: '25px 0'
}

export const whatCardText = {
  padding: '0 0px',
  margin: '5px 0px'
}

export default whyStyles;
