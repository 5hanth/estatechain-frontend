/**
*
* WhySection
*
*/

import React from 'react';
// import styled from 'styled-components';
import { Grid, Row, Col, Button, Thumbnail } from 'react-bootstrap';
import {whyTitle, whyImg, center, headerTitle, percent, plusStyle, plusImg, cardItem, cardText, desc, howItem, howImg, Wrapper, howDesc, howTitle, howCardText, whatImg, whatDesc, whatRow, cardHowItem, cardWhatItem, whatCardText, propStyle} from './whyStyles';
import BorderlessPng from './why2.png';
import ReturnsPng from './why1.png';
import LiquidityPng from './why3.png';
import how1 from './1how.png';
import how2 from './2how.png';
import how3 from './3how.png';
import how4 from './4how.png';
import what1 from './what1.png';
import what2 from './what2.png';
import plus from './plus.png';


function WhySection() {
  return (
    <Wrapper>
      <Row style={whyTitle} className="landingTitle">Why EstateChain ?</Row>
      <Grid>
    <Row>
    <Col xs={12} md={4} >
      <Thumbnail src={BorderlessPng} style={cardItem} alt="Borderless">
      <div style={cardText}>
        <h3 style={headerTitle}>CROSS BORDER OWNERSHIP</h3>
        <p style={desc}>With Ethereum, securely own properties and universally trade tokens across marketplaces.</p>
      </div>
      </Thumbnail>
    </Col>
    <Col xs={12} md={4}>
      <Thumbnail src={ReturnsPng} style={cardItem} alt="Returns">
        <div style={cardText}>
          <h3 style={headerTitle}>RETURNS ON YOUR INVESTMENT</h3>
          <p style={desc}>With global Real-Estate Partnerships, own a fraction of finest properties and achieve Y-O-Y ROI on your investments.</p>
        </div>
      </Thumbnail>
    </Col>
    <Col xs={12} md={4}>
      <Thumbnail src={LiquidityPng} style={cardItem} alt="Liquidity">
      <div style={cardText}>
        <h3 style={headerTitle}>DEDICATED LIQUIDITY SOLUTION</h3>
        <p style={desc}>Allows investors to investors to secure higher liquidity for real estate investment by interacting with Bancor smart contract.</p>
      </div>
      </Thumbnail>
    </Col>
    </Row>

    <Row style={howTitle} className="landingTitle">How it works?</Row>
    <Row>
    <Col xs={12} md={3} className="howCol">
      <Col xs={12}  style={howItem}>
        <img src={how1} style={howImg}  />
        <div style={howCardText}>
          <p style={howDesc}>Property gets listed on platform after Due-diligence</p>
        </div>
      </Col>
    </Col>
    <Col xs={12} md={3} className="howCol">
      <Col xs={12}  style={howItem}>
        <img src={how2} style={howImg}  />
        <div style={howCardText}>
          <p style={howDesc}>Buyers from around the world crowdfund the property, subject to reaching the minimum amount</p>
        </div>
      </Col>
    </Col>
    <Col xs={12} md={3} className="howCol">
      <Col xs={12}  style={howItem}>
        <img src={how3} style={howImg}  />
        <div style={howCardText}>
          <p style={howDesc}>Property tokens representing partial ownership get created in a legally compliant way</p>
        </div>
      </Col>
    </Col>
    <Col xs={12} md={3} className="howCol">
      <Col xs={12}  style={howItem}>
        <img src={how4} style={howImg}  />
        <div style={howCardText}>
          <p style={howDesc}>Property tokens are immediately exchangeable with others on a whitelist, creating instant liquidity</p>
        </div>
      </Col>
    </Col>
    </Row>

    <Row style={howTitle} className="landingTitle">What would you earn?</Row>
    <Row id="whatSection">
    <Col xs={12} md={5} mdOffset={1} style={center}>
      <Thumbnail src={what1} style={cardWhatItem} >
      <div style={whatCardText}>
        <p style={whatDesc}><b>Rental Income dividend</b></p>
      </div>
      </Thumbnail>
    </Col>
    <Col xs={12} md={1} style={plusStyle} id="plusCol">
        <img src={plus}  style={plusImg}/>
    </Col>
    <Col xs={12} md={5} style={center}>
      <Thumbnail src={what2} style={cardWhatItem} >
      <div style={whatCardText}>
        <p style={whatDesc}><b>Capital Growth</b></p>
      </div>
      </Thumbnail>
    </Col>
    </Row>

  </Grid>
    </Wrapper>
  );
}

WhySection.propTypes = {

};

export default WhySection;
