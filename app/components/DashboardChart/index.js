import React, { Component } from 'react';
import { Pie,Area,Bar,Line,PolarArea } from 'react-chartjs-2';
import api from '../../utils/api';
class PieChart extends Component {
    constructor(props) {
        super(props);
        //console.log(props.chart[0].chart);
        this.state = { data: [],labels:[],colors:[] }
        //
    }
    componentDidMount(){
        this.setState({
            data:this.props.chart[0].chart.data,
            labels:this.props.chart[0].chart.labels,
            colors:this.props.chart[0].chart.colors
        })
    }
    render() {
    const data = {
        labels:this.state.labels,
        datasets: [{
            data: this.state.data,
            backgroundColor: this.state.colors,
            hoverBackgroundColor: this.state.colors
        }]
    };
    return (
      <div className='PieChart'>
        <Pie data={data} legend={{display: false}} height='250' />
      </div>
    );
  }
}
export default PieChart;
