/**
*
* EsnavbarLoggedIn
*
*/

import React from 'react';
// import styled from 'styled-components';

import { Navbar,Image,Clearfix, NavItem, MenuItem, NavDropdown, Nav } from 'react-bootstrap';
import accountIco from './accountIco.png';
import Esnavbar from 'components/Esnavbar';
import logo from './logo.png';
import navbarStyles from './navbarStyles';
import { browserHistory } from 'react-router';

function EsnavbarLoggedIn() {
  function handleLogOut(){
    localStorage.removeItem('token');
    browserHistory.push('/auth');
  }
  if(localStorage.token===null || localStorage.token===undefined){
    return(
      <Esnavbar />
    );
  }
  return (
    <Navbar inverse collapseOnSelect style={navbarStyles.navStyle} className="mainNav">
    <Navbar.Header>
      <Navbar.Brand>
      <a href="https://estatechain.io"><Image src={logo} responsive className="navImg"/></a>
      </Navbar.Brand>
      <Navbar.Toggle className="navToggle"/>
    </Navbar.Header>
    <Navbar.Collapse>
      <Nav pullRight style={navbarStyles.rightMenu}>
        <MenuItem eventKey={1} href="/">Home</MenuItem>
        <MenuItem eventKey={2} href="/properties">Properties</MenuItem>
        <MenuItem eventKey={3} href="/comingsoon">Token Sale</MenuItem>
        <MenuItem eventKey={4} href="/wallet">Wallet</MenuItem>
        {/* <NavDropdown eventKey={3} title="Dropdown" id="basic-nav-dropdown">
          <MenuItem eventKey={2.1}>Action</MenuItem>
          <MenuItem eventKey={2.2}>Another action</MenuItem>
          <MenuItem eventKey={2.3}>Something else here</MenuItem>
          <MenuItem divider />
          <MenuItem eventKey={2.3}>Separated link</MenuItem>
        </NavDropdown>
        <NavDropdown eventKey={3} title="About Us" id="basic-nav-dropdown">
          <MenuItem eventKey={3.1}>Action</MenuItem>
          <MenuItem eventKey={3.2}>Another action</MenuItem>
          <MenuItem eventKey={3.3}>Something else here</MenuItem>
          <MenuItem divider />
          <MenuItem eventKey={3.4}>Separated link</MenuItem>
        </NavDropdown> */}
        <MenuItem eventKey={5} href="/dashboard">My Dashboard
           </MenuItem>
        <NavDropdown eventKey={6} title="My Account" id="basic-nav-dropdown" >
          {/*  */}
          <MenuItem eventKey={6.1} style={navbarStyles.drop} style={navbarStyles.drop} onClick={handleLogOut}>Log Out</MenuItem>
        </NavDropdown><img src={accountIco} style={navbarStyles.accountIcon}/>
      </Nav>
    </Navbar.Collapse>
  </Navbar>
  );
}

EsnavbarLoggedIn.propTypes = {

};

export default EsnavbarLoggedIn;
