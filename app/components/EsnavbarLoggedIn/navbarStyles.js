const navbarStyles = {
  navStyle:{
    border: 'none',
    // background:' -moz-linear-gradient(135deg, rgba(72,177,199,1) 0%, rgba(56,139,194,1) 25%, rgba(82,105,189,1) 75%, rgba(108,76,183,1) 100%)',
    // background:' -webkit-gradient(linear, left bottom, right top, color-stop(0%, rgba(72,177,199,1)), color-stop(25%, rgba(56,139,194,1)), color-stop(75%, rgba(82,105,189,1)), color-stop(100%, rgba(108,76,183,1)))',
    // background:' -webkit-linear-gradient(135deg, rgba(72,177,199,1) 0%, rgba(56,139,194,1) 25%, rgba(82,105,189,1) 75%, rgba(108,76,183,1) 100%)',
    // background:' -o-linear-gradient(135deg, rgba(72,177,199,1) 0%, rgba(56,139,194,1) 25%, rgba(82,105,189,1) 75%, rgba(108,76,183,1) 100%)',
    // background:' -ms-linear-gradient(135deg, rgba(72,177,199,1) 0%, rgba(56,139,194,1) 25%, rgba(82,105,189,1) 75%, rgba(108,76,183,1) 100%)',
    background:'linear-gradient(135deg, rgb(66, 92, 122) 0%, rgb(53, 80, 112) 25%, rgb(32,52, 79) 75%,  rgb(18, 34, 55) 100%)',
    height: '98px',
    width: '100%',
    top:'0px',
    zIndex:'999',
    position:'fixed'
  },
  drop:{
    background:'#FFFFFF',
    color:'white',
  },
  accountIcon: {
    width: '30px',
    margin: '3px 0px 0px -30px'
  },
  rightMenu:{
    marginTop:'30px',
    //marginRight:'145px'
  }
}

export default navbarStyles;
