/**
*
* FlipClock
*
*/

import React,{ Component } from "react";
import { Row, Col } from 'react-bootstrap';
import style from './style';
import Countdown, {zeroPad} from "react-countdown-now";
class FlipClock extends Component{
	render(){
		// Random component
		const Completionist = () => <span>You are good to go!</span>;

		// Renderer callback with condition
		const renderer = ({ days, hours, minutes, seconds, completed }) => {
			//var hr = zeroPad(hours, [length = 2])
		if (completed) {
			// Render a complete state
			return <Completionist />;
		} else {
			// Render a countdown
			return (
				<Col>
					<Col xs={12} sm={12} style={style.timediv}>
						<Col sm={1} xs={2} style={style.showTime}>{zeroPad(days)}</Col>
						<Col sm={1} xs={1} style={style.timecolon}>:</Col>
						<Col sm={1} xs={2} style={style.showTime}>{zeroPad(hours)}</Col>
						<Col sm={1} xs={1} style={style.timecolon}>:</Col>
						<Col sm={1} xs={2} style={style.showTime}>{zeroPad(minutes)}</Col>
						<Col sm={1} xs={1} style={style.timecolon}>:</Col>
						<Col sm={1} xs={2} style={style.showTime}>{zeroPad(seconds)}</Col>
					</Col>
					<Col sm={12} xs={12}>
						<Col sm={1} xs={2} style={style.timeHeading}>Days</Col>
						<Col sm={1} xs={1} style={style.timecolon}></Col>
						<Col sm={1} xs={2} style={style.timeHeading}>Hours</Col>
						<Col sm={1} xs={1} style={style.timecolon}></Col>
						<Col sm={1} xs={2} style={style.timeHeading}>Minutes</Col>
						<Col sm={1} xs={1} style={style.timecolon}></Col>
						<Col sm={1} xs={2} style={style.timeHeading}>Seconds</Col>
					</Col>
				</Col>
			);
		}
		};
		return (
			 <Countdown date={'2017-11-9'}  renderer={renderer} />
			//  <Col sm={12} style={style.timediv}>
			//  	<Countdown date={'2017-11-9'}>
			//  		<Completionist />
			// 	</Countdown>
			// 	{/* <Col sm={1} style={style.showTime}>{days}</Col>
			// 	<span style={style.showTime}>:</span>
			// 	<Col sm={1} style={style.showTime}>{hours}</Col>:<span>{minutes}</span>:<span>{seconds}</span> */}
		 	//  </Col>
			
		);
	}
}

export default FlipClock;

