import styled from 'styled-components';

const style ={
    timediv:{
        marginTop:'50%',
    },
    showTime:{
        fontSize: '20px',
        border: '2px solid #FFFFFF',
        paddingLeft: '3px',
        width:'34px',
        height:'34px',
        background:'#FFFFFF',

    },
    timecolon:{
        width: '0px',
        fontSize: '20px',
        paddingRight: '10px',
        paddingLeft: '4px',
        color:'#FFFFFF',
        fontWeight:'800'
    },
    timeHeading:{
        width: '34px',
        height: '34px',
        fontSize: '10px',
        paddingLeft: '3px',
        marginTop: '5px',
        color:'#FFF',
    }
}
export default style;



