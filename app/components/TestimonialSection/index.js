/**
*
* TestimonialSection
*
*/

import React from 'react';
// import styled from 'styled-components';
import { headerTitle, Wrapper, testText, TestContainer, testWrapper } from './testimonialStyles';
import { Row, Col, Button, Media } from 'react-bootstrap';


function TestimonialSection() {
  return (
    <Wrapper>
      <h1 style={headerTitle} className="landingTitle">Testimonials</h1>
      <div style={testWrapper}>
        <Col md={10} mdOffset={1}><TestContainer><p style={testText}>&#34;Crowdfunding Investment Market to Hit $93 Billion by 2025&#34; <br/> <br/> <b>— Richard Swart PhD</b>, Director of Research, UC Berkeley, Worldbank</p></TestContainer></Col>
      </div>
    </Wrapper>
  );
}

TestimonialSection.propTypes = {

};

export default TestimonialSection;
