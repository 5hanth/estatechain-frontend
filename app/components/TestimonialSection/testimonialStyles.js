import styled from 'styled-components';
import Bg from './testbg.png';

export const TestContainer = styled.div`
  overflow: 'hidden';
  max-height: 277px;
  background: linear-gradient(135deg, rgb(66, 92, 122) 0%, rgb(53, 80, 112) 25%, rgb(32, 52, 79) 75%, rgb(18, 34, 55) 100%);
  background-size: cover;
  box-shadow: -1px 2px 27px -2px rgba(0,0,0,0.24);
`;

const testimonialsStyles = {
}

export const Wrapper = styled.div`
  overflow: hidden;
  margin: 100px 0px;
  padding: 10px 0;
  @media only screen
    and (min-device-width : 320px) 
    and (max-device-width : 1024px)
     {
      margin: 20px 0px;
  }
`;

export const headerTitle = {
  fontFamily: 'Open Sans',
  fontWeight: 'bold',
  fontSize: '36px',
  color: '#4a4a4a',
  textAlign: 'center',
  lineHeight: '0.1',
  letterSpacing: '-0.6px',
  margin: '50px 0px'
}

export const testText = {
  fontSize: '18px',
  color: '#ffffff',
  textAlign: 'center',
  overflow: 'hidden',
  background: `url(${Bg}) no-repeat !important`,
  padding: '10%'
}

export const testWrapper = {
  padding: '100px'
}

export default testimonialsStyles;
