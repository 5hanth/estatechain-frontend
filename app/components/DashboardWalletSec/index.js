import React from 'react';
import {Clearfix, Row,Col, Button} from 'react-bootstrap';
import styles from './style.js';
import {browserHistory} from 'react-router';

function DashboardWalletSec(props){
    //var balance = '$100';
    function gotoWallet(e){
        browserHistory.push("/wallet");
    }
    var wallet = props.wallet[0].wallet;
    //console.log(wallet);
    return(
        <Row className="noMarginMobile">
            <Col sm={12} style={styles.infoContainer} className="noMarginMobile">
                <h1 style={styles.infoHeader}>My Wallet</h1>
                <h2 style={styles.infoBalance}>{wallet.tokens_value}</h2>
                <Col sm={12} className="walletMain" style={styles.btnContainer}>
                    <Row>
                        <Col sm={6} smOffset={3} className="walletBtnDiv" style={styles.btnCont}>
                            <Button style={styles.btnStyle} onClick={gotoWallet}>Deposit Funds</Button>
                        </Col>
                        </Row>
                        <Row>
                        <Col sm={6} smOffset={3} className="walletBtnDiv" style={styles.btnCont}>
                            <Button style={styles.btnStyle} onClick={gotoWallet}>Withdraw Funds</Button>
                        </Col>
                    </Row>
                </Col>
            </Col>
            <Col sm={12} style={styles.infoContainer2} id="walletNavHeight">
                <h1 style={styles.dataContainer2}>Transaction History</h1>
                <h1 style={styles.dataContainer2}>Smart Contracts</h1>
                <h1 style={styles.dataContainer2}>Investment Documents</h1>
                {/* <h1 style={styles.dataContainer2}>Market Trend</h1> */}
                <hr divider style={styles.line}/>
                <h1 style={styles.dataContainer2}>Refer A Friend</h1>
                <h1 style={styles.dataContainer2}>Subscribe To NewsLetter </h1>
            </Col>
        </Row>
    );
}
DashboardWalletSec.propTypes = {

};

export default DashboardWalletSec;
