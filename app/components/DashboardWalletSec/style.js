import styled from 'styled-components';
import Bg from './bg.png';

const Styles = {
    infoContainer:{
        width: '100%',
        overflow: 'hidden',
        textAlign:'center',
        backgroundSize: 'cover',
        backgroundImage: "url(" + Bg + ")"
    },
    infoHeader:{
        textAlign: 'center',
        height:'24px',
        fontsize:'24px',
        fontFamily: 'Open Sans',
        fontSize: '24px',
        letterSpacing: '0.6px',
        color: '#ffffff',
    },
    infoBalance:{
        textAlign: 'center',
        height: '25px',
        fontFamily: 'Open Sans',
        fontSize: '30px',
        letterSpacing: '0.8px',
        color: '#ffffff',
        textShadow: '0px 3px 7px rgba(21, 22, 22, 0.35)',
        margin:'-10px 0px 12px 0px',
    },
    btnStyle: {
        fontSize: '17px',
        fontWeight: '100',
        border: '2px solid #fff',
        fontFamily: 'Open Sans',
        padding: '2px 12px',
        position: 'relative',
        margin:'0px 0px 0px -9px',
        width: '164px',
        height:'42px',
        letterSpacing: '0.4px',
      },
      btnStyleW: {
          fontSize: '17px',
          fontWeight: '100',
          border: '2px solid #fff',
          background: 'transparent',
          fontFamily: 'Open Sans',
          padding: '2px 12px',
          position: 'relative',
          width: '164px',
          height:'42px',
          letterSpacing: '0.4px',
        },
      btnCont: {
        margin:'0px 15% 15px 0%',
        height:'42px',
      },
      btnContainer: {
        margin : '48px 50px 0px 0px',
      },
      infoContainer2:{
          background:'#FFF',
          height: '73vh',
          padding: '10px'
      },
      dataContainer2:{
        height: '18px',
        fontFamily: 'Open Sans',
        fontSize: '18px',
        textAlign: 'center',
        color: '#464646',
        marginTop:'30px',
      },
      line:{
          marginTop:'50px',
          backgroundColor:'dimgrey',
          height:'1px',
      }
}
export default Styles;
