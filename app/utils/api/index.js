import React from 'react';
import {browserHistory} from 'react-router';
import axios from 'axios';

const baseUrl = 'http://api.estatechain.io/'
// const baseUrl = 'http://localhost:4000/'

let getHeaders = async () => {
  //localstorage.clearItem('token');
    if(localStorage.token !== undefined){
        return {
            'Content-Type': 'application/json',
            'Authorization': 'Bearer ' + localStorage.getItem('token'),
          }
    }
    else{
        return {
            'Content-Type': 'application/json',
          }
    }
}

let _apiCallWithData = async (url, method, data) => {
  try {
    let headers = await getHeaders()
    let response = axios.post(url,data, {headers: headers}
      ).then(function (response) {
        if(response.status == 401) {
          browserHistory.push("/auth");
        }
          return response;
      })
      .catch(function (error) {
        if(error.response.status == 401) {
          browserHistory.push("/auth");
        }
          return error;
        });
    return response
  }
  catch (error) {
    return {}
  }
}

let _apiCallWithoutData = async (url, method, data) => {
  try{
    let headers = await getHeaders()
    let response = await axios.get(url,{headers:headers},data)
    .then(function (response) {
      return response;
    })
    .catch(function (error) {
      if(error.response.status == 401) {
        browserHistory.push("/auth");
      }
      return error;
    });
    return response
  }
  catch (error) {
    return {}
  }
}

let _apiCallFileUpload = async (url, method, data) => {
  try {
    const token = await localStorage.getItem('token')
    let headers = {
      'Content-Type': 'multipart/form-data',
      'Authorization': 'Token ' + token,
    }
    let response = await fetch(url, {
      method,
      headers,
      body: data,
    })
    let responseJson = await response.json()
    return responseJson
  } catch (error) {
  }
}

const baseService = {

  get: (endPoint,data) => {
    return _apiCallWithoutData(baseUrl + endPoint + ".json", "GET",data)
  },

  getWithFullUrl: (url) => {
    return _apiCallWithoutData(url, "GET")
  },

  post: (endPoint, data) => {
    return _apiCallWithData(baseUrl + endPoint + ".json", "POST", data)
  },

  patch: (endPoint, data) => {
    return _apiCallWithData(baseUrl + endPoint, "PATCH", data)
  },

  put: (endPoint, data) => {
    return _apiCallWithData(baseUrl + endPoint, "PUT", data)
  },

  delete: (endPoint) => {
    return _apiCallWithoutData(baseUrl + endPoint, "DELETE", {})
  },

  fileUpload: (endPoint, data) => {
    return _apiCallFileUpload(baseUrl + endPoint, "PATCH", data)
  },

  documentUpload: (endPoint, data) => {
    return _apiCallFileUpload(baseUrl + endPoint, "POST", data)
  },
}

export default baseService;